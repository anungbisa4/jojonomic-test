import React from "react";
import dynamic from "next/dynamic";

// components
import Layout from "../layouts";

const base_url = process.env.base_url;

export default function Home({
  dataProgram,
  dataMagazine,
  dataAds,
  dataBanners,
  dataTalent,
  dataInfluencer,
  dataVenue,
  dataVendor,
}) {
  return (
    <Layout>
    </Layout>
  );
}

export async function getStaticProps() {
  const [
    programRes,
    magazineRes,
    adsRes,
    bannersRes,
    talentRes,
    influencerRes,
    venueRes,
    vendorRes,
  ] = await Promise.all([
    fetch(`${base_url}/programs?page=1&pageSize=9`),
    fetch(`${base_url}/magazine?page=1&pageSize=3`),
    fetch(`${base_url}/ads`),
    fetch(`${base_url}/banners?page=1&pageSize=3`),
    fetch(`${base_url}/partners?page=1&pageSize=6&partner_type=TALENT`),
    fetch(`${base_url}/partners?page=1&pageSize=6&partner_type=INFLUENCER`),
    fetch(`${base_url}/partners?page=1&pageSize=8&partner_type=VENUE`),
    fetch(`${base_url}/partners?page=1&pageSize=8&partner_type=VENDOR`),
  ]);

  const [
    dataProgram,
    dataMagazine,
    dataAds,
    dataBanners,
    dataTalent,
    dataInfluencer,
    dataVenue,
    dataVendor,
  ] = await Promise.all([
    programRes.json(),
    magazineRes.json(),
    adsRes.json(),
    bannersRes.json(),
    talentRes.json(),
    influencerRes.json(),
    venueRes.json(),
    vendorRes.json(),
  ]);

  return {
    props: {
      dataProgram,
      dataMagazine,
      dataAds,
      dataBanners,
      dataTalent,
      dataInfluencer,
      dataVenue,
      dataVendor,
    },
  };
}
