import App from "next/app";
import Head from "next/head";
import CssBaseline from "@material-ui/core/CssBaseline";
import { Provider, useStore } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";
import { wrapper } from "../src/redux/store";
import "../styles/globals.css";
import "swiper/css";

function MyApp({ Component, pageProps, router }) {
  const store = useStore((state) => state);

  return (
    <>
      <Head>
        <title>Eventori</title>
        <meta
          name="viewport"
          content="minimum-scale=1, initial-scale=1, width=device-width"
        />
        <link
          rel="icon"
          href="https://www.eventori.id/assets/images/favicon.png"
        />
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link
          rel="preconnect"
          href="https://fonts.gstatic.com"
          crossOrigin="true"
        />
        <link
          href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700&display=swap"
          rel="stylesheet"
        />
      </Head>
      <Provider store={store}>
        <PersistGate loading={<Loading />} persistor={store.__persistor}>
          <CssBaseline />
          <Component {...pageProps} {...{ router }} key={router.route} />
        </PersistGate>
      </Provider>
    </>
  );
}

App.getInitialProps = async (appContext) => {
  // calls page's `getInitialProps` and fills `appProps.pageProps`
  const appProps = await App.getInitialProps(appContext);

  return { ...appProps };
};


export default wrapper.withRedux(MyApp);

const Loading = () => {
  return (
    <div className="flex justify-center items-center">
      <p></p>
    </div>
  );
};
