import React from "react";
import ImageUploading from "react-images-uploading";
import Button from "components/Button";
import removeArr from "lodash/remove";
import { PlusIcon } from "@heroicons/react/outline";

export default function ImageUpload({
  images,
  setImages,
  onUpload,
  onRemove,
  iconRemove,
  plusButton,
}) {
  const maxNumber = 10;

  const onChange = (imageList, addUpdateIndex) => {
    let resultImage = imageList;
    if (onUpload) {
      onUpload(resultImage[addUpdateIndex], addUpdateIndex[0], resultImage);
    } else {
      onRemove(resultImage[addUpdateIndex]);
    }
    setImages(resultImage);
  };

  return (
    <div>
      <ImageUploading
        multiple
        value={images}
        onChange={onChange}
        maxNumber={maxNumber}
        dataURLKey="data_url"
      >
        {({ onImageUpload, onImageRemove }) => {
          if (onRemove) {
            return (
              <button type="button" onClick={onImageRemove}>
                {iconRemove}
              </button>
            );
          }
          return (
            <div className="upload__image-wrapper">
              {plusButton ? (
                <Button
                  buttonType="button"
                  color="primary-bg"
                  iconPrefix={<PlusIcon className="w-12" />}
                  className="!rounded-lg"
                  iconButton
                  onClick={onImageUpload}
                />
              ) : (
                <Button
                  buttonType="button"
                  color="primary-bg"
                  text="Tambah Foto"
                  className="!p-1.5 relative"
                  classText="!text-xs"
                  onClick={onImageUpload}
                />
              )}
            </div>
          );
        }}
      </ImageUploading>
    </div>
  );
}
