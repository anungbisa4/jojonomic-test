import { Fragment, useState, useEffect } from "react";
import { Listbox, Transition } from "@headlessui/react";
import { CheckIcon, SelectorIcon } from "@heroicons/react/solid";
import { Controller } from "react-hook-form";
import { ErrorMessage } from "@hookform/error-message";

export default function ListBox({
  label,
  labelClass,
  required,
  defaultValues,
  selectedName,
  selectedValue,
  setValue,
  statusError,
  placeholder,
  listOption,
  control,
  name,
  error,
}) {
  const [selected, setSelected] = useState(defaultValues || null);

  if (!control) {
    return (
      <div className="text-xs space-y-1">
        {label && (
          <label className={`${labelClass}`}>
            {label}
            {required && <span className="text-red-500 font-bold">*</span>}
          </label>
        )}
        <Listbox value={selected} onChange={setSelected}>
          <div className="relative">
            <Listbox.Button className="border-1 p-2 rounded-lg border-gray-900 w-full focus:outline-none focus:border-primary-orange text-left">
              <span
                className={`block truncate ${!selected ? "text-gray-400" : ""}`}
              >
                {selected?.name || placeholder}
              </span>
              <span className="absolute inset-y-0 right-0 flex items-center pr-2 pointer-events-none">
                <SelectorIcon
                  className="w-5 h-5 text-gray-400"
                  aria-hidden="true"
                />
              </span>
            </Listbox.Button>
            <Transition
              as={Fragment}
              leave="transition ease-in duration-100"
              leaveFrom="opacity-100"
              leaveTo="opacity-0"
            >
              <Listbox.Options className="absolute w-full py-2 mt-2 border-1 border-gray-300 overflow-auto text-xs bg-white rounded-md shadow-lg max-h-60 ring-1 ring-black ring-opacity-5 focus:outline-none sm:text-xs z-50">
                {listOption?.map((option, index) => (
                  <Listbox.Option
                    key={index}
                    className={({ active }) =>
                      `${
                        active
                          ? "text-amber-900 bg-amber-100  bg-opacity-60"
                          : "text-gray-900 "
                      }
                          cursor-pointer select-none relative px-2`
                    }
                    value={option}
                  >
                    {({ selected, active }) => (
                      <>
                        <span
                          className={`p-2 ${
                            selected
                              ? "font-medium bg-orange-10 rounded-md text-black"
                              : "font-normal"
                          } block truncate`}
                        >
                          {option.name}
                        </span>
                      </>
                    )}
                  </Listbox.Option>
                ))}
              </Listbox.Options>
            </Transition>
          </div>
        </Listbox>
      </div>
    );
  }
  return (
    <div className="text-xs space-y-1 relative">
      {label && (
        <label className={`${labelClass}`}>
          {label}
          {required && <span className="text-red-500 font-bold">*</span>}
        </label>
      )}
      <Controller
        control={control}
        defaultValue={defaultValues?.[selectedName]}
        name={name}
        render={({ field: { onChange } }) => (
          <Listbox
            as="div"
            className="space-y-1"
            value={selected}
            onChange={(e) => {
              setValue(`list.${name}`, e);
              onChange(e[selectedValue]);
              setSelected(e);
            }}
          >
            {({ open }) => (
              <>
                <div className="relative">
                  <Listbox.Button
                    className={`border-1 p-2 rounded-lg ${
                      statusError ? "border-red-500" : "border-gray-900"
                    } w-full focus:outline-none focus:border-primary-orange text-left`}
                  >
                    <span
                      className={`block truncate ${
                        !selected ? "text-gray-400" : ""
                      }`}
                    >
                      {selected?.[selectedName] || placeholder}
                    </span>
                    <span className="absolute inset-y-0 right-0 flex items-center pr-2 pointer-events-none">
                      <SelectorIcon
                        className="w-5 h-5 text-gray-400"
                        aria-hidden="true"
                      />
                    </span>
                  </Listbox.Button>
                  <Transition
                    show={open}
                    as={Fragment}
                    leave="transition ease-in duration-100"
                    leaveFrom="opacity-100"
                    leaveTo="opacity-0"
                  >
                    <Listbox.Options className="absolute w-full py-2 mt-2 border-1 border-gray-300 overflow-auto text-xs bg-white rounded-md shadow-lg max-h-60 ring-1 ring-black ring-opacity-5 focus:outline-none sm:text-xs z-50">
                      {listOption && listOption?.map((option, index) => (
                        <Listbox.Option
                          key={index}
                          className={({ active }) =>
                            `${
                              active
                                ? "text-amber-900 bg-amber-100  bg-opacity-60"
                                : "text-gray-900 "
                            }
                          cursor-pointer select-none relative px-2`
                          }
                          value={option}
                        >
                          {(props) => {
                            return (
                              <>
                                <span
                                  className={`p-2 ${
                                    props.selected ||
                                    selected?.[selectedName] === props.selected
                                      ? "font-medium bg-orange-10 rounded-md text-black"
                                      : "font-normal"
                                  } block truncate`}
                                >
                                  {option[selectedName]}
                                </span>
                              </>
                            );
                          }}
                        </Listbox.Option>
                      ))}
                    </Listbox.Options>
                  </Transition>
                </div>
              </>
            )}
          </Listbox>
        )}
      />
      {error && (
        <div className="absolute -bottom-5 text-[11px] text-red-500">
          <ErrorMessage
            errors={error}
            name={name}
            render={({ message }) => <p>{message}</p>}
          />
        </div>
      )}
    </div>
  );
}
