import React from "react";
import Link from "next/link";
import { Tab } from "@headlessui/react";

import { StarOutlineIcon, ChatBubble } from "@/components/Icons";
import BellHandIcon from "@/components/Icons/BellHandIcon";
import Button from "components/Button";

const TabList = ({ label, src, nama }) => {
  return (
    <>
      <div className="flex flex-1 h-full items-center justify-between lg:mx-[35px] xl:mx-[145px] xs:gap-0 md:gap-2 lg:gap-2">
        <div className="flex xs:w-[150px] md:w-[200px] lg:w-[270px] xl:w-[297px] border-r-3 h-full">
          <div className="flex xs:mx-auto md:m-0 lg:m-0 xs:mt-1 xs:flex-col md:flex-row lg:flex-row items-center xs:gap-0 md:gap-3 lg:gap-5">
            {/* md:h-[50px] md:w-[50px] lg:h-[80px] lg:w-[80px] xl:h-[80px] xl:w-[80px] */}
            <img
              className="xs:max-h-[30px] sm:max-h-[40px] md:max-h-[50px] lg:max-h-[80px] xl:max-h-[80px] h-full rounded-full"
              src={src}
              alt="image"
            />
            <div className="flex-col pt-2">
              <h1 className="font-bold xs:text-center xs:text-[8px] md:text-xs lg:text-lg xl:text-lg">
                {nama}
              </h1>
              <Link href="/">
                <p className="font-bold xs:text-center xs:text-[8px] text-sm md:text-xs text-primary-orange">
                  Edit Profile
                </p>
              </Link>
            </div>
          </div>
        </div>
        <Tab className="flex h-full">
          {({ selected }) => (
            <div
              className={
                selected
                  ? "border-b-[3px] border-primary-orange text-primary-orange flex items-center xs:py-[25px] md:py-[29px] lg:py-[47px] xl:py-[47px]"
                  : "border-b-[3px] border-transparent md:py-[29px] lg:py-[47px] xl:py-[47px]"
              }
            >
              <div className="flex xs:flex-col md:flex-row lg:flex-row xs:gap-1 lg:gap-2">
                <StarOutlineIcon className="xs:mx-6 md:mx-0 lg:mx-0 xl:mx-0 xs:w-3 xs:h-3 md:w-4 md:h-4 lg:w-5 lg:h-5 xl:w-5 xl:h-5 mt-[2px]" />
                <span className="font-bold xs:text-[8px] md:text-sm lg:text-lg xl:text-lg">
                  {label}
                </span>
              </div>
            </div>
          )}
        </Tab>
        <Tab className="flex h-full">
          {({ selected }) => (
            <div
              className={
                selected
                  ? "border-b-[3px] border-primary-orange text-primary-orange flex items-center xs:py-[25px] md:py-[29px] lg:py-[47px] xl:py-[47px]"
                  : "border-b-[3px] border-transparent md:py-[29px] lg:py-[47px] xl:py-[47px]"
              }
            >
              <div className="flex xs:flex-col md:flex-row lg:flex-row xs:gap-1 lg:gap-3">
                <BellHandIcon className="xs:mx-6 md:mx-0 lg:mx-0 xl:mx-0xs:w-3 xs:h-3 md:w-4 md:h-4 w-5 h-5 mt-[2px]" />
                <span className="font-bold xs:text-[8px] md:text-sm lg:text-lg xl:text-lg">
                  Notifikasi
                </span>
              </div>
            </div>
          )}
        </Tab>
        <Tab className="flex h-full">
          {({ selected }) => (
            <div
              className={
                selected
                  ? "border-b-[3px] border-primary-orange text-primary-orange flex items-center xs:py-[25px] md:py-[29px] lg:py-[47px] xl:py-[47px]"
                  : "border-b-[3px] border-transparent md:py-[29px] lg:py-[47px] xl:py-[47px]"
              }
            >
              <div className="flex xs:flex-col md:flex-row lg:flex-row xs:gap-1 lg:gap-3">
                <ChatBubble className="xs:mx-6 md:mx-0 lg:mx-0 xl:mx-0 xs:h-3 md:w-4 md:h-4 w-5 h-5 mt-[2px]" />
                <span className="font-bold xs:text-[8px] md:text-sm lg:text-lg xl:text-lg">
                  Pesan Masuk
                </span>
              </div>
            </div>
          )}
        </Tab>
        <div>
          <Button
            type="button"
            className="bg-[#FF0000] text-white md:px-5 lg:px-7 xl:px-8"
            text="Logout"
            textClass="md:text-sm lg:text-lg xl:text-lg"
          />
        </div>
      </div>
    </>
  );
};

export default TabList;
