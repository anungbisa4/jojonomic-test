import * as React from "react";

import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Rating from "@material-ui/lab/Rating";

export default function InputRating() {
  const [radio, setRadio] = React.useState("");

  const handleChange = (event) => {
    setRadio(event.target.value);
  };

  return (
    <div>
      <RadioGroup
        aria-label="gender"
        name="gender1"
        value={radio}
        onChange={handleChange}
      >
        <FormControlLabel
          value="1"
          control={<Radio color="default" color="default" />}
          label={
            <Rating name="read-only" defaultValue={1} size="medium" readOnly />
          }
        />
        <FormControlLabel
          value="2"
          control={<Radio color="default" />}
          label={
            <Rating name="read-only" defaultValue={2} size="medium" readOnly />
          }
        />
        <FormControlLabel
          value="3"
          control={<Radio color="default" />}
          label={
            <Rating name="read-only" defaultValue={3} size="medium" readOnly />
          }
        />
        <FormControlLabel
          value="4"
          control={<Radio color="default" />}
          label={
            <Rating name="read-only" defaultValue={4} size="medium" readOnly />
          }
        />
        <FormControlLabel
          value="5"
          control={<Radio color="default" />}
          label={
            <Rating name="read-only" defaultValue={5} size="medium" readOnly />
          }
        />
        <FormControlLabel
          value="6"
          control={<Radio color="default" />}
          label="Semua Rating"
        />
      </RadioGroup>
    </div>
  );
}
