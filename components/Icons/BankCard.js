import React from "react";

function Icon() {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="22"
      height="18"
      fill="none"
      viewBox="0 0 22 18"
    >
      <path
        stroke="currentColor"
        strokeLinejoin="round"
        strokeWidth="2"
        d="M1 2a1 1 0 011-1h18a1 1 0 011 1v14a1 1 0 01-1 1H2a1 1 0 01-1-1V2z"
      ></path>
      <path
        stroke="currentColor"
        strokeLinecap="square"
        strokeLinejoin="round"
        strokeWidth="2"
        d="M1 5h20"
      ></path>
      <path
        stroke="currentColor"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="2"
        d="M12.5 13H17M21 2v8M1 2v8"
      ></path>
    </svg>
  );
}

export default Icon;
