import React from "react";

function Icon({ className }) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      className={className}
      fill="none"
      viewBox="0 0 18 22"
    >
      <path
        stroke="currentColor"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="2"
        d="M9.72 4.003L8.256 4c-3.344-.008-6.247 2.709-6.27 6v3.79c0 .79-.1 1.56-.531 2.218l-.287.438C.73 17.11 1.2 18 1.985 18h14.03c.785 0 1.254-.89.818-1.554l-.287-.438c-.43-.657-.531-1.43-.531-2.22v-3.787c-.04-3.292-2.95-5.99-6.294-5.998v0zM12 18a3 3 0 01-6 0"
      ></path>
      <path
        stroke="currentColor"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="2"
        d="M9 1a2 2 0 012 2v1H7V3a2 2 0 012-2z"
      ></path>
    </svg>
  );
}

export default Icon;
