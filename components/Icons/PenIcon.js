import React from "react";

function Icon({className}) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="30"
      height="30"
      fill="currentColor"
      className={className}
      viewBox="0 0 30 30"
    >
      <path
        fill="currentColor"
        d="M.352 28.113l1.333-8c.045-.273.174-.526.37-.723L19.943 1.506a4 4 0 015.656 0l2.895 2.896a4 4 0 010 5.655L10.61 27.943c-.197.197-.45.327-.724.373l-8 1.333a1.331 1.331 0 01-1.534-1.534z"
      ></path>
    </svg>
  );
}

export default Icon;
