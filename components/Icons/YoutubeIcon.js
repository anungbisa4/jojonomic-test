import React from "react";

function Icon({className}) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      className={className}
      fill="none"
      viewBox="0 0 28 19"
    >
      <path
        fill="currentColor"
        d="M26.77 3.085A3.365 3.365 0 0024.404.702C22.315.139 13.942.139 13.942.139S5.568.139 3.48.702a3.365 3.365 0 00-2.367 2.383 35.3 35.3 0 00-.56 6.487 35.301 35.301 0 00.56 6.487 3.315 3.315 0 002.367 2.345c2.088.563 10.462.563 10.462.563s8.374 0 10.462-.563a3.317 3.317 0 002.367-2.345 35.3 35.3 0 00.56-6.487 35.3 35.3 0 00-.56-6.487zM11.204 13.554V5.59l7 3.982-7 3.98z"
      ></path>
    </svg>
  );
}

export default Icon;