import React from "react";

function Icon({className}) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      fill="none"
      className={className}
      viewBox="0 0 13 13"
    >
      <path fill="currentColor" d="M6.5 12.07L.005.183h12.99L6.5 12.071z"></path>
    </svg>
  );
}

export default Icon;
