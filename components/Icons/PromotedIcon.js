import React from "react";

function Icon({className}) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      fill="none"
      className={className}
      viewBox="0 0 24 24"
    >
      <path
        fill="currentColor"
        fillOpacity="0.7"
        fillRule="evenodd"
        d="M.046 6.856v4.019c0 1.302 1 2.625 2.235 2.625h9.67v-9h-9.67C1.045 4.5.046 5.554.046 6.856zM10.594 23.908H6.037L3.093 13.594H7.65l2.944 10.314zM19.5 0l-6 4.14v8.971l6 4.889V0zM19.5 5.984v6.017c1.629 0 2.953-1.346 2.953-3.01 0-1.66-1.324-3.007-2.953-3.007z"
        clipRule="evenodd"
      ></path>
    </svg>
  );
}

export default Icon;
