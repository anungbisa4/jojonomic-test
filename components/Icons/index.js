import FbIcon from "./FbIcon";
import GoogleIcon from "./GoogleIcon";
import ImagePlaceIcon from "./ImagePlaceIcon";
import FacebookIcon from "./FacebookIcon";
import InstagramIcon from "./InstagramIcon";
import TwitterIcon from "./TwitterIcon";
import YoutubeIcon from "./YoutubeIcon";
import AppStoreIcon from "./AppStoreIcon";
import PlaystoreAppIcon from "./PlaystoreAppIcon";
import CircleSpinIcon from "./CircleSpinIcon";
import PlayCircleIcon from "./PlayCircleIcon";
import ChatBubble from "./ChatBubble";
import HandshakeIcon from "./HandshakeIcon";
import StarOutlineIcon from "./StarOutlineIcon";
import DropdownIcon from "./DropdownIcon";
import StarFilled from "./StarFilledIcon";
import LocationIcon from "./LocationIcon";
import PromotedIcon from "./PromotedIcon";
import AvatarIcon from "./AvatarIcon";
import BookmarkIcon from "./BookmarkIcon";
import BookmarkSavedIcon from "./BookmarkSavedIcon";
import ArrowIcon from "./ArrowIcon";
import CreditCardIcon from "./CreditCardIcon";
import DiscountIcon from "./DiscountIcon";
import ChecklistIcon from "./ChecklistIcon";
import AvatarOutlineIcon from "./AvatarOutlineIcon";
import PhoneIcon from "./PhoneIcon";
import PenIcon from "./PenIcon";

export {
  FbIcon,
  GoogleIcon,
  ImagePlaceIcon,
  YoutubeIcon,
  TwitterIcon,
  InstagramIcon,
  FacebookIcon,
  AppStoreIcon,
  // PlaystoreAppIcon,
  CircleSpinIcon,
  PlayCircleIcon,
  ChatBubble,
  HandshakeIcon,
  StarOutlineIcon,
  DropdownIcon,
  StarFilled,
  LocationIcon,
  PromotedIcon,
  AvatarIcon,
  BookmarkIcon,
  BookmarkSavedIcon,
  ArrowIcon,
  CreditCardIcon,
  DiscountIcon,
  ChecklistIcon,
  AvatarOutlineIcon,
  PhoneIcon,
  PenIcon,
};
