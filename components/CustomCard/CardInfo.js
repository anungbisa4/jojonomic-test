import { LocationIcon, StarFilled } from "../Icons";

const CardInfo = ({ category, data }) => {
  const handleStyle = () => {
    if (category == "talent" || category == "influencer") {
      return {
        rowOneStyle: "flex flex-col items-start text-primary-black mt-2",
        nameStyle: "text-xs sm:text-base lg:text-lg xl:text-xl",
        descStyle: "text-[8px] sm:text-[10px] lg:text-xs xl:text-sm",
      };
    }

    if (category == "venue" || category == "vendor") {
      return {
        rowOneStyle: "flex flex-col items-start text-primary-black mt-2",
        nameStyle: "text-xs sm:text-base lg:text-lg xl:text-xl",
        descStyle: "text-[8px] xl:text-[11px]",
      };
    }
  };

  return (
    <div className="font-pop">
      <div className="flex flex-col mt-1 xs:gap-[2px] lg:gap-1 p-4">
        <h4
          className={`${
            handleStyle().nameStyle
          } text-bold leading-tight break-all line-clamp-2 cursor-pointer`}
        >
          {data.name}
        </h4>
        <h4 className={`${handleStyle().nameStyle} font-bold`}>
          Rp 13.500.000
        </h4>
        <div className="box-content xs:h-6 xs:w-7 md:h-7 md:w-7 lg:h-[30px] lg:w-7 rounded-md border-2 border-transparent bg-primary-orange-opacity">
          <h3 className="absolute xs:pt-[2px] lg:pt-0 text-primary-orange font-bold">
            10%
            <span
              className={`${
                handleStyle().nameStyle
              }  text-[#747474] line-through pl-[6px]`}
            >
              Rp 15.000.000
            </span>
          </h3>
        </div>
        <div>
          <div className="flex flex-row items-end text-primary-gray">
            <LocationIcon className="w-4 h-5 lg:w-5 lg:h-6 text-primary-gray" />
            <h5 className="text-[10px] md:text-xs lg:text-sm">
              {data.location}
            </h5>
          </div>
        </div>
        <div>
          <div className="flex flex-row items-end text-primary-orange">
            <StarFilled className="w-4 h-5 lg:w-5 lg:h-6 text-primary-orange mr-[2px]" />
            <h5 className="text-[10px] md:text-xs lg:text-sm">
              {data.rating}{" "}
              <span className="text-primary-black">
                {/* ({data.voters}) */} (Dipesan 87 kali)
              </span>
            </h5>
          </div>
          <h5 className={`${handleStyle().descStyle} cursor-pointer`}>
            {/* {category === "talent" && `${data.job} / ${data.speciality}`}
              {category === "influencer" && `${data.speciality}`}
              {category === "venue" && `${data.type}`}
              {category === "vendor" &&
                `${data.totalProduction} produk terjual`} */}
            {category === "talent" && `${data?.category}`}
            {/* {category === "influencer" && "ACTOR - SOLO - TEATER - SEMUA GENRE"}
            {category === "venue" && "BARS/PUBS/CLUBS - INDOOR - BAR - BAR"}
            {category === "vendor" && "EVENT STAFFING - ALL - ALL - ALL"} */}
          </h5>
        </div>
      </div>
    </div>
  );
};

export default CardInfo;
