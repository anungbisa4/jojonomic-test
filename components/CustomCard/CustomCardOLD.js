import React from "react";
import {
  createMuiTheme,
  ThemeProvider,
  makeStyles,
} from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";
import LocationOnOutlined from "@material-ui/icons/LocationOnOutlined";
import StarOutlined from "@material-ui/icons/StarOutlined";
import Link from "@material-ui/core/Link";

const useStyles = makeStyles((theme) => ({
  root: {
    width: 380,
    [theme.breakpoints.down("sm")]: {
      width: 300,
    },
    [theme.breakpoints.between("sm", "md")]: {
      width: 190,
    },
    [theme.breakpoints.up("md")]: {
      width: 260,
    },
    [theme.breakpoints.up("lg")]: {
      width: 380,
    },
    [theme.breakpoints.up("xl")]: {
      width: 510,
    },
    margin: 20,
    display: "inline-block",
    boxShadow: "none",
  },
  media: {
    height: 405,
    [theme.breakpoints.down("sm")]: {
      height: 320,
    },
    [theme.breakpoints.between("sm", "md")]: {
      height: 202,
    },
    [theme.breakpoints.up("md")]: {
      height: 288,
    },
    [theme.breakpoints.up("lg")]: {
      height: 405,
    },
    [theme.breakpoints.up("xl")]: {
      height: 544,
    },
    borderRadius: 10,
  },
  titleTalent: {
    fontFamily: "Poppins",
    fontWeight: 700,
    color: "#C9C9C9",
  },
  navTalentLeft: {
    fontFamily: "Poppins",
    fontWeight: 700,
    color: "#23282D",
    paddingTop: theme.spacing(1),
  },
  navTalentRight: {
    fontFamily: "Poppins",
    fontWeight: 700,
    color: "#F6921E !important",
    paddingTop: theme.spacing(1),
  },
  nameTalent: {
    fontFamily: "Poppins",
    fontWeight: 700,
    color: "#23282D",
  },
  nameTalentJob: {
    fontFamily: "Open Sans",
    fontWeight: 400,
    color: "#23282D",
  },
  styleIconLoc: {
    verticalAlign: "text-bottom",
    [theme.breakpoints.down("sm")]: {
      width: 15,
    },
    [theme.breakpoints.up("lg")]: {
      width: 30,
      height: 40,
    },
    [theme.breakpoints.up("xl")]: {
      width: 40,
      height: 50,
    },
  },
  locationText: {
    color: "#C9C9C9",
    fontWeight: 400,
    fontStyle: "italic",
    fontFamily: "Open Sans",
  },
  styleIconStar: {
    color: "#F6921E",
    verticalAlign: "text-bottom",
    [theme.breakpoints.down("sm")]: {
      width: 15,
    },
    [theme.breakpoints.up("lg")]: {
      width: 30,
      height: 38,
    },
    [theme.breakpoints.up("xl")]: {
      width: 40,
      height: 48,
    },
  },
  paddingCardContent: {
    paddingTop: 16,
    paddingLeft: 5,
    paddingRight: 5,
    paddingBottom: 16,
  },
  titleCard: {
    fontFamily: "Montserrat",
    fontWeight: 700,
    color: "#23282D",
    fontSize: 20,
  },
}));

// responsive font size
const theme = createMuiTheme();

theme.typography.h1 = {
  [theme.breakpoints.down("sm")]: {
    fontSize: 13,
  },
  [theme.breakpoints.between("sm", "md")]: {
    fontSize: 20,
  },
  [theme.breakpoints.up("md")]: {
    fontSize: 28,
  },
  [theme.breakpoints.up("lg")]: {
    fontSize: 34,
  },
  [theme.breakpoints.up("xl")]: {
    fontSize: 46,
  },
};

theme.typography.h2 = {
  [theme.breakpoints.down("sm")]: {
    fontSize: 12,
  },
  [theme.breakpoints.between("sm", "md")]: {
    fontSize: 14,
  },
  [theme.breakpoints.up("md")]: {
    fontSize: 22,
  },
  [theme.breakpoints.up("lg")]: {
    fontSize: 30,
  },
  [theme.breakpoints.up("xl")]: {
    fontSize: 36,
  },
};

theme.typography.h3 = {
  [theme.breakpoints.down("sm")]: {
    fontSize: 10,
  },
  [theme.breakpoints.between("sm", "md")]: {
    fontSize: 10,
  },
  [theme.breakpoints.up("md")]: {
    fontSize: 14,
  },
  [theme.breakpoints.up("lg")]: {
    fontSize: 18,
  },
  [theme.breakpoints.up("xl")]: {
    fontSize: 24,
  },
};

theme.typography.h4 = {
  [theme.breakpoints.down("sm")]: {
    fontSize: 8,
  },
  [theme.breakpoints.up("sm")]: {
    fontSize: 12,
  },
  [theme.breakpoints.up("lg")]: {
    fontSize: 16,
  },
  [theme.breakpoints.up("xl")]: {
    fontSize: 24,
  },
};

export default function CustomCard(props) {
  const classes = useStyles();

  var renderCardIndex = (item, partnerType) => {
    let partner = partnerType.toLowerCase();
    return (
      <Grid item>
        {/* {console.log(partner)}; */}
        <Link href={`/detail-partner/${partner}/${item.id}`}>
          <Card className={classes.root}>
            <CardActionArea>
              <CardMedia
                className={classes.media}
                image={item.profilePicture}
                title={item.title}
              />
              <CardContent className={classes.paddingCardContent}>
                <ThemeProvider theme={theme}>
                  <Grid container alignItems="center" justify="space-between">
                    <Typography
                      gutterBottom
                      variant="h2"
                      className={classes.nameTalent}
                    >
                      {item.name}
                    </Typography>
                    <Typography
                      gutterBottom
                      variant="h4"
                      className={classes.nameTalentJob}
                    >
                      {item.job}
                    </Typography>
                  </Grid>
                  <Grid container alignItems="center" justify="space-between">
                    <Grid item>
                      <Grid container alignItems="flex-end">
                        <Grid item>
                          <LocationOnOutlined
                            className={classes.styleIconLoc}
                          />
                        </Grid>
                        <Grid item>
                          <Typography
                            gutterBottom
                            variant="h4"
                            className={classes.locationText}
                          >
                            {item.location}
                          </Typography>
                        </Grid>
                      </Grid>
                    </Grid>

                    <Grid item>
                      <Grid container alignItems="flex-end">
                        <StarOutlined className={classes.styleIconStar} />
                        <Typography gutterBottom variant="h4">
                          {item.rating} ({item.voters})
                        </Typography>
                      </Grid>
                    </Grid>
                  </Grid>
                </ThemeProvider>
              </CardContent>
            </CardActionArea>
          </Card>
        </Link>
      </Grid>
    );
  };

  var renderCardPartner = (item) => {
    return (
      <Grid item>
        <Link href={`/detail-partner/talent/${item.id}`}>
          <Card className={classes.root}>
            <CardActionArea>
              <CardMedia
                className={classes.media}
                image={item.profilePicture}
                title={item.title}
              />
              <CardContent className={classes.paddingCardContent}>
                <Grid container alignItems="center" justify="space-between">
                  <Typography gutterBottom className={classes.nameTalent}>
                    {item.name}
                  </Typography>
                  <Typography gutterBottom className={classes.nameTalentJob}>
                    {item.type}
                  </Typography>
                </Grid>
                <Grid container alignItems="center" justify="space-between">
                  <Typography gutterBottom className={classes.locationText}>
                    <LocationOnOutlined className={classes.styleIconLoc} />
                    {item.location}
                  </Typography>
                  <Typography gutterBottom>
                    <StarOutlined className={classes.styleIconStar} />
                    {item.rating} ({item.voters})
                  </Typography>
                </Grid>
              </CardContent>
            </CardActionArea>
          </Card>
        </Link>
      </Grid>
    );
  };
  var renderCardMediaProgram = (item) => {
    return (
      <Grid item>
        <Card className={classes.root}>
          <CardActionArea>
            <CardMedia
              className={classes.media}
              image="https://www.eventori.id/assets/images/foto_statis/img_1617013957.png"
              title=""
            />
            <CardContent className={classes.paddingCardContent}>
              <Grid container alignitem="center" justify="center">
                <Typography>
                  <Typography className={classes.titleCard}>{item}</Typography>
                </Typography>
              </Grid>
            </CardContent>
          </CardActionArea>
        </Card>
      </Grid>
    );
  };
  var renderSearchPartner = (item) => {
    return (
      <Card className={classes.root}>
        <CardActionArea>
          <CardMedia
            className={classes.media}
            image="https://www.eventori.id/assets/images/foto_berita/img_1617970675.png"
            title="Contemplative Reptile"
          />
          <CardContent className={classes.paddingCardContent}>
            <Grid container alignItems="center" justify="space-between">
              <Typography gutterBottom className={classes.nameTalent}>
                {item}
              </Typography>
              <Typography gutterBottom className={classes.nameTalentJob}>
                Job/Speciality
              </Typography>
            </Grid>
            <Grid container alignItems="center" justify="space-between">
              <Typography gutterBottom className={classes.locationText}>
                <LocationOnOutlined className={classes.styleIconLoc} />
                Location
              </Typography>
              <Typography gutterBottom>
                <StarOutlined className={classes.styleIconStar} />
                4.4(87)
              </Typography>
            </Grid>
          </CardContent>
        </CardActionArea>
      </Card>
    );
  };
  var renderMagazine = (item) => {
    return (
      <Link id={datas.code} href="/detail-majalah">
        <Grid item>
          <Card className={classes.root}>
            <CardActionArea>
              <CardMedia
                className={classes.media}
                image={datas.images}
                title="Contemplative Reptile"
              />
              <CardContent className={classes.paddingCardContent}>
                <Grid
                  container
                  direction="column"
                  justify="flex-start"
                  alignitem="flex-start"
                >
                  <Grid item>
                    <Typography gutterBottom className={classes.titleCategory}>
                      Article Category
                    </Typography>
                  </Grid>
                  <Grid item>
                    <Typography gutterBottom className={classes.titleArticle}>
                      Lights Go Out, Nights Goes In
                    </Typography>
                  </Grid>
                  <Grid item>
                    <Typography gutterBottom className={classes.contentArticle}>
                      Setiap 27 Maret diperingati sebagai Hari Teater Sedunia.
                      Meski kini di dunia tengah merebaknya virus.
                    </Typography>
                  </Grid>
                </Grid>
                <Grid container justify="flex-end">
                  <Typography className={classes.continueContent}>
                    Baca Selengkapnya
                  </Typography>
                </Grid>
              </CardContent>
            </CardActionArea>
          </Card>
        </Grid>
      </Link>
    );
  };

  return (
    <Box mt={4}>
      {/* Card Label Group */}

      {/* {props.type != "type3" ||
      props.type != "type4" ||
      props.type != "type5" */}

      {/* Card Group Label */}
      {props.type == "type1" || props.type == "type2" ? (
        <Box width="100%" style={{ margin: "auto" }}>
          <Grid container justify="center" alignItems="center">
            {props.type == "type1" ? (
              <Grid item xs={10}>
                <ThemeProvider theme={theme}>
                  <Typography variant="h2" className={classes.titleTalent}>
                    {props.title}
                  </Typography>
                </ThemeProvider>
              </Grid>
            ) : (
              ""
            )}
            <Grid item xs={10}>
              <Grid
                container
                justify="space-between"
                alignItems="flex-end"
                spacing={2}
              >
                <Grid item xs={7} lg={6}>
                  <ThemeProvider theme={theme}>
                    <Typography variant="h1" className={classes.navTalentLeft}>
                      {props.label}
                    </Typography>
                  </ThemeProvider>
                </Grid>
                <Grid item xs={5} lg={6} style={{ textAlign: "right" }}>
                  <ThemeProvider theme={theme}>
                    <a
                      component="button"
                      href="/list-partner"
                      className={classes.navTalentRight}
                      style={{ textDecoration: "none" }}
                    >
                      <Typography variant="h3">
                        {`Lihat Semua ${props.title}`}
                      </Typography>
                    </a>
                  </ThemeProvider>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Box>
      ) : (
        ""
      )}

      {/* Card */}
      <Grid
        container
        // spacing={1}
        justify="center"
        style={{ width: "90%", margin: "auto" }}
      >
        {props.items.map((item, index) => {
          if (props.type === "type1") {
            // partner (card with location)
            return renderCardIndex(item, props.title);
          } else if (props.type === "type2") {
            // production/venue/community (card without location)
            return renderCardPartner(item);
          } else if (props.type === "type3") {
            // media & program
            return renderCardMediaProgram(item);
          } else if (props.type === "type4") {
            //  search list partner
            return renderSearchPartner(item);
          } else if (props.type == "type5") {
            // magazine /  post community magazine
            return renderMagazine(item);
          }
        })}
      </Grid>
    </Box>
  );
}
