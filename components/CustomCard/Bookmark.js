import { useState } from "react";
import { BookmarkIcon, BookmarkSavedIcon } from "@/components/Icons";

const Bookmark = () => {
  const [saved, setSaved] = useState(false);

  const handleBookmark = () => {
    if (!saved) {
      setSaved(true);
    } else {
      setSaved(false);
    }
  };

  return (
    <div className="absolute -top-2 right-[calc(7%)]">
      <div onClick={handleBookmark} className="w-10 xl:w-[45px] cursor-pointer">
        {saved ? (
          <BookmarkSavedIcon className="w-full text-primary-orange" />
        ) : (
          <BookmarkIcon className="w-full text-primary-orange" />
        )}
      </div>
    </div>
  );
};

export default Bookmark;
