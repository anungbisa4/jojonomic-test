import Breadcrumb from "components/Breadcrumb";
import BookingProfile from "../pages/partner/booking/BookingProfile";
import PaymentSummary from "../pages/partner/booking/PaymentSummary";


const DetailBooking = ({ type }) => {
  

  return (
      <div className="container-content xs:mt-12 lg:mt-[175px]">
        <Breadcrumb />
        <div className="grid xs:grid-cols-1 md:grid-cols-3 lg:grid-cols-3 max-h-[328px] mt-5 md:gap-2 lg:gap-16">
          <BookingProfile type={type}/>
          <PaymentSummary/>
        </div>
      </div>
  );
};

export default DetailBooking;
