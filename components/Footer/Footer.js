import React from "react";

import DataFooter from "/data/DataFooter.json";

export default function FooterReuse() {
  return (
    <section>
      <div className="lg:container-xl h-full w-full pt-16 bg-primary-white">
        <div className="grid grid-cols-12 grid-rows-1 gap-0 mx-10">
          <div className="sm:col-span-4 md:col-span-2 lg:col-span-3">
            <div
              className="bg-primary-orange w-full h-32 rounded-tr-3xl border-1"
              style={{ boxShadow: "0px 4px 4px rgba(0, 0, 0, 0.25)" }}
            >
              <div className="py-12 text-center text-white font-bold text-lg">
                Sister Company
              </div>
            </div>
          </div>

          <div className="sm:col-span-4 md:col-span-2 lg:col-span-3">
            <div
              className="bg-white w-full h-32 full border-2 border-white p-4"
              style={{
                borderRightColor: "#F6921E",
                boxShadow: "0px 4px 4px rgba(0, 0, 0, 0.25)",
              }}
            >
              <div className="grid grid-rows-2 grid-rows-2">
                <div className="pt-7 text-center font-bold">ACTIVATION</div>
                <a className="text-center text-primary-orange">Kunjungi Kami</a>
              </div>
            </div>
          </div>

          <div className="sm:col-span-4 md:col-span-2 lg:col-span-3">
            <div
              className="bg-white w-full h-32 full border-2 border-white p-4"
              style={{
                borderRightColor: "#F6921E",
                boxShadow: "0px 4px 4px rgba(0, 0, 0, 0.25)",
              }}
            >
              <div className="grid grid-rows-2 grid-rows-2">
                <div className="pt-7 text-center font-bold">BENANG MERAH</div>
                <a className="text-center text-primary-orange">Kunjungi Kami</a>
              </div>
            </div>
          </div>

          <div className="sm:col-span-4 md:col-span-2 lg:col-span-3">
            <div
              className="bg-white w-full h-32 full border-2 border-white p-4 rounded-tr-3xl"
              style={{ boxShadow: "4px 4px 4px rgba(0, 0, 0, 0.25)" }}
            >
              <div className="grid grid-rows-2 grid-rows-2">
                <div className="pt-7 text-center font-bold">
                  V ENTERTAINMENT
                </div>
                <a className="text-center text-primary-orange">Kunjungi Kami</a>
              </div>
            </div>
          </div>
        </div>
        <div className="grid grid-cols-4 mx-10 mb-20 gap-6">
          <div className="grid grid-rows-2 grid-cols-1 h-72 gap-4 pt-16">
            {DataFooter.column1.map((item, index) => {
              return (
                <div key={index}>
                  <p className="text-2xl font-bold">{item.title}</p>

                  {item.content.map((item2, index2) => {
                    return (
                      <p key={index2} className="text-xl pt-3 break-words">
                        <a href={item2.href}>{item2.subtitle}</a>
                        <span
                          className="font-bold"
                          style={{ fontFamily: "Poppins" }}
                        >
                          {item2.email}
                        </span>
                      </p>
                    );
                  })}
                </div>
              );
            })}
          </div>
          <div className="grid grid-rows-1 grid-cols-1 gap-4 pt-16">
            {DataFooter.column2.map((item, index) => {
              return (
                <div key={index}>
                  <p className="text-2xl font-bold">{item.title}</p>

                  {item.content.map((item2, index2) => {
                    return (
                      <p key={index2} className="text-xl pt-3 break-words">
                        <a href={item2.href}>{item2.subtitle}</a>
                      </p>
                    );
                  })}
                </div>
              );
            })}
          </div>
          <div className="grid grid-rows-1 grid-cols-1 gap-4 pt-16">
            {DataFooter.column3.map((item, index) => {
              return (
                <div key={index}>
                  <p className="text-2xl font-bold">{item.title}</p>

                  {item.content.map((item2, index2) => {
                    return (
                      <p key={index2} className="text-xl pt-3 break-words">
                        <a href={item2.href}>{item2.subtitle}</a>
                      </p>
                    );
                  })}
                </div>
              );
            })}
          </div>
          <div className="grid grid-rows-2 grid-cols-1 h-72 gap-4 pt-16">
            {DataFooter.column4.map((item, index) => {
              return (
                <div key={index} className="grid-col-1">
                  <p className="text-2xl font-bold">{item.title}</p>
                  <div className="grid grid-cols-2">
                    {item.content.map((item2, index2) => {
                      return (
                        <img
                          key={index2}
                          className="w-72 h-28"
                          src={item2.img}
                        />
                      );
                    })}
                  </div>
                </div>
              );
            })}
            {DataFooter.column5.map((item, index) => {
              return (
                <div key={index} className="grid-col-1">
                  <p className="text-2xl font-bold">{item.title}</p>
                  <div className="flex grid-cols-5 gap-1 w-60">
                    {item.content.map((item2, index2) => {
                      return (
                        <img
                          key={index2}
                          className="w-10 h-auto mx-auto"
                          src={item2.img}
                        />
                      );
                    })}
                  </div>
                </div>
              );
            })}
          </div>
        </div>
        <div className="grid w-full h-18 bg-primary-black">
          <p
            className="text-center py-5 text-white text-lg"
            style={{ fontFamily: "Open Sans" }}
          >
            Copyright © 2021 Eventori.id | All Rights Reserved.
          </p>
        </div>
      </div>
    </section>
  );
}
