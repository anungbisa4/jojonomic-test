import React, { useState } from "react"
import { PropTypes } from "prop-types";

import Step from "./Step"
const Stepper = ({activeStep, listStep, numbered}) => {
  return (
    <>
      <div className="stepper-wrapper flex items-center">
        {listStep.map((step, index) => (
          <Step
            key={index}
            index={index + 1}
            title={step.title}
            first={index + 1 === activeStep}
            isLast={index + 1 === listStep.length}
            active={index + 1 === activeStep}
            completed={index + 1 < activeStep}
            numbered={numbered}
          />
        ))}
      </div>
    </>
  );
}

export default React.memo(Stepper)

Stepper.defaultProps = {
  activeStep: 1,
};