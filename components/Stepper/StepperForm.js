import React from "react";
import PropTypes from "prop-types";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import clsx from "clsx";
import Stepper from "@material-ui/core/Stepper";
import Step from "@material-ui/core/Step";
import StepLabel from "@material-ui/core/StepLabel";
import StepConnector from "@material-ui/core/StepConnector";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";

// Connector stepper 2
const CustomConnector = withStyles({
  alternativeLabel: {
    top: 17,
    left: "calc(-30%+34px)",
    right: "calc(30%+34px)",
  },
  active: {
    "& $line": {
      borderColor: "#f6921e",
    },
  },
  completed: {
    "& $line": {
      borderColor: "#f6921e",
    },
  },
  line: {
    borderColor: "#c9c9c9",
    borderTopWidth: 3,
    borderRadius: 1,
  },
})(StepConnector);

// Shape Stepper 2
const StepIconStyles = makeStyles({
  root: {
    color: "#eaeaf0",
    display: "flex",
    width: 34,
    height: 34,
    alignItems: "center",
    // backgroundColor: "blue",
  },
  active: {
    // color: "blue",
    // border: "3px solid #f6921e"
    width: 34,
    height: 34,
    borderRadius: "50%",
    backgroundColor: "#fff",
    "&::after": {
      content: '""',
      display: "block",
      marginLeft: -27,
      width: 20,
      height: 20,
      borderRadius: "50%",
      backgroundColor: "#f6921e",
    },
    // border: "3px solid #f6921e"
  },
  circle: {
    width: 34,
    height: 34,
    borderRadius: "50%",
    backgroundColor: "none",
    border: "3px solid #f6921e",
  },
  completed: {
    width: 34,
    height: 34,
    borderRadius: "50%",
    backgroundColor: "#f6921e",
    // border: "3px solid #f6921e"
  },
});

// misahin antara class aktif dan non aktif
function CustomStepIcon(props) {
  const classes = StepIconStyles();
  const { active, completed } = props;

  return (
    <div
      className={clsx(classes.root, {
        [classes.active]: active,
      })}
    >
      {completed ? (
        <div className={classes.completed} />
      ) : (
        <div className={classes.circle} />
      )}
    </div>
  );
}

CustomStepIcon.propTypes = {
  /**
   * Whether this step is active.
   */
  active: PropTypes.bool,
  /**
   * Mark the step as completed. Is passed to child components.
   */
  completed: PropTypes.bool,
};

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
  },
  button: {
    marginRight: theme.spacing(1),
  },
  instructions: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
  },
}));

function getSteps() {
  return ["Select campaign settings", "Create an ad group", "Create an ad", "faf", "jkjk"];
}

export default function CustomizedSteppers() {
  const classes = useStyles();
  const [activeStep, setActiveStep] = React.useState(0);
  const steps = getSteps();

  const handleNext = () => {
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  const handleReset = () => {
    setActiveStep(0);
  };

  return (
    <div className={classes.root}>
      <Stepper
        alternativeLabel
        activeStep={activeStep}
        connector={<CustomConnector />}
      >
        {steps.map((label) => (
          <Step key={label}>
            <StepLabel StepIconComponent={CustomStepIcon}>
              {/* {label} */}
            </StepLabel>
          </Step>
        ))}
      </Stepper>

      {/* Button Bawah */}
      <div>
        {activeStep === steps.length ? (
          <div>
            <Typography className={classes.instructions}>
              All steps completed - you&apos;re finished
            </Typography>
            <Button onClick={handleReset} className={classes.button}>
              Reset
            </Button>
          </div>
        ) : (
          <div>
            {/* <Typography className={classes.instructions}>
              {getStepContent(activeStep)}
            </Typography> */}
            <div>
              <Button
                disabled={activeStep === 0}
                onClick={handleBack}
                className={classes.button}
              >
                Back
              </Button>
              <Button
                variant="contained"
                color="primary"
                onClick={handleNext}
                className={classes.button}
              >
                {activeStep === steps.length - 1 ? "Finish" : "Next"}
              </Button>
            </div>
          </div>
        )}
      </div>
    </div>
  );
}
