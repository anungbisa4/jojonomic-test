import React from "react";
import { makeStyles } from "@material-ui/core/styles";

// import style material ui
import {Grid, Box, Paper, Typography} from "@material-ui/core/";
import StarOutlined from "@material-ui/icons/StarOutlined";
// end import style material ui

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 400,
    flexGrow: 1,
  },
  contentCard: {
    width: "100%",
  },
  header: {
    display: "flex",
    alignItems: "center",
    height: 50,
    justifyContent: "center",
    backgroundColor: "transparent",
    marginBottom: 20,
  },
  img: {
    height: 255,
    maxWidth: 400,
    overflow: "hidden",
    display: "block",
    width: "100%",
    border: "3px solid #F6921E",
  },
  styleIconStar: {
    color: "#F6921E",
    verticalAlign: "text-bottom",
  },
  labelStyleRating: {
    color: "#ffffff",
    textAlign: "center",
  },
  labelStyleName: {
    color: "#ffffff",
    textAlign: "center",
    fontFamily: "Montserrat",
    fontWeight: 700,
    fontSize: 25,
  },
  labelTop: {
    color: "#000000",
    fontWeight: 700,
    fontSize: 20,
    fontFamily: "Montserrat",
  },
  labelBottom: {
    color: "#000000",
    fontWeight: 400,
    fontSize: 14,
    fontFamily: "Montserrat",
  },
}));

export default function InfoPartner(props) {
  const classes = useStyles();

  var pilihan = (pilihan, item) => {
    if (pilihan == "talent") {
      return renderInfoTalent(item);
    } else if (pilihan == "influencer") {
      return renderInfoInfluencer(item);
    } else if (pilihan == "production") {
      return renderInfoProduction(item);
    } else if (pilihan == "venue") {
      return renderInfoVenue(item);
    }
  };

  var renderInfoTalent = (item) => {
    return (
      <Box>
        {item.data.map((categories) => {
          if (categories.title.toLowerCase() == props.type) {
            return categories.items.map((member) => {
              if (member.id == props.id) {
                return (
                  <div>
                    <Grid container justify="center" alignItem="center">
                      <div className={classes.root}>
                        <Paper square elevation={0} className={classes.header}>
                          <Grid container alignitem="center" direction="column">
                            <Grid item>
                              <Typography className={classes.labelStyleName}>
                                {member.name}
                              </Typography>
                            </Grid>
                            <Grid item>
                              <Typography className={classes.labelStyleRating}>
                                <StarOutlined
                                  className={classes.styleIconStar}
                                />
                                {member.rating}({member.voters})
                              </Typography>
                            </Grid>
                          </Grid>
                        </Paper>
                        <img
                          className={classes.img}
                          src={member.profilePicture}
                          alt={member.name}
                        />
                      </div>
                    </Grid>
                    <Box maxWidth={370} pt={2} style={{ margin: "auto" }}>
                      <Grid
                        container
                        justify="space-between"
                        direction="row"
                        alignitem="center"
                      >
                        <Grid item xs={2}>
                          <Typography className={classes.labelTop}>
                            Job
                          </Typography>
                        </Grid>
                        <Grid item xs={4}>
                          <Typography
                            className={classes.labelTop}
                            style={{ textAlign: "right" }}
                          >
                            Speciality
                          </Typography>
                        </Grid>
                      </Grid>
                      <Grid
                        container
                        justify="space-between"
                        direction="row"
                        alignitem="center"
                      >
                        <Grid item>
                          <Typography className={classes.labelBottom}>
                            {member.job}
                          </Typography>
                        </Grid>
                        <Grid item>
                          <Typography className={classes.labelBottom}>
                            {member.speciality}
                          </Typography>
                        </Grid>
                      </Grid>
                    </Box>
                  </div>
                );
              }
            });
          }
        })}
      </Box>
    );
  };

  var renderInfoInfluencer = (item) => {
    return (
      <Box>
        {item.data.map((categories) => {
          if (categories.title.toLowerCase() == props.type) {
            return categories.items.map((member) => {
              if (member.id == props.id) {
                return (
                  <div>
                    <Grid container justify="center" alignItem="center">
                      <div className={classes.root}>
                        <Paper square elevation={0} className={classes.header}>
                          <Grid container alignitem="center" direction="column">
                            <Grid item>
                              <Typography className={classes.labelStyleName}>
                                {member.name}
                              </Typography>
                            </Grid>
                            <Grid item>
                              <Typography className={classes.labelStyleRating}>
                                <StarOutlined
                                  className={classes.styleIconStar}
                                />
                                {member.rating}({member.voters})
                              </Typography>
                            </Grid>
                          </Grid>
                        </Paper>
                        <img
                          className={classes.img}
                          src={member.profilePicture}
                          alt={member.name}
                        />
                      </div>
                    </Grid>
                    <Box maxWidth={370} pt={2} style={{ margin: "auto" }}>
                      <Grid
                        container
                        justify="space-between"
                        direction="row"
                        alignitem="center"
                      >
                        <Grid item xs={2}>
                          <Typography className={classes.labelTop}>
                            Job
                          </Typography>
                        </Grid>
                        <Grid item xs={4}>
                          <Typography
                            className={classes.labelTop}
                            style={{ textAlign: "right" }}
                          >
                            Speciality
                          </Typography>
                        </Grid>
                      </Grid>
                      <Grid
                        container
                        justify="space-between"
                        direction="row"
                        alignitem="center"
                      >
                        <Grid item>
                          <Typography className={classes.labelBottom}>
                            {member.job}
                          </Typography>
                        </Grid>
                        <Grid item>
                          <Typography className={classes.labelBottom}>
                            {member.speciality}
                          </Typography>
                        </Grid>
                      </Grid>
                    </Box>
                  </div>
                );
              }
            });
          }
        })}
      </Box>
    );
  };

  var renderInfoProduction = (item) => {
    return (
      <Box>
        {item.data.map((categories) => {
          if (categories.title.toLowerCase() == props.type) {
            return categories.items.map((member) => {
              if (member.id == props.id) {
                return (
                  <div>
                    <Grid container justify="center" alignItem="center">
                      <Box className={classes.root}>
                        <Paper square elevation={0} className={classes.header}>
                          <Grid container alignitem="center" direction="column">
                            <Grid item>
                              <Typography className={classes.labelStyleName}>
                                {member.name}
                              </Typography>
                            </Grid>
                            <Grid item>
                              <Typography className={classes.labelStyleRating}>
                                <StarOutlined
                                  className={classes.styleIconStar}
                                />
                                {member.rating}({member.voters})
                              </Typography>
                            </Grid>
                          </Grid>
                        </Paper>

                        <img
                          className={classes.img}
                          src={member.profilePicture}
                          alt={member.name}
                        />
                      </Box>
                    </Grid>
                    <Box maxWidth={370} pt={2} style={{ margin: "auto" }}>
                      <Grid
                        container
                        justify="space-between"
                        direction="row"
                        alignitem="center"
                      >
                        <Grid item xs={2}>
                          <Typography className={classes.labelTop}>
                            {member.cuisine}
                          </Typography>
                        </Grid>
                        {/* <Grid item xs={4}>
                            <Typography
                              className={classes.labelTop}
                              style={{ textAlign: "right" }}
                            >
                              Speciality
                            </Typography>
                          </Grid> */}
                      </Grid>
                      <Grid
                        container
                        justify="space-between"
                        direction="row"
                        alignitem="center"
                      >
                        <Grid item>
                          <Typography className={classes.labelBottom}>
                            {member.location}
                          </Typography>
                        </Grid>
                        <Grid item>
                          <Typography className={classes.labelBottom}>
                            {member.rate}
                          </Typography>
                        </Grid>
                      </Grid>
                    </Box>
                  </div>
                );
              }
            });
          }
        })}
      </Box>
    );
  };

  var renderInfoVenue = (item) => {
    return (
      <Box>
        {item.data.map((categories) => {
          if (categories.title.toLowerCase() == props.type) {
            return categories.items.map((member) => {
              if (member.id == props.id) {
                return (
                  <div>
                    <Grid container justify="center" alignItem="center">
                      <Box className={classes.root}>
                        <Paper square elevation={0} className={classes.header}>
                          <Grid container alignitem="center" direction="column">
                            <Grid item>
                              <Typography className={classes.labelStyleName}>
                                {member.name}
                              </Typography>
                            </Grid>
                            <Grid item>
                              <Typography className={classes.labelStyleRating}>
                                <StarOutlined
                                  className={classes.styleIconStar}
                                />
                                {member.rating}({member.voters})
                              </Typography>
                            </Grid>
                          </Grid>
                        </Paper>

                        <img
                          className={classes.img}
                          src={member.profilePicture}
                          alt={member.name}
                        />
                      </Box>
                    </Grid>
                    <Box maxWidth={370} pt={2} style={{ margin: "auto" }}>
                      <Grid
                        container
                        justify="space-between"
                        direction="row"
                        alignitem="center"
                      >
                        <Grid item xs={2}>
                          <Typography className={classes.labelTop}>
                            {member.type}
                          </Typography>
                        </Grid>
                        {/* <Grid item xs={4}>
                            <Typography
                              className={classes.labelTop}
                              style={{ textAlign: "right" }}
                            >
                              Speciality
                            </Typography>
                          </Grid> */}
                      </Grid>
                      <Grid
                        container
                        justify="space-between"
                        direction="row"
                        alignitem="center"
                      >
                        <Grid item>
                          <Typography className={classes.labelBottom}>
                            {member.location}
                          </Typography>
                        </Grid>
                        <Grid item>
                          <Typography className={classes.labelBottom}>
                            {member.rate}
                          </Typography>
                        </Grid>
                      </Grid>
                    </Box>
                  </div>
                );
              }
            });
          }
        })}
      </Box>
    );
  };

  return <div>{pilihan(props.type, props.item)}</div>;
}
