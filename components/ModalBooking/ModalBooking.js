import React from "react";
import { withStyles, makeStyles } from "@material-ui/core/styles";
import Box from "@material-ui/core/Box";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";

// import Dialog Modal
import Dialog from "@material-ui/core/Dialog";
import MuiDialogTitle from "@material-ui/core/DialogTitle";
import MuiDialogContent from "@material-ui/core/DialogContent";

// import Component Support
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import TextField from "@material-ui/core/TextField";
import TextArea from "@material-ui/core/TextareaAutosize";

// import Icon
import CloseIcon from "@material-ui/icons/Close";

// import Field Component
import DropdownField from "../ModalBooking/DropdownField/DropdownField";
import DatePicker from "../ModalBooking/DatePicker/DatePicker";
import TimePicker from "../ModalBooking/TimePicker/TimePicker";
import Disclaimer from "../ModalBooking/Disclaimer/Disclaimer";

const styles = (theme) => ({
  root: {
    margin: 0,
    paddingTop: theme.spacing(2),
    paddingLeft: theme.spacing(5),
    paddingRight: theme.spacing(5),
  },
  closeButton: {
    position: "absolute",
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: "#000000",
  },
});

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  padding: {
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(2),
  },
  styleTitle: {
    color: "#000000",
    fontSize: 27,
    fontFamily: "Montserrat",
    fontWeight: 700,
    lineHeight: 1.2,
  },
  styleLabel: {
    fontFamily: "Montserrat",
    fontSize: 14,
    fontWeight: 500,
    marginBottom: -10,
  },

  styleLabel2: {
    fontFamily: "Montserrat",
    fontSize: 14,
    fontWeight: 500,
    marginBottom: 5,
  },
  styleLabel3: {
    fontFamily: "Montserrat",
    fontSize: 12,
    fontWeight: 500,
    marginBottom: -10,
  },
  styleBtnDefault: {
    width: 200,
    height: 35,
    backgroundColor: "#F6921E",
    color: "#ffffff",
    fontFamily: "Montserrat",
    fontWeight: 700,
    borderRadius: 10,
    marginTop: theme.spacing(2),
    marginRight: 20,
    "&:hover": {
      backgroundColor: "#F6921E",
      borderColor: "none",
      boxShadow: "none",
      color: "none",
    },
    "&:active": {
      boxShadow: "none",
      backgroundColor: "#F6921E",
      borderColor: "none",
    },
  },
  styleBtnOutline: {
    width: 200,
    height: 35,
    border: "1px solid #F6921E",
    color: "#F6921E",
    backgroundColor: "#FFFFFF",
    fontFamily: "Montserrat",
    fontWeight: 700,
    borderRadius: 10,
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(3),
    "&:hover": {
      backgroundColor: "#F6921E",
      borderColor: "none",
      boxShadow: "none",
      color: "#ffffff",
    },
    "&:active": {
      boxShadow: "none",
      backgroundColor: "#F6921E",
      borderColor: "none",
    },
  },
  // style button tambah item
  styleBtnAlternate: {
    height: 28,
    backgroundColor: "#F6921E",
    color: "#ffffff",
    fontFamily: "Montserrat",
    fontWeight: 500,
    fontSize: 30,
    borderRadius: 10,
    marginTop: theme.spacing(2),
    marginRight: 20,
    "&:hover": {
      backgroundColor: "#F6921E",
      borderColor: "none",
      boxShadow: "none",
      color: "none",
    },
    "&:active": {
      boxShadow: "none",
      backgroundColor: "#F6921E",
      borderColor: "none",
    },
  },
}));

const DialogTitle = withStyles(styles)((props) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton
          aria-label="close"
          className={classes.closeButton}
          onClick={onClose}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles((theme) => ({
  root: {
    paddingTop: 0,
    paddingLeft: theme.spacing(5),
    paddingRight: theme.spacing(5),
    border: 0,
  },
}))(MuiDialogContent);

const CustomTextField = withStyles({
  root: {
    "& label.Mui-focused": {
      color: "#FFAC33",
    },
    "& .MuiInput-underline:after": {
      borderBottomColor: "#FFAC33",
    },
    "& .MuiOutlinedInput-root": {
      "& fieldset": {
        width: "27.5ch",
        borderColor: "#000000",
      },
      "&:hover fieldset": {
        borderColor: "#FFAC33",
      },
      "&.Mui-focused fieldset": {
        borderColor: "#FFAC33",
      },
    },
  },
})(TextField);

export default function ModalBooking(props) {
  const classes = useStyles();

  var pilihan = (pilihan) => {
    if (pilihan == "talent") {
      return renderBookingTalent();
    } else if (pilihan == "influencer") {
      return renderBookingInfluencer();
    } else if (pilihan == "production") {
      return renderBookingProduction();
    } else if (pilihan == "venue") {
      return renderBookingVenue();
    }
  };

  var renderBookingTalent = () => {
    return (
      <Dialog open={props.openModal}>
        <DialogTitle
          id="detail-partner-dialog-title"
          onClose={props.handleClickCloseModal}
        >
          <Box mt={4}>
            <Grid
              container
              direction="column"
              alignItems="center"
              className={classes.padding}
            >
              <Grid item className={classes.styleTitle}>
                Pesan
              </Grid>
              <Grid item className={classes.styleTitle}>
                Daffa Kiel
              </Grid>
            </Grid>
          </Box>
        </DialogTitle>
        <DialogContent>
          <form noValidate autoComplete="off">
            <Grid container direction="column" spacing={2}>
              {/* Textfield */}
              <Grid item>
                <Grid container direction="column" spacing={2}>
                  {/* Textfield row 1 */}
                  <Grid item>
                    <Grid
                      container
                      direction="row"
                      justify="space-between"
                      spacing={3}
                    >
                      <Grid item xs={12} sm={6}>
                        <Box className={classes.fieldWrapper}>
                          <Typography className={classes.styleLabel}>
                            Nama Acara
                          </Typography>
                          <CustomTextField
                            //   id="custom-css-standard-input"
                            placeholder="Nama Acara"
                            size="small" // vertical height
                            margin="normal" //dense //none
                            // label="Nama Lengkap"
                            variant="outlined"
                          />
                        </Box>
                      </Grid>
                      <Grid item xs={12} sm={6}>
                        <Typography className={classes.styleLabel}>
                          Lokasi Acara
                        </Typography>
                        <DropdownField type="talent" />
                      </Grid>
                    </Grid>
                  </Grid>
                  {/* Textfield row 2 */}
                  <Grid item>
                    <Grid
                      container
                      direction="row"
                      justify="space-between"
                      spacing={3}
                    >
                      <Grid item xs={12} sm={6}>
                        <Typography className={classes.styleLabel}>
                          Tanggal
                        </Typography>
                        <DatePicker />
                      </Grid>
                      <Grid item xs={12} sm={6}>
                        <Typography className={classes.styleLabel}>
                          Waktu
                        </Typography>
                        <TimePicker />
                      </Grid>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
              {/* Text Area */}
              <Grid item>
                <Typography className={classes.styleLabel2}>
                  Alamat Acara
                </Typography>
                <TextArea rowsMin={4} style={{ width: "100%" }} />
              </Grid>
              <Grid item>
                <Typography className={classes.styleLabel2}>
                  Catatan untuk Partner
                </Typography>
                <TextArea rowsMin={4} style={{ width: "100%" }} />
              </Grid>
              {/* Disclaimer and Button */}
              <Grid item>
                <Grid container direction="column" spacing={0}>
                  <Grid item>
                    <Disclaimer />
                  </Grid>
                  <Grid item>
                    <Grid container direction="row" justify="center">
                      <Button className={classes.styleBtnDefault}>Pesan</Button>
                      <Button className={classes.styleBtnOutline}>Batal</Button>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </form>
        </DialogContent>
      </Dialog>
    );
  };

  var renderBookingInfluencer = () => {
    return (
      <Dialog open={props.openModal}>
        <DialogTitle
          id="detail-partner-dialog-title"
          onClose={props.handleClickCloseModal}
        >
          <Box mt={4}>
            <Grid
              container
              direction="column"
              alignItems="center"
              className={classes.padding}
            >
              <Grid item className={classes.styleTitle}>
                Pesan
              </Grid>
              <Grid item className={classes.styleTitle}>
                {`Daffa Kiel`}
              </Grid>
            </Grid>
          </Box>
        </DialogTitle>
        <DialogContent>
          <form noValidate autoComplete="off">
            <Grid container direction="column" spacing={2}>
              {/* Textfield */}
              <Grid item>
                <Grid container direction="column" spacing={2}>
                  {/* Textfield row 1 */}
                  <Grid item>
                    <Grid
                      container
                      direction="row"
                      justify="space-between"
                      spacing={3}
                    >
                      <Grid item xs={12} sm={6}>
                        <Box className={classes.fieldWrapper}>
                          <Typography className={classes.styleLabel}>
                            Apa yang ingin Dipromosikan
                          </Typography>
                          <CustomTextField
                            //   id="custom-css-standard-input"
                            placeholder="Barang Promosi"
                            size="small" // vertical height
                            margin="normal" //dense //none
                            // label="Nama Lengkap"
                            variant="outlined"
                          />
                        </Box>
                      </Grid>
                      <Grid item xs={12} sm={6}>
                        <Typography className={classes.styleLabel}>
                          Tanggal Post
                        </Typography>
                        <DatePicker />
                      </Grid>
                    </Grid>
                  </Grid>
                  {/* Textfield row 2 */}
                  <Grid item>
                    <Grid
                      container
                      direction="row"
                      justify="space-between"
                      spacing={3}
                    >
                      <Grid item xs={12}>
                        <Typography className={classes.styleLabel}>
                          Tipe Post
                        </Typography>
                        <DropdownField fullWidth type="influencer" />
                      </Grid>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
              {/* Text Area */}
              <Grid item>
                <Typography className={classes.styleLabel2}>
                  Detail Barang
                </Typography>
                <TextArea rowsMin={4} style={{ width: "100%" }} />
              </Grid>
              <Grid item>
                <Typography className={classes.styleLabel2}>
                  Apa yang harus {`Daffa Kiel`} lakukan
                </Typography>
                <TextArea rowsMin={4} style={{ width: "100%" }} />
              </Grid>
              <Grid item>
                <Typography className={classes.styleLabel2}>
                  Catatan untuk {`Daffa Kiel`}
                </Typography>
                <TextArea rowsMin={4} style={{ width: "100%" }} />
              </Grid>
              <Grid item>
                <Typography className={classes.styleLabel2}>
                  Upload Gambar
                </Typography>
                <TextArea rowsMin={4} style={{ width: "100%" }} />
              </Grid>
              {/* Disclaimer and Button */}
              <Grid item>
                <Grid container direction="column" spacing={0}>
                  <Grid item>
                    <Disclaimer />
                  </Grid>
                  <Grid item>
                    <Grid container direction="row" justify="center">
                      <Button className={classes.styleBtnDefault}>Pesan</Button>
                      <Button className={classes.styleBtnOutline}>Batal</Button>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </form>
        </DialogContent>
      </Dialog>
    );
  };

  var renderBookingProduction = () => {
    return (
      <Dialog open={props.openModal}>
        <DialogTitle
          id="detail-partner-dialog-title"
          onClose={props.handleClickCloseModal}
        >
          <Box mt={4}>
            <Grid
              container
              direction="column"
              alignItems="center"
              className={classes.padding}
            >
              <Grid item className={classes.styleTitle}>
                Pesan
              </Grid>
              <Grid item className={classes.styleTitle}>
                {`Primarasa Catering`}
              </Grid>
            </Grid>
          </Box>
        </DialogTitle>
        <DialogContent>
          <form noValidate autoComplete="off">
            <Grid container direction="column" spacing={2}>
              {/* Textfield */}
              <Grid item>
                <Grid container direction="column" spacing={2}>
                  {/* Textfield row 1 */}
                  <Grid item>
                    <Grid
                      container
                      direction="row"
                      justify="space-between"
                      spacing={3}
                    >
                      <Grid item xs={12} sm={6}>
                        <Box className={classes.fieldWrapper}>
                          <Typography className={classes.styleLabel}>
                            Nama Acara
                          </Typography>
                          <CustomTextField
                            //   id="custom-css-standard-input"
                            placeholder="Nama Lengkap"
                            size="small" // vertical height
                            margin="normal" //dense //none
                            // label="Nama Lengkap"
                            variant="outlined"
                          />
                        </Box>
                      </Grid>
                      <Grid item xs={12} sm={6}>
                        <Typography className={classes.styleLabel}>
                          Lokasi Acara
                        </Typography>
                        <DropdownField type="production-1" />
                      </Grid>
                    </Grid>
                  </Grid>
                  {/* Textfield row 2 */}
                  <Grid item>
                    <Grid
                      container
                      direction="row"
                      justify="space-between"
                      spacing={3}
                    >
                      <Grid item xs={12} sm={6}>
                        <Typography className={classes.styleLabel}>
                          Waktu Mulai
                        </Typography>
                        <TimePicker />
                      </Grid>
                      <Grid item xs={12} sm={6}>
                        <Typography className={classes.styleLabel}>
                          Waktu Selesai
                        </Typography>
                        <TimePicker />
                      </Grid>
                    </Grid>
                  </Grid>
                  {/* Textfield row 3 */}
                  <Grid item>
                    <Grid
                      container
                      direction="row"
                      justify="space-between"
                      spacing={3}
                    >
                      <Grid item xs={12} sm={6}>
                        <Typography className={classes.styleLabel}>
                          Barang yang Diinginkan
                        </Typography>
                        <DropdownField type="production-2" />
                      </Grid>
                      <Grid item xs={12} sm={6}>
                        <Typography className={classes.styleLabel}>
                          Jumlah Pemesanan
                        </Typography>
                        <CustomTextField
                          //   id="custom-css-standard-input"
                          placeholder="Jumlah Pemesanan"
                          size="small" // vertical height
                          margin="normal" //dense //none
                          // label="Nama Lengkap"
                          variant="outlined"
                        />
                      </Grid>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
              {/* Tambah Item */}
              <Grid item>
                <Typography
                  className={classes.styleLabel3}
                  style={{ textAlign: "center" }}
                >
                  Tambah Item
                </Typography>
                <Button fullWidth className={classes.styleBtnAlternate}>
                  +
                </Button>
              </Grid>
              {/* Text Area */}
              <Grid item>
                <Typography className={classes.styleLabel2}>
                  Alamat Acara
                </Typography>
                <TextArea rowsMin={4} style={{ width: "100%" }} />
              </Grid>
              <Grid item>
                <Typography className={classes.styleLabel2}>
                  Catatan untuk Partner
                </Typography>
                <TextArea rowsMin={4} style={{ width: "100%" }} />
              </Grid>
              {/* Disclaimer and Button */}
              <Grid item>
                <Grid container direction="column" spacing={0}>
                  <Grid item>
                    <Disclaimer />
                  </Grid>
                  <Grid item>
                    <Grid container direction="row" justify="center">
                      <Button className={classes.styleBtnDefault}>Pesan</Button>
                      <Button className={classes.styleBtnOutline}>Batal</Button>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </form>
        </DialogContent>
      </Dialog>
    );
  };

  var renderBookingVenue = () => {
    return (
      <Dialog open={props.openModal}>
        <DialogTitle
          id="detail-partner-dialog-title"
          onClose={props.handleClickCloseModal}
        >
          <Box mt={4}>
            <Grid
              container
              direction="column"
              alignItems="center"
              className={classes.padding}
            >
              <Grid item className={classes.styleTitle}>
                Pesan
              </Grid>
              <Grid item className={classes.styleTitle}>
                {`Big Leaf Garden`}
              </Grid>
            </Grid>
          </Box>
        </DialogTitle>
        <DialogContent>
          <form noValidate autoComplete="off">
            <Grid container direction="column" spacing={2}>
              {/* Textfield */}
              <Grid item>
                <Grid container direction="column" spacing={2}>
                  {/* Textfield row 1 */}
                  <Grid item>
                    <Grid
                      container
                      direction="row"
                      justify="space-between"
                      spacing={3}
                    >
                      <Grid item xs={12} sm={6}>
                        <Box className={classes.fieldWrapper}>
                          <Typography className={classes.styleLabel}>
                            Nama Acara
                          </Typography>
                          <CustomTextField
                            //   id="custom-css-standard-input"
                            placeholder="Nama Lengkap"
                            size="small" // vertical height
                            margin="normal" //dense //none
                            // label="Nama Lengkap"
                            variant="outlined"
                          />
                        </Box>
                      </Grid>
                      <Grid item xs={12} sm={6}>
                        <Box className={classes.fieldWrapper}>
                          <Typography className={classes.styleLabel}>
                            Jumlah Undangan
                          </Typography>
                          <CustomTextField
                            //   id="custom-css-standard-input"
                            placeholder="Jumlah Undangan"
                            size="small" // vertical height
                            margin="normal" //dense //none
                            // label="Nama Lengkap"
                            variant="outlined"
                          />
                        </Box>
                      </Grid>
                    </Grid>
                  </Grid>
                  {/* Textfield row 2 */}
                  <Grid item>
                    <Grid
                      container
                      direction="row"
                      justify="space-between"
                      spacing={3}
                    >
                      <Grid item xs={12} sm={6}>
                        <Typography className={classes.styleLabel}>
                          Ruangan yang ingin Dipesan
                        </Typography>
                        <DropdownField type="venue" />
                      </Grid>
                      <Grid item xs={12} sm={6}>
                        <Typography className={classes.styleLabel}>
                          Tanggal Acara
                        </Typography>
                        <DatePicker />
                      </Grid>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
              {/* Text Area */}
              <Grid item>
                <Typography className={classes.styleLabel2}>
                  Catatan untuk Partner
                </Typography>
                <TextArea rowsMin={4} style={{ width: "100%" }} />
              </Grid>
              {/* Disclaimer and Button */}
              <Grid item>
                <Grid container direction="column" spacing={0}>
                  <Grid item>
                    <Disclaimer />
                  </Grid>
                  <Grid item>
                    <Grid container direction="row" justify="center">
                      <Button className={classes.styleBtnDefault}>Pesan</Button>
                      <Button className={classes.styleBtnOutline}>Batal</Button>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </form>
        </DialogContent>
      </Dialog>
    );
  };

  return <div>{pilihan(props.type)}</div>;
}
