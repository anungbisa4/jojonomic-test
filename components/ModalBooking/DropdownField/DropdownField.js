import React from "react";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import Autocomplete from "@material-ui/lab/Autocomplete";
import TextField from "@material-ui/core/TextField";

const locations = [
  { value: "Jakarta" },
  { value: "Bekasi" },
  { value: "Depok" },
  { value: "Tangerang" },
  { value: "Surabaya" },
];

const posts = [
  { value: "Instagram Post" },
  { value: "Instagram Story" },
  { value: "Tweet" },
  { value: "Video Tiktok" },
];

const items = [
  { value: "Nasi Goreng" },
  { value: "Sop Buntut" },
  { value: "Es Krim" },
  { value: "Es Campur" },
  { value: "Es Buah" },
];

const rooms = [
  { value: "Wedding Ceremony" },
  { value: "Meeting Room" },
  { value: "Mini Studio" },
  { value: "Hall A" },
  { value: "Hall B" },
];

const useStyles = makeStyles((theme) => ({
  root: {
    "& .MuiTextField-root": {
      marginTop: 16,
      // width: "31ch",
      borderColor: "black",
    },
  },
}));

const CustomTextField = withStyles({
  root: {
    "& label.Mui-focused": {
      color: "#FFAC33",
    },
    "& .MuiInput-underline:after": {
      borderBottomColor: "#FFAC33",
    },
    "& .MuiOutlinedInput-root": {
      "& fieldset": {
        // width: "27ch",
        width: "100%",
        borderColor: "#000000",
      },
      "&:hover fieldset": {
        borderColor: "#FFAC33",
      },
      "&.Mui-focused fieldset": {
        borderColor: "#FFAC33",
      },
    },
  },
})(TextField);

export default function DropdownField(props) {
  const classes = useStyles();

  var pilihan = (item) => {
    if (item === "talent") {
      return locations;
    } else if (item === "influencer") {
      return posts;
    } else if (item === "production-1") {
      return locations;
    } else if (item === "production-2") {
      return items;
    } else if (item === "venue") {
      return rooms;
    }
  };

  return (
    <form className={classes.root} noValidate autoComplete="off">
      <div>
        <Autocomplete
          id="combo-box-demo"
          options={pilihan(props.type)} //locations ///posts
          getOptionLabel={(option) => option.value}
          renderInput={(params) => (
            <CustomTextField
              {...params}
              placeholder="Pilihan"
              // label="Cari Lokasi"
              variant="outlined"
              size="small"
            />
          )}
          size="small"
          fullWidth
          style={{ marginBottom: 5 }}
        />
      </div>
    </form>
  );
}
