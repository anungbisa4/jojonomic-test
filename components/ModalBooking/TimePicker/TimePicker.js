import "date-fns";
import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import DateFnsUtils from "@date-io/date-fns";
import AccessTimeIcon from "@material-ui/icons/AccessTime";
import {
  MuiPickersUtilsProvider,
  KeyboardTimePicker,
} from "@material-ui/pickers";

const useStyles = makeStyles((theme) => ({
  borderTime: {
    "& label.Mui-focused": {
      color: "#FFAC33",
    },
    "& .MuiInput-underline:after": {
      borderBottomColor: "#FFAC33",
    },
    "& .MuiOutlinedInput-root": {
      "& fieldset": {
        // width: "28ch",
        borderColor: "#000000",
      },
      "&:hover fieldset": {
        borderColor: "#FFAC33",
      },
      "&.Mui-focused fieldset": {
        borderColor: "#FFAC33",
      },
    },
  },
}));

export default function TimePicker() {
  const classes = useStyles();

  const [selectedDate, setSelectedDate] = React.useState(new Date());

  const handleTimeChange = (date) => {
    setSelectedDate(date);
  };

  return (
    <MuiPickersUtilsProvider utils={DateFnsUtils}>
      <KeyboardTimePicker
        className={classes.borderTime}
        margin="normal"
        size="small"
        fullWidth
        inputVariant="outlined"
        value={selectedDate}
        onChange={handleTimeChange}
        keyboardIcon={<AccessTimeIcon />}
      />
    </MuiPickersUtilsProvider>
  );
}
