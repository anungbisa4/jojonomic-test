import { Dialog, Transition } from "@headlessui/react";
import { Fragment } from "react";
import Image from "next/image";
import CroosIcon from "@/components/Icons/CroosIcon";
import Button from "@/components/Button";
import ListBox from "pages/signup/ListBox";
import TextField from "../TextField";

const dataBank = [{ nama: "bni" }, { nama: "divi" }];

const Modal = (props) => {
  return (
    <>
      <Transition appear show={props.show} as={Fragment}>
        <Dialog
          as="div"
          className="fixed inset-0 z-10 overflow-y-auto"
          open={props.isOpen}
          onClose={props.closeModal}
        >
          <div className="min-h-screen px-4 text-center">
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0"
              enterTo="opacity-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100"
              leaveTo="opacity-0"
            >
              <Dialog.Overlay className="fixed inset-0" />
            </Transition.Child>

            {/* This element is to trick the browser into centering the modal contents. */}
            <span
              className="inline-block h-screen align-middle"
              aria-hidden="true"
            >
              &#8203;
            </span>
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0 scale-95"
              enterTo="opacity-100 scale-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100 scale-100"
              leaveTo="opacity-0 scale-95"
            >
              <div className="inline-block w-full h-full max-w-[900px] max-h-[575px] xs:py-5 xs:px-3 shadow-2xl lg:py-12 lg:px-20 overflow-hidden align-middle transition-all transform bg-white rounded-lg">
                <div className="flex justify-between">
                  <p className="font-bold lg:text-4xl">Tambah Rekening Bank</p>
                  <button
                    type="button"
                    className="bg-transparent text-black"
                    onClick={props.closeModal}
                  >
                    <CroosIcon className="xs:w-3 lg:w-5" />
                  </button>
                </div>
                <div className="flex flex-col gap-5 text-left">
                  <ListBox
                    label="Nama Bank"
                    // listOptions={}
                    placeholder="Pilih Bank"
                    labelClass="font-bold"
                  />
                  <TextField
                    label="Nomor Rekening"
                    placeholder="Masukkan Nomor Rekening"
                    labelClass="font-bold"
                  />
                  <TextField
                    label="Nama Pemilik Rekening"
                    placeholder="Masukkan Nama Pemilik Rekening"
                    labelClass="font-bold"
                  />
                  <Button text="simpan" color="primary-bg" className="p-1" />
                </div>
              </div>
            </Transition.Child>
          </div>
        </Dialog>
      </Transition>
    </>
  );
};

export default Modal;
