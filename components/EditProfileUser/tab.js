import { Tab } from "@headlessui/react";
import { useState } from "react";
import ProfileCircle from "@/components/Icons/ProfileCircle";
import BankCard from "@/components/Icons/BankCard";
import AddRekening from "@/components/Icons/AddRekening";
import Link from "next/link";
import TextField from "../TextField";
import Button from "../Button";
import Modal from "./modal";

function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}

export default function tab() {
  let [isOpen, setIsOpen] = useState(false);

  const closeModal = () => {
    return setIsOpen(false);
  };

  const openModal = () => {
    return setIsOpen(true);
  };
  return (
    <>
      <Modal show={isOpen} isOpen={isOpen} closeModal={closeModal} />
      <Tab.Group>
        <Tab.List className="flex gap-5">
          <Tab>
            {({ selected }) => (
              <div
                className={classNames(
                  "flex gap-1",
                  selected ? "text-primary-orange underline" : " text-black"
                )}
              >
                <span>
                  <ProfileCircle />
                </span>
                Akun
              </div>
            )}
          </Tab>
          <Tab className="flex gap-1">
            {({ selected }) => (
              <div
                className={classNames(
                  "flex gap-1",
                  selected ? "text-primary-orange underline" : " text-black"
                )}
              >
                <span>
                  <BankCard />
                </span>
                Rekening Bank
              </div>
            )}
          </Tab>
        </Tab.List>
        <Tab.Panels>
          <Tab.Panel className="pt-[53px]">
            <div className="box-border xs:px-2 xs:py-2 md:px-5 md:py-4 lg:px-24 lg:py-14 h-full border-2 border-transparent bg-primary-white rounded-lg shadow-xl">
              <div className="flex flex-1 h-full items-center xs:justify-center md:justify-start lg:justify-start">
                <div className="flex h-full">
                  <div className="flex gap-3 items-center xs:justify-center md:justify-start lg:justify-start ">
                    <img
                      className="xs:max-h-[60px] sm:max-h-[70px] md:max-h-[50px] lg:max-h-[80px] xl:max-h-[80px] h-full rounded-full"
                      src="/pictprofile.png"
                      alt="image"
                    />
                    <div className="flex-col pt-2">
                      <h1 className="font-bold xs:text-[8px] md:text-xs lg:text-lg xl:text-lg">
                        David
                      </h1>
                      <Link href="/">
                        <p className="font-bold xs:text-center xs:text-[8px] text-sm md:text-xs text-primary-orange">
                          Ganti Foto Profil
                        </p>
                      </Link>
                    </div>
                  </div>
                </div>
              </div>
              <div className="grid xs:grid-cols-1 md:grid-cols-2 lg:grid-cols-2 xs:gap-3 md:gap-8 lg:gap-10 pt-8">
                <div className="grid gap-3">
                  <TextField
                    label="Nama Lengkap"
                    labelClass="font-bold"
                    placeholder="Masukkan Nama Lengkap"
                  />
                  <TextField
                    labelClass="font-bold"
                    label="Nama Pengguna"
                    placeholder="Masukkan Nama Pengguna"
                  />
                </div>
                <div className="grid gap-3">
                  <TextField
                    labelClass="font-bold"
                    type="email"
                    label="Email"
                    placeholder="Masukkan Email"
                  />
                  <TextField
                    labelClass="font-bold"
                    prefixPhone
                    label="Nomor Telepon"
                    placeholder="Masukkan Nomor Telepon"
                  />
                </div>
              </div>
              <h3 className="font-bold pt-8">Ubah Password</h3>
              <div className="grid xs:grid-cols-1 md:grid-cols-2 lg:grid-cols-2 xs:gap-3 md:gap-8 lg:gap-10 pt-6">
                <TextField
                  labelClass="font-bold"
                  placeholder="Masukan Kata Sandi"
                  label="Kata Sandi"
                  type="password"
                  autoComplete="new-password"
                />

                <TextField
                  labelClass="font-bold"
                  placeholder="Masukan Konfirmasi Kata Sandi"
                  label="Konfirmasi Kata Sandi"
                  type="password"
                  autoComplete="new-password"
                />
              </div>
              <div className="flex pt-12 justify-end">
                <Button text="Simpan" color="primary-bg" className="w-36 p-1" />
              </div>
            </div>
          </Tab.Panel>
          <Tab.Panel className="pt-[57px]">
            <div className="border-box border-transparent border-2 rounded-lg h-[588px] shadow-lg">
              <div className="flex flex-col px-2 gap-3 h-full w-full items-center justify-center">
                <AddRekening />
                <p className="font-bold lg:text-4xl">
                  Belum ada rekening yang disimpan
                </p>
                <p className="lg:text-lg text-center">
                  Ayo, tambah rekening bank-mu agar lebih mudah dalam transaksi
                </p>
                <Button
                  text="Tambah Rekening"
                  color="primary-bg"
                  className="max-w-[265px] p-1"
                  onClick={openModal}
                />
              </div>
            </div>
          </Tab.Panel>
        </Tab.Panels>
      </Tab.Group>
    </>
  );
}
