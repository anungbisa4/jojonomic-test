import "date-fns";
import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import DateFnsUtils from "@date-io/date-fns";
import AccessTimeIcon from "@material-ui/icons/AccessTime";
import {
  MuiPickersUtilsProvider,
  KeyboardTimePicker,
} from "@material-ui/pickers";

const useStyles = makeStyles((theme) => ({
  borderTime: {
    width: 117,
    "& label.Mui-focused": {
      color: "#FFAC33",
    },
    "& .MuiInput-underline:after": {
      borderBottomColor: "#FFAC33",
    },
    "& .MuiOutlinedInput-root": {
      padding: 0,
      "& fieldset": {
        borderColor: "#000000",
      },
      "&:hover fieldset": {
        borderColor: "#FFAC33",
      },
      "&.Mui-focused fieldset": {
        borderColor: "#FFAC33",
      },
    },
    "& .MuiButtonBase-root": {
      padding: 0,
      marginLeft: -30,
    },
  },
}));

export default function TimePickerForm(props) {
  const classes = useStyles();

  const [selectedDate, setSelectedDate] = React.useState(null);

  const handleTimeChange = (date) => {
    setSelectedDate(date);
  };

  return (
    <MuiPickersUtilsProvider utils={DateFnsUtils}>
      <KeyboardTimePicker
        className={classes.borderTime}
        margin="normal"
        size="small"
        fullWidth
        inputVariant="outlined"
        placeholder={props.type == "start" ? "Dari" : "Sampai"}
        value={selectedDate}
        onChange={handleTimeChange}
        keyboardIcon={<AccessTimeIcon />}
      />
    </MuiPickersUtilsProvider>
  );
}
