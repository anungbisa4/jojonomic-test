import RatingProfile from "./Rating";
import { PlusIcon } from "@heroicons/react/solid";
import Button from "@/components/Button";
import ModalSuccess from "./ModalSuccess";
import { useState } from "react";

const UlasanCard = () => {
  let [isOpen, setIsOpen] = useState(false);

  const closeModal = () => {
    return setIsOpen(false);
  };

  const openModal = () => {
    return setIsOpen(true);
  };
  return (
    <>
      <ModalSuccess show={isOpen} isOpen={isOpen} closeModal={closeModal} />
      <div className="container-content">
        <div className="border rounded-lg xs:h-full md:h-[420px] lg:h-[420px] max-w-[834px] mx-auto border-black shadow-xl">
          <div className="box-content xs:my-8 md:my-10 lg:mt-[60px] lg:mb-[30px] xs:mx-2 md:mx-12 lg:mx-14">
            <div className="grid xs:grid-cols-1 md:grid-cols-3 lg:grid-cols-3 md:gap-8 lg:gap-9">
              <div className="md:col-span-2 lg:col-span-2">
                <p className="font-bold text-xl">
                  Ulasan untuk Sule Sutisna di Ngelawak Guys
                </p>
                <p className="text-primary-gray text-sm">
                  Order Number: 1299513XXXXX
                </p>
                <p className="text-sm font-bold mt-3">
                  Dipesan pada:
                  <span className="font-normal"> 30 Januari 2021</span>
                </p>
                <p className="text-xs mt-4 mb-3">Rating</p>
                <RatingProfile />
                <p className="text-xs mt-4 mb-3">Ulasan</p>
                <textarea className="resize border-2 border-black rounded-lg w-full h-[98px]"></textarea>
              </div>
              <div className="col-span-1">
                <p className="font-bold text-sm text-center xs:my-3 md:mb-6 lg:mb-10">
                  Tambahkan Foto
                </p>

                <div className="flex w-full border-2 md:h-[230px] lg:h-[230px] rounded-lg border-black items-center justify-center bg-grey-lighter">
                  <label className="w-64 flex flex-col items-center px-4 py-6  text-black tracking-wide uppercase cursor-pointer">
                    <PlusIcon className="w-14 text-primary-orange" />
                    <span className="mt-2 text-base leading-normal">
                      Select a file
                    </span>
                    <input type="file" className="hidden" />
                  </label>
                </div>
                <div className="flex justify-end">
                  <div className="flex w-40">
                    <Button
                      text="Simpan"
                      onClick={openModal}
                      className="mt-3 p-1 bg-primary-orange text-white"
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default UlasanCard;
