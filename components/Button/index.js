// import { ButtonBase } from "@material-ui/core";

const Button = ({
  form,
  children,
  text,
  classText = "",
  color,
  loadingIcon,
  iconPrefix,
  disabled,
  className = "",
  buttonType = "button",
  type,
  iconButton,
  onClick = () => {},
}) => {
  const handleStyle = () => {
    if (color === "primary") {
      return {
        buttonClass:
          "group border-primary-orange active:text-white border-2 hover:bg-primary-orange",
        textClass: "font-bold text-primary-orange group-hover:text-white",
      };
    }
    if (color === "primary-bg") {
      return {
        buttonClass:
          "group border-primary-orange text-white border-2 hover:bg-primary-orange bg-primary-orange",
        textClass: "font-bold text-white group-hover:text-white",
      };
    }
    if (color === "primary-gray") {
      return {
        buttonClass:
          "group border-primary-gray text-white border-2 hover:bg-primary-gray bg-primary-gray",
        textClass: "font-bold text-white group-hover:text-white",
      };
    }
    if (color === "blue") {
      return {
        buttonClass: "bg-[#4285F4] border-[#4285F4] text-white border-2",
        textClass: "font-bold",
      };
    }
    if (color === "red2") {
      return {
        buttonClass: "bg-red-500 border-red-500 text-white border-2",
        textClass: "font-bold",
      };
    }
    if (color === "green") {
      return {
        buttonClass: "bg-green-400 border-green-400 text-white border-2",
        textClass: "font-bold",
      };
    }
    if (color === "gmail") {
      return {
        buttonClass: "bg-white border-white text-gray-500 border-2 shadow-md",
        textClass: "font-bold",
      };
    }
    if (color === "input") {
      return {
        buttonClass:
          "border-primary-orange text-white border-2 hover:bg-primary-orange bg-primary-orange p-1.5 relative",
        textClass: "font-bold text-xs",
      };
    }
    return {
      buttonClass: "border-1",
      textClass: "",
    };
  };

  return (
    <button
      form={form}
      disabled={disabled}
      onClick={onClick}
      type={type || buttonType}
      className={`w-full rounded-md p-2 ${
        handleStyle().buttonClass
      }  outline-none ${className} disabled:cursor-not-allowed disabled:opacity-50 `}
    >
      <div
        className={`flex items-center ${
          !iconButton && "space-x-2"
        } justify-center group`}
      >
        {children || (
          <>
            {iconPrefix}
            {disabled && loadingIcon}
            <span
              className={`text-xs sm:text-sm md:text-base ${
                handleStyle().textClass
              } ${classText}`}
            >
              {iconButton ? "" : text || "button component"}
            </span>
          </>
        )}
      </div>
    </button>
  );
};

export default Button;
