const Breadcrumb = () => {
  return (
    <ul className="flex text-gray-500 xs:gap-0 lg:gap-2">
      <li className="inline-flex items-center xs:gap-0 lg:gap-2 text-primary-orange font-bold text-lg">
        <a href="/" className="xs:text-[10px] md:text-base lg:text-lg">
          Home
        </a>
      </li>
      <li className="inline-flex items-center xs:gap-0 lg:gap-2 text-primary-orange">
        <span className="text-primary-black">/</span>
      </li>
      <li className="inline-flex items-center xs:gap-0 lg:gap-2 text-primary-orange font-bold text-lg">
        <a href="/#" className="xs:text-[10px] md:text-base lg:text-lg">
          Partner
        </a>
      </li>
      <li className="inline-flex items-center xs:gap-0 lg:gap-2 text-primary-orange">
        <span className="text-primary-black">/</span>
      </li>
      <li className="inline-flex items-center xs:gap-0 lg:gap-2 text-primary-orange font-bold text-lg">
        <a href="/#" className="xs:text-[10px] md:text-base lg:text-lg">
          Talent
        </a>
      </li>
      <li className="inline-flex items-center xs:gap-0 lg:gap-2 text-primary-orange">
        <span className="text-primary-black">/</span>
      </li>
      <li className="inline-flex items-center xs:gap-0 lg:gap-2 text-primary-orange font-bold text-lg">
        <a href="/#" className="xs:text-[10px] md:text-base lg:text-lg">
          Penyanyi
        </a>
      </li>
      <li className="inline-flex items-center xs:gap-0 lg:gap-2 text-primary-orange">
        <span className="text-primary-black">/</span>
      </li>
      <li className="inline-flex items-center xs:gap-0 lg:gap-2 text-primary-orange font-bold text-lg">
        <a
          href="/#"
          className="xs:text-[10px]  xs:leading-3 md:text-base lg:text-lg"
        >
          Daffa Kiel
        </a>
      </li>
      <li className="inline-flex items-center xs:gap-0 lg:gap-2 text-primary-orange">
        <span className="text-primary-black">/</span>
      </li>
      <li className="inline-flex items-center xs:gap-0 lg:gap-2 text-primary-black text-lg">
        <a
          href="/#"
          className="xs:text-[10px] xs:leading-3 md:text-base lg:text-lg"
        >
          Pembayaran Partner
        </a>
      </li>
    </ul>
  );
};

export default Breadcrumb;
