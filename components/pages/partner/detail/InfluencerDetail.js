import React, { Fragment } from "react";
import services from "data/DummyContentPartner/DataService.json";
import { Tab } from "@headlessui/react";

export default function InfluencerServiceDetail({ data, type }) {
  console.log(data);
  const handleDetail = () => {
    if (type == "Talent") {
      return {
        title: "Rider",
      };
    }
    if (type == "Influencer") {
      return {
        title: "Detail Tambahan",
      };
    }
    return {
      title: `Alamat ${type}`,
    };
  };
  console.log(services.data);

  return (
    <>
      <Tab.Group>
        <Tab.List>
          <section className="container-fluid h-max bg-gray-200">
            <div className="w-max h-[55px] xl:h-[70px] flex items-center ">
              <Tab as={Fragment}>
                {({ selected }) => (
                  <div
                    className={`h-full flex items-center mr-[60px] text-24-responsive font-bold ${
                      selected ? "border-b-3 border-primary-orange" : "mb-[2px]"
                    }  cursor-pointer`}
                  >
                    Detail
                  </div>
                )}
              </Tab>
              <Tab as={Fragment}>
                {({ selected }) => (
                  <div
                    className={`h-full flex items-center mr-[60px] text-24-responsive font-bold ${
                      selected ? "border-b-3 border-primary-orange" : "mb-[2px]"
                    }  cursor-pointer`}
                  >
                    Service yang Ditawarkan
                  </div>
                )}
              </Tab>
            </div>
          </section>
        </Tab.List>
        <Tab.Panels>
          <Tab.Panel as={Fragment}>
            <section className="container-fluid pt-7 xl:pt-9 pr-detail-partner xxl:w-[1500px]">
              <div className="pb-4 lg:pb-6 xl:pb-8">
                <h1 className="text-18-responsive font-bold">Deskripsi</h1>
                <p className="text-14-responsive">
                  {data?.data?.partner?.description}
                </p>
              </div>
              <div className="pb-6 lg:pb-10 xl:pb-12">
                <h1 className="text-18-responsive font-bold pb-1">
                  {handleDetail().title}
                </h1>
                {(type == "Talent" || type == "Influencer") && (
                  <ul className="list-disc list-inside text-18-responsive font-bold">
                    {type == "Talent" &&
                      data.riderList.map((rider, key) => {
                        return (
                          <li key={key}>
                            {rider.title}
                            <span className="font-normal">
                              - {rider.content}{" "}
                            </span>
                          </li>
                        );
                      })}
                    {/* {type == "Influencer" &&
                data.partner.map((detail, key) => {
                  return (
                    <li key={key}>
                      {detail.title}
                      <span className="font-normal">- {detail.content} </span>
                    </li>
                  );
                })} */}
                    {type == "Influencer" && (
                      <>
                        <li>
                          Status:{" "}
                          <span className="font-normal">
                            {data?.data?.partner?.status_active}
                          </span>
                        </li>
                        <li>
                          Agama:{" "}
                          <span className="font-normal">
                            {data?.data?.religion?.name}
                          </span>
                        </li>
                      </>
                    )}
                  </ul>
                )}
                {type == "Vendor" ||
                  (type == "Venue" && (
                    <p className="text-14-responsive">
                      {data?.data?.partner?.user?.address}
                    </p>
                  ))}
              </div>
            </section>
          </Tab.Panel>
          <Tab.Panel as={Fragment}>
            <section className="container-fluid pt-7 xl:pt-9 pb-32 xxl:w-[1500px]">
              <hr />
              <div className="grid grid-cols-4 py-4">
                <h1 className="text-20-responsive font-bold">Service</h1>
                <h1 className="text-20-responsive font-bold">Jumlah Post</h1>
                <h1 className="text-20-responsive font-bold">Durasi Post</h1>
                <h1 className="text-20-responsive font-bold">Harga</h1>
              </div>
              <hr />
              <div className="grid grid-cols-4 gap-y-[60px] pt-8">
                {services.data.map((service) => {
                  return (
                    <>
                      <p className="text-18-responsive font-bold">
                        {service.name}
                      </p>
                      <p className="text-18-responsive ">{service.quantity}</p>
                      <p className="text-18-responsive ">{service.timeBound}</p>
                      <p className="text-18-responsive font-bold text-primary-orange">
                        {service.price}
                      </p>
                    </>
                  );
                })}
              </div>
            </section>
          </Tab.Panel>
        </Tab.Panels>
      </Tab.Group>
    </>
  );
}
