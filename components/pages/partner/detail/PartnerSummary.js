import Button from "components/Button";
import {
  ChatBubble,
  HandshakeIcon,
  LocationIcon,
  StarOutlineIcon,
} from "@/components/Icons";
import Link from "next/link";
import { PartnerPrice } from "./utils";
import PriceFormat from "@/components/PriceFormat";
import { useEffect } from "react";
import { apiGet } from "src/utils/fetch";

export default function PartnerSummary({ data, type }) {
  // const price = PartnerPrice(data.price || data.priceMin, data.priceMax, 10);
  const price = data?.price_rate;
  const category = data?.partner?.partner_category;
  // console.log(data, "s");

  const location = apiGet

  // useEffect(() => {
  // },)


  function titleCase(string) {
    return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
  }

  return (
    <div className="w-full h-[320px] lg:h-[380px] xl:h-[483px] md:relative mt-4 md:mt-0">
      <div className="text-center md:text-left">
        <h1 className="text-48-responsive font-bold leading-normal">
          {data?.stage_name || "name here"}
        </h1>
        <h2 className="text-24-responsive leading-none font-bold text-primary-orange mb-3 xl:mb-[18px]">
          {titleCase(category?.type_categories)}
          {` | `}
          {titleCase(category?.group_categories)}
          {` / `}
          {titleCase(category?.genre_categories)}
        </h2>
      </div>
      <div className="ml-2 md:ml-0">
        <p className="text-18-responsive mb-2 line-clamp-5">
          {data?.partner?.description || "desc here"}
        </p>
        <div className="h3-detail-partner">
          <LocationIcon className="icon-h3-detail-partner" />
          {data?.partner?.city || "lokasi"}
        </div>
        <div className="h3-detail-partner ">
          <HandshakeIcon className="icon-h3-detail-partner" />
          Sudah {data?.partner?.rating_count} kali Dipesan
        </div>
        <div className="h3-detail-partner text-primary-orange">
          <StarOutlineIcon className="icon-h3-detail-partner mb-1" />(
          {data?.partner?.rating}){" "}
          <span className="ml-1 text-xs underline cursor-pointer">
            Lihat Semua Ulasan
          </span>
        </div>
        {type == "Talent" && (
          <div className="mb-4 md:mb-[40px] flex items-center">
            <div className="relative mr-2">
              <div className="px-1">
                {/* {price[0]} */}
                <PriceFormat value={price*9/10} />
              </div>
              <div className="border-b-2 border-primary-orange absolute inset-0 bottom-[10px]"></div>
            </div>
            <h3 className="text-24-responsive font-bold text-primary-orange">
              <PriceFormat value={price} />
              {/* {price[1]} */}
            </h3>
          </div>
        )}
        {type == "Influencer" && (
          <div className="mb-4 md:mb-[40px] flex items-center">
            <div className="relative mr-2">
              <div className="px-1">
                Rp 750.000,00
                {/* {price[0]} */}
              </div>
              <div className="border-b-2 border-primary-orange absolute inset-0 bottom-[10px]"></div>
            </div>
            <h3 className="text-24-responsive font-bold text-primary-orange">
              Rp 637.500,00 - Rp 737.500,00
              {/* {price[1]} - {price[2]} */}
            </h3>
          </div>
        )}
      </div>
      <div className="mx-0 w-[calc(100%)] md:w-[calc(85%)] lg:w-[calc(70%)] xl:w-[calc(58%)] xxl:w-[376px] md:absolute bottom-0">
        <div className="flex justify-between">
          <div className="w-[calc(83%)] mr-5">
            <Link href="/partner/talent/1/divi/payment?step=talent-detail">
              <Button color="primary-bg" text="Pesan Sekarang" />
            </Link>
          </div>
          <div className="">
            <Button
              iconButton
              color="primary"
              iconPrefix={
                <ChatBubble className=" w-4 md:w-[24px] text-primary-orange group-hover:text-white" />
              }
            ></Button>
          </div>
        </div>
      </div>
    </div>
  );
}
