import Image from "next/image";
import TextField from "@/components/TextField";
import InputDatePicker from "@/components/DatePicker";
import ListBox from "@/components/ListBox";
import HeaderFormBooking from "../StepperBooking";
import Button from "@/components/Button";
import ImageUpload from "@/components/ImageUpload";
import { TrashIcon } from "@heroicons/react/solid";

import { Grid } from "@material-ui/core";
import { useState } from "react";
import { useForm, useFieldArray } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { schemaValidation, service } from "./utils";
import { useSelector, useDispatch } from "react-redux";
import {
  removePhotoInfluencer,
  stepInfluencer,
  stepPhotoInfluencer,
} from "@/src/redux/booking/action";
import { fetcher } from "src/utils/fetch";
import { ImagePlaceIcon } from "@/components/Icons";
import { useRouter } from "next/router";

const arrList = Array(3).fill(null);

const InfluencerDetail = () => {
  const { booking: partner } = useSelector((state) => state);
  const dispatch = useDispatch();
  const router = useRouter();
  const [images, setImages] = useState(partner?.influencer?.photo || []);

  const addValue = {
    service_id: "",
    // detail: "",
  };

  const defaultValues = {
    influencer_services: partner?.influencer?.influencer_services || [addValue],
    form: {
      event: partner?.influencer?.form?.event || "",
      date: partner?.influencer?.form?.date || "",
      script: partner?.influencer?.form?.script || "",
      detail: partner?.influencer?.form?.detail || "",
      notes: partner?.influencer?.form?.notes || "",
    },
    list: { ...partner?.influencer?.list },
  };

  const {
    register,
    control,
    setValue,
    handleSubmit,
    watch,
    formState: { errors, isValid },
  } = useForm({
    mode: "onBlur",
    resolver: yupResolver(schemaValidation),
    defaultValues,
  });

  console.log(watch());

  const { fields, append, remove } = useFieldArray({
    control,
    name: "influencer_services",
  });

  const handleBooking = (data) => {
    dispatch(stepInfluencer(data));
    router.push("/partner/influencer/1/divi/payment?step=influencer-order");
  };

  const findDetail = (index) => {
    return service.find(
      (e) => e.id === watch("influencer_services")[index].service_id
    )?.detail;
  };

  const handleImage = (item, type, index) => {
    const method = type === "upload" ? "POST" : "DELETE";
    const formData = new FormData();
    if (type === "upload") {
      formData.append("image", item.file, item.file.name);
      fetcher("/auth/upload-image", { method, data: formData })
        .then((res) => {
          dispatch(stepPhotoInfluencer([res.data], index));
        })
        .catch((err) => {
          console.log(err);
        });
    } else {
      dispatch(removePhotoInfluencer(index));
    }
  };

  return (
    <>
      <div className="w-[calc(90%)] md:w-full">
        <HeaderFormBooking title="Detail Acara" activeStep={1} />
      </div>
      <div className="w-full bg-primary-white rounded-2xl shadow-primary py-[54px] px-[43px] ">
        <form id="booking-form" onSubmit={handleSubmit(handleBooking)}>
          <Grid container spacing={2} className="!mb-2">
            <Grid item xs={12} md={6} className="!mb-4">
              <TextField
                label="Apa yang ingin Dipromosikan?"
                placeholder="Tuliskan item yang ingin dipromosikan?"
                name="form.event"
                register={register}
                required
                error={errors}
                statusError={errors?.form?.event}
              />
            </Grid>
            <Grid item xs={12} md={6} className="!mb-4">
              <InputDatePicker
                placeholder="Tanggal Post"
                label="Masukkan Tanggal Post"
                name="form.date"
                control={control}
                required
                error={errors}
                statusError={errors?.form?.date}
                defaultValues={partner?.influencer?.form?.date}
              />
            </Grid>
          </Grid>
          {fields.map((field, index) => {
            return (
              <Grid key={field.id} container spacing={2} className="!mb-1">
                <Grid item xs={12} md={6}>
                  <ListBox
                    label="Service"
                    placeholder="Pilih Service"
                    listOption={service || []}
                    name={`influencer_services.${index}.service_id`}
                    selectedName="name"
                    selectedValue="id"
                    setValue={setValue}
                    control={control}
                    required
                    defaultValues={
                      defaultValues?.list?.influencer_services?.[index]
                        ?.service_id
                    }
                  />
                </Grid>

                <Grid item xs={12} md={6}>
                  <div className="relative">
                    <TextField
                      label="Detail Service"
                      placeholder={findDetail(index)}
                      inputClass="border-gray-400 bg-primary-gray placeholder-white font-bold"
                      disabled
                    />
                    {index > 0 && (
                      <button
                        type="button"
                        onClick={() => remove(index)}
                        className="absolute top-0 right-0"
                      >
                        <TrashIcon className="w-4 text-red-500 cursor-pointer" />
                      </button>
                    )}
                  </div>
                </Grid>
              </Grid>
            );
          })}
          <Grid container spacing={2}>
            <Grid item xs={12} className="!mb-4">
              <Button
                color="primary-bg"
                text="Tambah Service"
                className="mt-3 !rounded-lg"
                onClick={() => append(addValue)}
                classText="!text-xs"
              />
            </Grid>
            <Grid item xs={12} md={6} className="!mb-4">
              <TextField
                label="Apa yang harus Partner Lakukan?"
                placeholder="Jelaskan sesuai script"
                name="form.script"
                register={register}
                type="textarea"
                rows={3}
                required
                error={errors}
                statusError={errors?.form?.script}
              />
            </Grid>
            <Grid item xs={12} md={6} className="!mb-4">
              <div>
                <label className="text-xs">Upload Gambar atau Foto Item</label>
                <div className="w-full">
                  <ImageUpload
                    images={images}
                    setImages={setImages}
                    onUpload={(item, index) =>
                      handleImage(item, "upload", index)
                    }
                    plusButton
                  />
                </div>
              </div>
            </Grid>
          </Grid>
          <Grid container spacing={2} className="!mb-2">
            {arrList.map((itemImage, index) => {
              return (
                <Grid key={index} item xs={12} sm={6} md={4}>
                  <div className="flex items-center justify-center w-full h-[120px] border-2 rounded-lg border-gray-900 overflow-hidden relative">
                    {!images?.[index] ? (
                      <ImagePlaceIcon className="w-12" />
                    ) : (
                      <Image
                        alt="iventori"
                        src={
                          images?.[index]?.url ||
                          images?.[index]?.data_url ||
                          "/logoDark.png"
                        }
                        width={244}
                        height={244}
                        objectFit="cover"
                      />
                    )}
                    {(images?.[index]?.url || images?.[index]?.data_url) && (
                      <ImageUpload
                        images={images}
                        setImages={setImages}
                        iconRemove={
                          <TrashIcon className="absolute top-1 right-1 text-red-500 w-4" />
                        }
                        onRemove={() =>
                          handleImage(images?.[index], "remove", index)
                        }
                      />
                    )}
                  </div>
                </Grid>
              );
            })}
          </Grid>
          <Grid container spacing={2} className="!mb-2">
            <Grid item xs={12} className="!mb-4">
              <TextField
                label="Detail Barang"
                placeholder="Masukkan detail barang yang akan dipromosikan"
                name="form.detail"
                register={register}
                type="textarea"
                required
                error={errors}
                statusError={errors?.form?.detail}
              />
            </Grid>
            <Grid item xs={12} className="!mb-4">
              <TextField
                label="Catatan untuk Partner"
                placeholder="Masukkan Catatan untuk Partner"
                name="form.notes"
                register={register}
                type="textarea"
              />
            </Grid>
          </Grid>
        </form>
      </div>
      <Button
        form="booking-form"
        className="mt-16 px-0 py-4 rounded-[15px]"
        color={!isValid ? "primary-gray" : "primary-bg"}
        type="submit"
        classText="font-bold"
        text="LANJUTKAN"
      />
    </>
  );
};

export default InfluencerDetail;
