import dynamic from "next/dynamic";
import { useRouter } from "next/router";
import Breadcrumb from "@/components/Breadcrumb";
import BookingProfile from "../BookingProfile";

const InfluencerDetail = dynamic(import("./InfluencerDetail"));
const InfluencerOrder = dynamic(import("./InfluencerOrder"));
// const InfluencerVoucher = dynamic(import("./InfluencerVoucher"));

export default function Influencer() {
  const data = {
    name: "Alexandreve",
    price: 1500000,
  };

  const router = useRouter();
  const step = router.query?.step;

  const handleStep = () => {
    if (step === "influencer-detail") {
      return <InfluencerDetail />;
    }
    if (step === "influencer-order") {
      return <InfluencerOrder />;
    }
    // if (step === "influencer-voucher") {
    //   return <InfluencerVoucher />;
    // }
    return <div>loading...</div>;
  };

  return (
    <div className="container-content xs:mt-12 lg:mt-[175px]">
      <Breadcrumb />
      <div className="grid grid-cols-1">
          <BookingProfile type="influencer" data={data} activeStep={step} />
          <div className="mt-10 mb-10">
            <div className="h-auto flex flex-col items-center justify-center">
              {handleStep()}
            </div>
          </div>
      </div>
    </div>
  );
}
