import dateFormat from "dateformat";

const OrderDetail = ({ title, value, date }) => {
  return (
    <>
      <h2 className="text-responsive-18 font-bold">{title}</h2>
      <div className="md:col-span-3 flex">
        <p className="mr-6 md:mr-14">:</p>
        <p className="text-responsive-18">
        {date?
        dateFormat(new Date(value), "d/mm/yyyy")
        : value}
        </p>
      </div>
    </>
  );
}

export default OrderDetail;
