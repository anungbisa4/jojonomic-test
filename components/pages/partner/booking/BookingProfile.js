import Link from "next/link";
import Image from "next/image";
import { ArrowIcon, LocationIcon } from "@/components/Icons";
import PriceFormat from "@/components/PriceFormat";

import { useRouter } from "next/router";

const BookingProfile = ({ type, data, activeStep }) => {
  const router = useRouter();

  const handleBack = () => {
    router.back();
  };

  const handleText = () => {
    if (type == "talent") {
      return {
        namaPartner: "Daffa Kiel",
        partner: "Talent",
        detail: "Item",
      };
    }
    if (type == "influencer") {
      return {
        namaPartner: "Alexandreve",
        partner: "Influencer",
        detail: "Detail Tambahan",
      };
    }
    if (type == "venue") {
      return {
        namaPartner: "Gedung Serbaguna",
        partner: "Venue",
        detail: "Detail Ruangan",
      };
    }
    if (type == "vendor") {
      return {
        namaPartner: "Catering Serba Ada",
        partner: "Vendor",
        detail: "Item",
      };
    }
  };

  return (
    <>
      <p className="font-bold xl:text-4xl">
        Pemesanan {data.name || handleText().namaPartner}
      </p>
      {(activeStep != "talent-detail" &&
        activeStep != "influencer-detail" &&
        activeStep != "venue-detail" &&
        activeStep != "vendor-detail") && (
        <div className="flex mt-2 mb-8 cursor-pointer" onClick={handleBack}>
          <ArrowIcon className="w-2 mr-2 text-primary-orange font-bold" />
          <p className="text-18-responsive text-primary-orange font-bold">
            Kembali
          </p>
        </div>
      )}
      {(activeStep === "talent-detail" ||
        activeStep === "influencer-detail" ||
        activeStep === "venue-detail" ||
        activeStep === "vendor-detail") && (
        <p className="xl:text-sm mt-2 mb-8">
          Pastikan semua sudah benar ya :){" "}
        </p>
      )}
      <p className="xl:text-2xl mt-2 font-bold">
        Detail {handleText().partner} yang Dipesan
      </p>
      <div className="grid xs:grid-cols-4 grid-cols-2 justify-between mt-4">
        <div className="text-left xs:col-span-3">
          <div className="flex xs:gap-1 lg:gap-5">
            <div className="relative xs:w-[100px] xs:h-[100px] md:w-[150px] md:h-[150px] lg:w-[150px] lg:h-[150px]">
              <Image
                src="/carousel1.png"
                layout="fill"
                objectFit="cover"
                objectPosition="center"
                className="rounded-lg"
              />
            </div>
            <div className="flex-col ml-1">
              <p className="font-bold text-20-responsive">
                {data.name || handleText().namaPartner}
              </p>
              <PriceFormat
                className="font-bold  text-14-responsive"
                value={data.price || 750000}
              />
              <div className="flex gap-1 mt-2">
                <LocationIcon className="xs:h-3 xs:mt-1 md:mt-1 lg:mt-0 lg:h-5 text-primary-gray" />
                <span className=" xs:text-[8px] lg:text-[10px] mt-1">
                  Jakarta Selatan
                </span>
              </div>
              <Link href="#">
                <p className="xs:text-[8px] lg:text-xs xs:mt-6 md:mt-8 lg:mt-14 text-primary-orange font-bold">
                  Lihat {handleText().detail}
                </p>
              </Link>
            </div>
          </div>
        </div>
        <div className="text-left xs:col-span-1" onClick={()=>router.push("/partner/talent")}>
            <p className="font-bold xs:text-[10px] xl:text-xs text-primary-orange">
              Ganti Partner
            </p>
        </div>
      </div>
    </>
  );
};

export default BookingProfile;
