import CardTab from "../CardTab";
import HeaderFormBooking from "../StepperBooking";

import Data from "data/DummyContentPartner/DataPayment.json";

import { useDispatch, useSelector } from "react-redux";
import { stepTalent } from "@/src/redux/booking/action";
import { useRouter } from "next/router";

const TalentMethod = () => {
  const { booking: partner } = useSelector((state) => state);
  const dispatch = useDispatch();
  const router = useRouter();

  const handleMethod = (id) => {
    const data = {
      method: id,
    };
    dispatch(stepTalent(data));
  };

  const defaultValue = partner?.talent?.method;

  const handleNext = () => {
    router.push("/partner/talent/1/divi/payment?step=talent-voucher");
  };

  return (
    <>
      <div className="w-[calc(90%)] md:w-full">
        <HeaderFormBooking title="Pilih Metode Pembayaran" activeStep={2} />
      </div>
      {/* <form id="booking-form" onSubmit={handleNext}></form> */}
      <CardTab data={Data} handler={handleMethod} defaultValue={defaultValue} />
    </>
  );
};

export default TalentMethod;
