import CardTab from "../CardTab";
import HeaderFormBooking from "../StepperBooking";

import Data from "data/DummyContentPartner/DataPromo.json";

import { useDispatch, useSelector } from "react-redux";
import { stepTalent } from "@/src/redux/booking/action";
import { useRouter } from "next/router";
import { clearBooking } from "src/redux/booking/action";

const TalentVoucher = () => {
  const { booking: partner } = useSelector((state) => state);
  const dispatch = useDispatch();
  const router = useRouter();

  const handleVoucher = (id) => {
    const data = {
      voucher: id,
    };
    dispatch(stepTalent(data));
  };

  const defaultValue = partner?.talent?.voucher;

  const handleNext = () => {
    dispatch(clearBooking());
    router.push("/partner/talent/1/divi/payment?step=talent-detail");
  };

  return (
    <>
      <div className="w-[calc(90%)] md:w-full">
        <HeaderFormBooking
          title="Pilih Voucher Diskonmu"
          activeStep={3}
          note="Klik untuk memilih diskon, Kamu hanya dapat menggunakan 1 Voucher dalam 1 transaksi"
        />
      </div>
      {/* <form id="booking-form" onSubmit={handleNext}></form> */}
      <CardTab
        promo
        data={Data}
        handler={handleVoucher}
        defaultValue={defaultValue}
      />
    </>
  );
};

export default TalentVoucher;
