import TextField from "@/components/TextField";
import InputDatePicker from "@/components/DatePicker";
import ListBox from "@/components/ListBox";
import HeaderFormBooking from "../StepperBooking";
import Button from "@/components/Button";

import { Grid } from "@material-ui/core";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { schemaValidation, timeArr } from "./utils";
import { fetcher } from "src/utils/fetch";
import useSWR from "swr";
import { stepTalent } from "@/src/redux/booking/action";
import { useDispatch, useSelector } from "react-redux";
import { useRouter } from "next/router";

const TalentDetail = () => {
  const { booking: partner } = useSelector((state) => state);
  const dispatch = useDispatch();
  const router = useRouter();

  const defaultValues = {
    form: {
      event: partner?.talent?.form?.event || "",
      date: partner?.talent?.form?.date || "",
      address: partner?.talent?.form?.address || "",
      notes: partner?.talent?.form?.notes || "",
    },
    list: { ...partner?.talent?.list },
  };

  const {
    register,
    handleSubmit,
    setValue,
    control,
    formState: { errors, isValid },
  } = useForm({
    resolver: yupResolver(schemaValidation),
    mode: "onBlur",
    defaultValues,
  });

  const { data: cities } = useSWR("/reference/cities", fetcher);

  const handleBooking = (data) => {
    dispatch(stepTalent(data));
    router.push("/partner/talent/1/divi/payment?step=talent-order");
  };

  return (
    <>
      <div className="w-[calc(90%)] md:w-full">
        <HeaderFormBooking title="Detail Acara" activeStep={1} />
      </div>
      <div className="w-full bg-primary-white rounded-2xl shadow-primary py-[54px] px-[43px]">
        <form id="booking-form" onSubmit={handleSubmit(handleBooking)}>
          <Grid container spacing={2}>
            <Grid item xs={12} md={6} className="!mb-4">
              <TextField
                label="Nama Acara"
                placeholder="Masukkan Nama Acara"
                name="form.event"
                required
                register={register}
                error={errors}
                statusError={errors?.form?.event}
              />
            </Grid>
            <Grid item xs={12} md={6} className="!mb-4">
              <InputDatePicker
                placeholder="Pilih Tanggal Acara"
                label="Tanggal Acara"
                name="form.date"
                control={control}
                required
                error={errors}
                statusError={errors?.form?.date}
                defaultValues={defaultValues.form.date}
              />
            </Grid>
            <Grid item xs={12} md={6} className="!mb-4">
              <ListBox
                label="Waktu Acara"
                placeholder="Masukkan Waktu Acara"
                name="form.time"
                listOption={timeArr || []}
                selectedName="name"
                selectedValue="name"
                setValue={setValue}
                control={control}
                required
                error={errors}
                statusError={errors?.form?.time}
                defaultValues={defaultValues?.list?.form?.time}
              />
            </Grid>
            <Grid item xs={12} md={6} className="!mb-4">
              <ListBox
                label="Lokasi Acara"
                placeholder="Pilih Lokasi Acara"
                name="form.city_id"
                listOption={cities?.data || []}
                selectedName="name"
                selectedValue="id"
                setValue={setValue}
                control={control}
                required
                error={errors}
                statusError={errors?.form?.city_id}
                defaultValues={defaultValues?.list?.form?.city_id}
              />
            </Grid>
            <Grid item xs={12} className="!mb-4">
              <TextField
                label="Alamat Acara"
                placeholder="Masukkan Alamat Acara"
                name="form.address"
                type="textarea"
                required
                register={register}
                error={errors}
                statusError={errors?.form?.address}
              />
            </Grid>
            <Grid item xs={12} className="!mb-4">
              <TextField
                label="Catatan untuk Partner"
                placeholder="Masukkan Catatan untuk Partner"
                name="form.notes"
                type="textarea"
                register={register}
              />
            </Grid>
          </Grid>
        </form>
      </div>
      <Button
        form="booking-form"
        className="mt-16 px-0 py-4 rounded-[15px]"
        color={!isValid ? "primary-gray" : "primary-bg"}
        type="submit"
        classText="font-bold"
        text="LANJUTKAN"
      />
    </>
  );
};

export default TalentDetail;
