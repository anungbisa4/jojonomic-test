import dynamic from "next/dynamic";
import { useRouter } from "next/router";
import Breadcrumb from "@/components/Breadcrumb";
import BookingProfile from "../BookingProfile";
import PaymentSummary from "../PaymentSummary";

const VenueDetail = dynamic(import("./VenueDetail"));
const VenueOrder = dynamic(import("./VenueOrder"));
const VenuePayment = dynamic(import("./VenuePayment"));

export default function Venue() {
  const data = {
    name: "Ruangan ABC",
    price: 4500000,
  };

  const router = useRouter();
  const step = router.query?.step;

  const handleStep = () => {
    if (step === "venue-detail") {
      return <VenueDetail />;
    }
    if (step === "venue-order") {
      return <VenueOrder />;
    }
    if (step === "venue-payment") {
      return <VenuePayment />;
    }
    return <div>loading...</div>;
  };

  return (
    <div className="container-content xs:mt-12 lg:mt-[175px]">
      <Breadcrumb />
      <div className="grid grid-cols-1">
        <BookingProfile type="venue" data={data} activeStep={step} />
        <div className="mt-10 mb-10">
          <div className="h-auto flex flex-col items-center justify-center">
            {handleStep()}
          </div>
        </div>
      </div>
    </div>
  );
}
