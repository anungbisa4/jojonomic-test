import { StarFilled } from "@/components/Icons";
import PictureUlasan from "./PictureUlasan";

const Content = ({ half }) => {
  return (
    <>
      <div className="w-full md:w-[380px] xl:w-[530px] xxl:w-full md:ml-[20px] mr-12 xl:mr-24 mb-[20px] pt-1">
        <div className="flex mb-2 xl:mb-3">
          <StarFilled className="w-[10px] text-orange-500" />
          <StarFilled className="w-[10px] text-orange-500" />
          <StarFilled className="w-[10px] text-orange-500" />
          <StarFilled className="w-[10px] text-orange-500" />
        </div>
        {!half && (
          <div className="text-14-responsive">
            Penampilannya bagus banget garugi deh mesen daffa hehe. Mantap
            pokoknya! Selanjutnya pasti bakal mesen Daffa lg kalo ada acara gini{" "}
          </div>
        )}
      </div>
      {!half && <PictureUlasan className="hidden md:block w-[90px] xl:w-[100px] mb-4" quantity="2" />}
    </>
  );
};

export default Content;
