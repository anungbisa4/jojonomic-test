import React from "react";
import UlasanSummary from "./UlasanSummary";
import UlasanDetail from "./UlasanDetail";

export default function index({ data }) {
  return (
    <section className="container-fluid pr-detail-partner pb-10 md:pb-18 lg:pb-20 xl:pb-28">
      <UlasanSummary />
      <UlasanDetail />
    </section>
  );
}
