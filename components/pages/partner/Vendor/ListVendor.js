import React from 'react'
import ListPartner from "../ListPartner";
import Data from "data/DummyContentPartner/DataTalentAll.json";
// import Data from "data/DummyContentPartner/DataVendorAll.json";

export default function ListVendor() {
    return (
        <ListPartner data={Data} />
    )
}

