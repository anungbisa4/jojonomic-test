import React from 'react'
import ListPartner from "../ListPartner";
import Data from "data/DummyContentPartner/DataTalentAll.json";

export default function ListInfluencer() {
    return (
        <ListPartner data={Data} />
    )
}

