import React from "react";
import Banner from "@/components/Banner/";
import Picture from "../ProfilePicture";
import Summary from "../detail/PartnerSummary";
import Detail from "../detail/PartnerDetailDesc";
import Ulasan from "../ulasan";
// import Data from "data/DummyContentPartner/DataTalentAll.json";
import Data from "data/DummyContentPartner/DataPartner.json";
import { useRouter } from "next/router";
import useSWR from "swr";
import { apiGet } from "src/utils/fetch";

export default function MainDetailTalent({dataTalentNew}) {
  const router = useRouter();
  console.log(dataTalentNew);

  // const query = router.query;
  // console.log(query);

  // const { data: dataTalentRaw } = useSWR(
  //   `/partners/${query.id_talent}/TALENT`,
  //   apiGet
  // );
  // const dataTalentNew = dataTalentRaw.data.data;
  // console.log(dataTalentNew);

  const dataTalent = Data.items[0];
  // console.log(dataTalent);
  return (
    <>
      <Banner
        imgSrc={dataTalent.imgBanner}
        slug={router.asPath}
        title={dataTalent.name}
        partner={dataTalent.partnerType}
        type={dataTalent.job}
      />
      {/* atas */}
      <section className="container-fluid pt-10 md:pt-[90px] xl:pt-[135px] pb-8 md:pb-[60px] xl:pb-[92px] grid md:flex">
        <Picture data={dataTalent} ButtonText="Lihat Galeri" />
        <Summary data={dataTalentNew} type={dataTalent.partnerType} />
      </section>
      {/* bawah */}
      <Detail data={dataTalentNew} type={dataTalent.partnerType} />
      <Ulasan data={dataTalent} />
    </>
  );
}
