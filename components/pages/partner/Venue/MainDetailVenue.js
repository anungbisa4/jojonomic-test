import React from "react";
import Banner from "@/components/Banner/";
import Picture from "../ProfilePicture";
// import Summary from "./Detail/VenueSummary";
// import Detail from "./Detail/VenueDetailDesc";
import Summary from "../detail/PartnerSummary";
import Detail from "../detail/PartnerDetailDesc";
import Ulasan from "../ulasan";
// import Data from "data/DummyContentPartner/DataTalentAll.json";
import Data from "data/DummyContentPartner/DataPartner.json";
import { useRouter } from "next/router";

export default function MainDetailVenue({ data }) {
  console.log(data);
  const dataVenue = Data.items[1];
  const path = useRouter();

  return (
    <>
      <Banner
        imgSrc={dataVenue.imgBanner}
        slug={path.asPath}
        title={dataVenue.name}
        partner={dataVenue.partnerType}
        type={dataVenue.speciality}
      />
      {/* atas */}
      <section className="container-fluid pt-10 md:pt-[90px] xl:pt-[135px] pb-8 md:pb-[60px] xl:pb-[92px] grid md:flex">
        <Picture data={dataVenue} ButtonText="Lihat Seluruh Item" />
        <Summary data={data} type={dataVenue.partnerType} />
      </section>
      {/* bawah */}
      <Detail data={data} type={dataVenue.partnerType} />
      <Ulasan data={dataVenue} />
    </>
  );
}
