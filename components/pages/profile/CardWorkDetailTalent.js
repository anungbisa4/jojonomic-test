import Button from "@/components/Button";

const CardWorkDetailTalent = () => {
  return (
    <div className="h-[calc(600px)] my-18 md:h-auto md:my-0">
      <div className="w-[calc(80%)] xl:w-[1155px] m-8 pl-5 pr-1 p-6 md:p-6 lg:p-8 xl:p-[55px] bg-white  relative rounded-lg drop-shadow-md">
        <div className="md:w-[calc(100%-24%)] grid grid-cols-2 gap-2 lg:gap-4 xl:gap-8">
          {/* <div className="w-[calc(100%-24.4%)] grid grid-cols-4 gap-8"> */}
          <div>
            <h2 className="text-20-responsive">Nama Acara</h2>
            <h1 className="text-24-responsive font-bold">Malam Rap</h1>
          </div>
          <div>
            <h2 className="text-20-responsive">Penyewa</h2>
            <div className="flex items-center">
              <div className="w-4 h-4 lg:w-6 lg:h-6 xl:w-8 xl:h-8 rounded-full bg-primary-green "></div>
              <h1 className="text-24-responsive font-bold ml-1 lg:ml-2 xl:ml-4">
                Dan James
              </h1>
            </div>
          </div>
          <div className="col-span-2">
            <h2 className="text-20-responsive">Waktu</h2>
            <h1 className="text-24-responsive font-bold">
              Senin, 6 September 2021
            </h1>
          </div>
          <div>
            <h2 className="text-20-responsive">Lokasi</h2>
            <h1 className="text-24-responsive font-bold">Kota Bekasi</h1>
          </div>
          <div className="col-span-2 md:col-span-3">
            <h2 className="text-20-responsive">Alamat</h2>
            <h1 className="text-24-responsive font-bold">
              Jl. Pahlawan III no 14, Bekasi Timur, Kota Bekasi, Jawa Barat
            </h1>
          </div>
          <div className="col-span-2 md:col-span-4">
            <h2 className="text-20-responsive">Catatan untuk Kamu</h2>
            <h1 className="text-24-responsive font-bold">
              Pakai kaos merah polos ya
            </h1>
          </div>
        </div>
        <div className="w-full h-[200px] md:w-[calc(24.4%)] md:h-full pt-8 md:pt-8 lg:pt-[55px] mt-4 md:mt-0 px-6 bg-gray-100 rounded-b-lg md:rounded-r-lg absolute md:top-0 right-0 text-center">
          <div className="h-full relative">
            <h3 className="md:mb-4 lg:mb-0 px-4 md:px-0 text-18-responsive font-bold">
              Pilih Tindakan untuk Pekerjaan Ini
            </h3>
            <div className="flex flex-col w-full md:static lg:absolute mt-6 md:mt-0 md:bottom-[55px]">
              <Button
                color="green"
                text="Terima Pekerjaan"
                className="mb-4"
                classText="md:leading-tight md:text-[10px] lg:text-xs"
              />
              <Button
                color="red2"
                text="Tolak Pekerjaan"
                className=""
                classText="md:leading-tight md:text-[10px] lg:text-xs"
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default CardWorkDetailTalent;
