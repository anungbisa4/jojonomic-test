import CardSignup from "./CardSignup";
import Button from "components/Button";
import Image from "@/components/Image";
import { useRouter } from "next/router";
import { CheckCircleIcon } from "@heroicons/react/outline"

const SuccessSignup = () => {
  const router = useRouter()

  const handleLink = (href) => {
    router.push(href);
  };

  const status = router.query?.status

  return (
    <section className="relative sm:w-[500px] font-pop m-auto mt-10">
      <div className="w-[244px] m-auto mb-18">
        <Image
          alt="iventori"
          src="/logoDark.png"
          width={244}
          height={79}
          bgTransparent
        />
      </div>
      <CardSignup className="!pb-12">
        <div className="space-y-2 text-center">
          <div className="flex items-center justify-center">
            <h1 className="text-base xs:text-xl sm:text-2xl font-bold">
              Daftar Sebagai
            </h1>
          </div>
          <p className="font-semibold text-xs sm:text-sm mt-2">
            Selamat bergabung menjadi
          </p>
          <h3 className="font-medium text-xs sm:text-sm">
            <span className="capitalize">{status || "User"}</span> di Eventori
          </h3>
        </div>
        <div>
          <CheckCircleIcon className="w-7/12 m-auto my-8 text-primary-orange" />
        </div>
        <div className="sm:w-8/12 m-auto">
          <Button
            onClick={() => handleLink("/")}
            color="primary"
            text="Ke Halaman Utama"
          />
        </div>
      </CardSignup>
    </section>
  );
};

export default SuccessSignup;
