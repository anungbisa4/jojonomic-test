import TextField from "components/TextField";
import CardSignup from "../CardSignup";
import { useRouter } from "next/router";
import Grid from "@material-ui/core/Grid";
import HeaderPartner from "../HeaderPartner";
import InputDatePicker from "../InputDate";
import ListBox from "pages/signup/ListBox";

import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { schemaValidationOne } from "./utils";
import { useSelector, useDispatch } from "react-redux";
import { stepOneTalent } from "@/redux/signup/action";
import { fetcher } from "src/utils/fetch";
import useSWR from "swr";

const genre = [{ name: "Laki-laki" }, { name: "Perempuan" }];

const TalentInfo = () => {
  const { signup: partner } = useSelector((state) => state);
  const dispatch = useDispatch();
  const router = useRouter();

  const handleLink = (href) => {
    router.push(href);
  };
  const { data: categoriesPartner } = useSWR(
    "/reference/partner_categories",
    fetcher
  );

  const defaultValues = {
    user: {
      name: partner?.talent?.stepOne?.user?.name || "",
      username: partner?.talent?.stepOne?.user?.username || "",
      email: partner?.talent?.stepOne?.user?.email || "",
      password: partner?.talent?.stepOne?.user?.password || "",
      confirm_password: partner?.talent?.stepOne?.user?.confirm_password || "",
    },
    talent: {
      stage_name: partner?.talent?.stepOne?.talent?.stage_name || "",
      date_of_birth: partner?.talent?.stepOne?.talent?.date_of_birth || null,
    },
    partner: {
      gender: partner?.talent?.stepOne?.partner?.gender || "",
      category_id: partner?.talent?.stepOne?.partner?.category_id || "",
    },
    list: { ...partner?.talent?.stepOne?.list },
  };

  const {
    register,
    handleSubmit,
    setValue,
    control,
    formState: { errors, isValid, isDirty },
  } = useForm({
    resolver: yupResolver(schemaValidationOne),
    mode: "onBlur",
    defaultValues,
  });

  console.log(errors, isValid, isDirty);

  const handleStepRegister = (data) => {
    dispatch(stepOneTalent(data));
    handleLink("/signup/talent?step=talent-rate");
  };

  return (
    <section className="flex items-center justify-center font-pop text-center  min-h-[calc(100vh-9rem)]">
      <CardSignup className="mt-36 xs:px-6 xs:w-[400px] sm:w-[500px] md:w-[670px] lg:w-[800px] md:px-8">
        <HeaderPartner title="Talent" activeStep={1} />
        <form
          className="mt-8 space-y-4 mb-12 w-full"
          onSubmit={handleSubmit(handleStepRegister)}
        >
          <Grid container spacing={2}>
            <Grid item xs={12} md={4} className="text-left !mb-4">
              <TextField
                placeholder="Masukan Nama Lengkap"
                label="Nama Lengkap"
                name="user.name"
                required
                register={register}
                error={errors}
                statusError={errors?.user?.name}
              />
            </Grid>
            <Grid item xs={12} md={4} className="text-left !mb-4">
              <TextField
                placeholder="Masukan Nama Panggung"
                label="Nama Panggung"
                name="talent.stage_name"
                required
                register={register}
                error={errors}
                statusError={errors?.talent?.stage_name}
              />
            </Grid>
            <Grid item xs={12} md={4} className="text-left !mb-4">
              <TextField
                placeholder="Masukan Nama Pengguna"
                label="Nama Pengguna"
                name="user.username"
                required
                register={register}
                error={errors}
                statusError={errors?.user?.username}
              />
            </Grid>
            <Grid item xs={12} md={4} className="text-left !mb-4">
              <InputDatePicker
                placeholder="Pilih Tanggal Lahir"
                label="Tanggal Lahir"
                name="talent.date_of_birth"
                required
                control={control}
                error={errors}
                statusError={errors?.talent?.date_of_birth}
                defaultValues={defaultValues.talent.date_of_birth}
              />
            </Grid>
            <Grid item xs={12} md={4} className="text-left !mb-4">
              <ListBox
                label="Jenis Kelamin"
                placeholder="Pilih jenis kelamin"
                listOption={genre || []}
                name="partner.gender"
                selectedName="name"
                selectedValue="name"
                setValue={setValue}
                required
                control={control}
                error={errors}
                statusError={errors?.partner?.gender}
                defaultValues={defaultValues?.list?.partner?.gender}
              />
            </Grid>
            <Grid item xs={12} md={4} className="text-left !mb-4">
              <ListBox
                label="Kategori"
                placeholder="Pilih Kategori"
                listOption={categoriesPartner?.data || ""}
                name="partner.category_id"
                selectedName="name_categories"
                selectedValue="id"
                setValue={setValue}
                control={control}
                error={errors}
                statusError={errors?.partner?.category_id}
                defaultValues={defaultValues?.list?.partner?.category_id}
                required
              />
            </Grid>
            <Grid item xs={12} md={4} className="text-left !mb-4">
              <TextField
                placeholder="Masukan Email"
                label="Email"
                name="user.email"
                required
                register={register}
                error={errors}
                statusError={errors?.user?.email}
              />
            </Grid>
            <Grid item xs={12} md={4} className="text-left !mb-4">
              <TextField
                placeholder="Masukan Kata Sandi"
                label="Kata Sandi"
                name="user.password"
                required
                register={register}
                error={errors}
                statusError={errors?.user?.password}
                type="password"
                autoComplete="new-password"
              />
            </Grid>
            <Grid item xs={12} md={4} className="text-left !mb-4">
              <TextField
                placeholder="Masukkan Konfirmasi Sandi"
                label="Konfirmasi Kata Sandi"
                name="user.confirm_password"
                required
                register={register}
                error={errors}
                statusError={errors?.user?.confirm_password}
                type="password"
                autoComplete="new-password"
              />
            </Grid>
          </Grid>
          <div className="flex justify-end">
            <button
              type="submit"
              className="text-primary-orange font-semibold text-right active:opacity-70"
            >
              Selanjutnya
            </button>
          </div>
        </form>
      </CardSignup>
    </section>
  );
};

export default TalentInfo;
