import dynamic from "next/dynamic";
import { useState } from "react";
import TextField from "components/TextField";
import CardSignup from "../CardSignup";
import { useRouter } from "next/router";
import Grid from "@material-ui/core/Grid";
import HeaderPartner from "../HeaderPartner";
import Button from "components/Button";
import { TrashIcon } from "@heroicons/react/outline";
import { useForm, useFieldArray } from "react-hook-form";
import { useSelector, useDispatch } from "react-redux";
import { stepRiderTalent } from "@/redux/signup/action";

const SocialMediaDialog = dynamic(() => import("../SocialMediaDialog"));

const TalentRate = () => {
  const { signup: partner } = useSelector((state) => state);
  const dispatch = useDispatch();
  const [openDialog, setOpenDialog] = useState(false);
  const router = useRouter();

  const handleLink = (href) => {
    router.push(href);
  };

  const addValue = {
    details: "",
    name: "",
  };
  const defaultValues = partner?.talent?.stepRider ||  {
    talent_riders: [
      {
        details: "",
        name: "",
      },
    ],
  };

  const { register, control, handleSubmit, setValue } = useForm({
    defaultValues,
  });
  const { fields, append, remove } = useFieldArray({
    control,
    name: "talent_riders",
  });

  const handleRemove = (index) => {
    if (index > 0) {
      remove(index);
    }
  };
  const handleStepRegister = (data) => {
    dispatch(stepRiderTalent(data));
    handleLink("/signup/verification/email?type=talent")
  };

  return (
    <section className="flex items-center justify-center font-pop text-center  min-h-[calc(100vh-9rem)]">
      <CardSignup className="mt-36 xs:px-6 xs:w-[400px] sm:w-[500px] md:w-[670px] lg:w-[800px] md:px-8">
        <HeaderPartner title="Talent" activeStep={4} />
        <form
          className="mt-8 space-y-4 mb-12 w-full"
          onSubmit={handleSubmit(handleStepRegister)}
        >
          <div className="flex justify-between items-center">
            <p className="font-bold">Rider Pemesananmu</p>
          </div>
          {fields.map((field, index) => {
            return (
              <Grid key={field.id} container spacing={2}>
                <Grid item xs={12} md={6} className="text-left !mb-4">
                  <TextField
                    placeholder="Masukkan Nama Rider"
                    label="Nama Rider"
                    name={`talent_riders.${index}.name`}
                    register={register}
                  />
                </Grid>
                <Grid item xs={11} md={5} className="text-left !mb-4">
                  <TextField
                    placeholder="Masukan Detail Rider"
                    label="Detail Rider"
                    name={`talent_riders.${index}.details`}
                    register={register}
                  />
                </Grid>
                <Grid item xs={1} className="text-left !mb-4 self-end">
                  <button type="button" onClick={() => handleRemove(index)}>
                    <TrashIcon className="w-8 text-red-500 cursor-pointer" />
                  </button>
                </Grid>
              </Grid>
            );
          })}
          <Grid item xs={12} className="text-left !mb-4">
            <div className="">
              <Button
                type="button"
                onClick={() => append(addValue)}
                className="!p-1.5 relative !rounded-xl"
                classText="!text-xs"
                color="primary-bg"
                text="Tambah Rider"
              />
            </div>
          </Grid>
          <div className="flex justify-end">
            <button
              type="submit"
              className="text-primary-orange font-semibold text-right active:opacity-70"
            >
              Selanjutnya
            </button>
          </div>
        </form>
      </CardSignup>
      <SocialMediaDialog open={openDialog} setOpen={setOpenDialog} />
    </section>
  );
};

export default TalentRate;
