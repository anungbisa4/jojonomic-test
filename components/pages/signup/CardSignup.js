const CardSignup = ({ children, title, description, step, className }) => {
  return (
    <div
      className={`py-4 px-7 xs:py-8 xs:px-8 border-1 rounded-xl border-black shadow-primary min-w-[300px] ${className}`}
    >
      {title && description && (
        <div>
          <h1 className="text-3xl">{title}</h1>
        </div>
      )}
      {children}
    </div>
  );
};

export default CardSignup