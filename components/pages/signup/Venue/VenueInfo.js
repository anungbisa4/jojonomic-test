import dynamic from "next/dynamic";
import { useState } from "react";
import TextField from "components/TextField";
import CardSignup from "../CardSignup";
import { useRouter } from "next/router";
import Grid from "@material-ui/core/Grid";
import HeaderPartner from "../HeaderPartner";
import ListBox from "pages/signup/ListBox";
import { PlusIcon } from "@heroicons/react/solid";

import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { schemaValidationOne } from "./utils";
import { useSelector, useDispatch } from "react-redux";
import { stepOneVenue, stepSocialMedia } from "@/redux/signup/action";
import { fetcher } from "src/utils/fetch";
import useSWR from "swr";

const SocialMediaDialog = dynamic(() => import("../SocialMediaDialog"));

const VenueInfo = () => {
  const { signup: partner } = useSelector((state) => state);
  const dispatch = useDispatch();
  const router = useRouter();

  const [openDialog, setOpenDialog] = useState(false);

  const handleLink = (href) => {
    router.push(href);
  };
  const { data: cities } = useSWR("/reference/cities", fetcher);
  const { data: categoriesPartner} = useSWR(
    "/reference/partner_categories",
    fetcher
  );

  const defaultValues = {
    user: {
      name: partner?.venue?.stepOne?.user?.name || "",
      username: partner?.venue?.stepOne?.user?.username || "",
      email: partner?.venue?.stepOne?.user?.email || "",
      password: partner?.venue?.stepOne?.user?.password || "",
      confirm_password: partner?.venue?.stepOne?.user?.confirm_password || "",
      phone_number: partner?.venue?.stepOne?.user?.phone_number || "",
    },
    partner: {
      category_id: partner?.venue?.stepOne?.partner?.category_id || "",
      city_id: partner?.venue?.stepOne?.partner?.city_id || "",
    },
    list: { ...partner?.venue?.stepOne?.list },
  };

  const {
    register,
    handleSubmit,
    setValue,
    control,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schemaValidationOne),
    mode: "onBlur",
    defaultValues,
  });

  const handleStepRegister = (data) => {
    dispatch(stepOneVenue(data));
    handleLink("/signup/venue?step=venue-about");
  };

  return (
    <section className="flex items-center justify-center font-pop text-center  min-h-[calc(100vh-9rem)]">
      <CardSignup className="mt-36 xs:px-6 xs:w-[400px] sm:w-[500px] md:w-[670px] lg:w-[800px] md:px-8">
        <HeaderPartner title="Venue" activeStep={1} />
        <form
          className="mt-8 space-y-4 mb-12 w-full"
          onSubmit={handleSubmit(handleStepRegister)}
        >
          <Grid container spacing={2}>
            <Grid item xs={12} md={4} className="text-left !mb-4">
              <TextField
                placeholder="Masukan Nama Lengkap"
                label="Nama Lengkap"
                name="user.name"
                required
                register={register}
                error={errors}
                statusError={errors?.user?.name}
              />
            </Grid>
            <Grid item xs={12} md={4} className="text-left !mb-4">
              <TextField
                placeholder="Masukan Nama Pengguna"
                label="Nama Pengguna"
                name="user.username"
                required
                register={register}
                error={errors}
                statusError={errors?.user?.username}
              />
            </Grid>
            <Grid item xs={12} md={4} className="text-left !mb-4">
              <TextField
                placeholder="Masukan Nomor Telepon"
                label="Nomor Telepon"
                name="user.phone_number"
                register={register}
                prefixPhone
                required
                error={errors}
                statusError={errors?.user?.phone_number}
              />
            </Grid>
            <Grid item xs={12} md={4} className="text-left !mb-4">
              <TextField
                placeholder="Masukan Email"
                label="Email"
                name="user.email"
                required
                register={register}
                error={errors}
                statusError={errors?.user?.email}
              />
            </Grid>
            <Grid item xs={12} md={4} className="text-left !mb-4">
              <ListBox
                label="Kategori"
                placeholder="Pilih Kategori"
                listOption={categoriesPartner?.data || []}
                name="partner.category_id"
                selectedName="name_categories"
                selectedValue="id"
                setValue={setValue}
                control={control}
                error={errors}
                statusError={errors?.partner?.category_id}
                defaultValues={defaultValues?.list?.partner?.category_id}
                required
              />
            </Grid>
            <Grid item xs={12} md={4} className="text-left !mb-4">
              <ListBox
                label="Domisili"
                placeholder="Pilih Domisili"
                listOption={cities?.data || []}
                name="partner.city_id"
                selectedName="name"
                selectedValue="id"
                setValue={setValue}
                required
                control={control}
                error={errors}
                statusError={errors?.partner?.city_id}
                defaultValues={defaultValues?.list?.partner?.city_id}
              />
            </Grid>
            <Grid item xs={12} md={4} className="text-left !mb-4">
              <TextField
                placeholder="Masukan Kata Sandi"
                label="Kata Sandi"
                name="user.password"
                required
                register={register}
                error={errors}
                statusError={errors?.user?.password}
                type="password"
                autoComplete="new-password"
              />
            </Grid>
            <Grid item xs={12} md={4} className="text-left !mb-4">
              <TextField
                placeholder="Masukkan Konfirmasi Sandi"
                label="Konfirmasi Kata Sandi"
                name="user.confirm_password"
                required
                register={register}
                error={errors}
                statusError={errors?.user?.confirm_password}
                type="password"
                autoComplete="new-password"
              />
            </Grid>
            <Grid item xs={12} md={4} className="text-left !mb-4">
              <div className={`space-y-1 text-xs flex flex-col`}>
                <label>Akun Sosial Media</label>
                <button
                  onClick={() => setOpenDialog(!openDialog)}
                  type="button"
                  className="justify-center border-1 p-1 rounded-lg border-primary-orange w-full bg-primary-orange focus:outline-none focus:border-primary-orange flex items-center active:opacity-70"
                >
                  <PlusIcon className="text-white w-6" />
                </button>
              </div>
            </Grid>
          </Grid>
          <div className="flex justify-end">
            <button
              type="submit"
              className="text-primary-orange font-semibold text-right active:opacity-70"
            >
              Selanjutnya
            </button>
          </div>
        </form>
      </CardSignup>
      <SocialMediaDialog
        open={openDialog}
        setOpen={setOpenDialog}
        dispatchSocMed={(data) => dispatch(stepSocialMedia(data, 'venue'))}
        listValue={null}
      />
    </section>
  );
};

export default VenueInfo;
