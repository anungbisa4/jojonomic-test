import dynamic from "next/dynamic";
import { useState } from "react"
import TextField from "components/TextField";
import CardSignup from "../CardSignup";
import { useRouter } from "next/router";
import Grid from "@material-ui/core/Grid";
import HeaderPartner from "../HeaderPartner";
import ListBox from "pages/signup/ListBox";

import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { schemaValidationDetail } from "./utils";
import { useSelector, useDispatch } from "react-redux";
import { stepDetailVenue } from "@/redux/signup/action";


const genre = [{ name: "Laki-laki" }, { name: "Perempuan" }];

const VenueDetail = () => {
  const { signup: partner } = useSelector((state) => state);
  const dispatch = useDispatch();
  const router = useRouter();

  const handleLink = (href) => {
    router.push(href);
  };

  const defaultValues = {
    venue: {
      pic_name: partner?.venue?.stepDetail?.venue?.pic_name || "",
      pic_phone_number: partner?.venue?.stepDetail?.venue?.pic_phone_number || "",
    },
    user: {
      address: partner?.venue?.stepDetail?.user?.address || "",
    },
    partner: {
      gender: partner?.venue?.stepDetail?.partner?.gender || "",
    },
    list: { ...partner?.venue?.stepDetail?.list },
  };

  const {
    register,
    handleSubmit,
    setValue,
    control,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schemaValidationDetail),
    mode: "onBlur",
    defaultValues,
  });

  const handleStepRegister = (data) => {
    dispatch(stepDetailVenue(data));
    handleLink("/signup/venue?step=venue-photo");
  };

  return (
    <section className="flex items-center justify-center font-pop text-center  min-h-[calc(100vh-9rem)]">
      <CardSignup className="mt-36 xs:px-6 xs:w-[400px] sm:w-[500px] md:w-[670px] lg:w-[800px] md:px-8">
        <HeaderPartner title="Venue" activeStep={3} />
        <form onSubmit={handleSubmit(handleStepRegister)} className="mt-8 space-y-4 mb-12 w-full">
          <Grid container spacing={2}>
            <Grid item xs={12} md={4} className="text-left !mb-4">
              <TextField
                placeholder="Masukkan Nama PIC"
                label="Nama PIC"
                name="venue.pic_name"
                register={register}
                required
                error={errors}
                statusError={errors?.venue?.pic_name}
              />
            </Grid>
            <Grid item xs={12} md={4} className="text-left !mb-4">
              <ListBox
                label="Jenis Kelamin"
                placeholder="Pilih jenis kelamin"
                listOption={genre || []}
                name="partner.gender"
                selectedName="name"
                selectedValue="name"
                setValue={setValue}
                required
                control={control}
                error={errors}
                statusError={errors?.partner?.gender}
                defaultValues={defaultValues?.list?.partner?.gender}
              />
            </Grid>
            <Grid item xs={12} md={4} className="text-left !mb-4">
              <TextField
                placeholder="Masukan Nomor Telepon PIC"
                label="Nomor Telepon PIC"
                name="venue.pic_phone_number"
                register={register}
                prefixPhone
                required
                error={errors}
                statusError={errors?.venue?.pic_phone_number}
              />
            </Grid>
            <Grid item xs={12} className="text-left !mb-4">
              <TextField
                placeholder="Masukan Alamat Sesuai KTP"
                label="Alamat Sesuai KTP"
                required
                name="user.address"
                required
                register={register}
                error={errors}
                statusError={errors?.user?.address}
              />
            </Grid>
          </Grid>
          <div className="flex justify-end">
            <button
              type="submit"
              className="text-primary-orange font-semibold text-right active:opacity-70"
            >
              Selanjutnya
            </button>
          </div>
        </form>
      </CardSignup>
    </section>
  );
};

export default VenueDetail;
