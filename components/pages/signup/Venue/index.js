import { useEffect } from "react";
import dynamic from "next/dynamic";
import { useRouter } from "next/router";

const VenueInfo = dynamic(import("./VenueInfo"));
const VenueAbout = dynamic(import("./VenueAbout"));
const VenueDetail = dynamic(import("./VenueDetail"));
const VenuePhoto = dynamic(import("./VenuePhoto"));
const VenueRider = dynamic(import("./VenueInfo"));

export default function Venue() {
  const router = useRouter();
  const step = router.query?.step;

  if (step === "venue-info") {
    return <VenueInfo />;
  }
  if (step === "venue-about") {
    return <VenueAbout />;
  }
  if (step === "venue-detail") {
    return <VenueDetail />;
  }
  if (step === "venue-photo") {
    return <VenuePhoto />;
  }

  return <div>loading...</div>;
}
