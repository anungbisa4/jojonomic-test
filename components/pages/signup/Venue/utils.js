import * as yup from "yup";

export const schemaValidationOne = yup.object().shape({
  user: yup.object({
    name: yup.string().required("harus diisi"),
    username: yup.string().required("harus diisi"),
    email: yup.string().email("Email tidak valid").required("harus diisi"),
    password: yup.string().required("harus diisi"),
    confirm_password: yup
      .string()
      .required("harus diisi")
      .oneOf([yup.ref("password"), null], "konfirmasi sandi tidak sesuai"),
    phone_number: yup
      .string()
      .matches(/^-?\d*\.?\d*$/, "harus diisi angka")
      .max(13, "maksimal 13 digit")
      .required("harus diisi")
      .min(9, "minimal 9 digit"),
  }),
  partner: yup.object({
    category_id: yup.string().required("harus diisi"),
    city_id: yup.string().required("harus diisi"),
  }),
});
export const schemaValidationTwo = yup.object().shape({
  user: yup.object({
    address: yup.string().required("harus diisi"),
  }),
  partner: yup.object({
    description: yup.string().required("harus diisi"),
  }),
});
export const schemaValidationDetail = yup.object().shape({
  user: yup.object({
    address: yup.string().required("harus diisi"),
  }),
  partner: yup.object({
    gender: yup.string().required("harus diisi"),
  }),
  venue: yup.object({
    pic_name: yup.string().required("harus diisi"),
    pic_phone_number: yup
    .string()
    .matches(/^-?\d*\.?\d*$/, "harus diisi angka")
    .max(13, "maksimal 13 digit")
    .required("harus diisi")
    .min(9, "minimal 9 digit"),
  }),
});
export const schemaValidationPhoto = yup.object().shape({
  influencer_services: yup.array().of(
    yup.object({
      area_size: yup.string().required("harus diisi"),
      capacity: yup.string().required("harus diisi"),
      // detail_items: yup.string().required("harus diisi"),
      // minimum_order: yup.string().required("harus diisi"),
      name: yup.string().required("harus diisi"),
      price: yup.string().required("harus diisi"),
      quota: yup.string().required("harus diisi"),
      // venue_vendor_item_images: [],
    })
  )
});
