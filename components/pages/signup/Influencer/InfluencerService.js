import dynamic from "next/dynamic";
import { useState } from "react";
import TextField from "components/TextField";
import CardSignup from "../CardSignup";
import { useRouter } from "next/router";
import Grid from "@material-ui/core/Grid";
import HeaderPartner from "../HeaderPartner";
import Button from "components/Button";
import { TrashIcon } from "@heroicons/react/solid";

import ListBox from "pages/signup/ListBox";

import { useForm, useFieldArray } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { schemaValidationTwo } from "./utils";
import { useSelector, useDispatch } from "react-redux";
import { stepServiceInfluencer } from "@/redux/signup/action";
import { fetcher } from "src/utils/fetch";
import useSWR from "swr";

const serviceTemp = [
  { id: 1, name: "Jos" },
  { id: 2, name: "Yes" },
];

const addValue = {
  post_duration: "",
  price_rate: "",
  service_id: "",
  total_post: "",
};
const InfluencerRate = () => {
  const { signup: partner } = useSelector((state) => state);
  const dispatch = useDispatch();
  const router = useRouter();

  const handleLink = (href) => {
    router.push(href);
  };

  const defaultValues = partner?.influencer?.stepService || {
    influencer_services: [
      {
        post_duration: "",
        price_rate: "",
        service_id: "",
        total_post: "",
      },
    ],
  };

  const {
    control,
    register,
    handleSubmit,
    setValue,
    formState: { errors },
  } = useForm({
    // resolver: yupResolver(schemaValidationTwo),
    mode: "onBlur",
    defaultValues,
  });

  const { fields, append, remove } = useFieldArray({
    control,
    name: "influencer_services",
  });

  const handleRemove = (index) => {
    if (index > 0) {
      remove(index);
    }
  };

  const handleStepService = (data) => {
    dispatch(stepServiceInfluencer(data));
    handleLink("/signup/verification/email?type=influencer")
  };

  return (
    <section className="flex items-center justify-center font-pop text-center  min-h-[calc(100vh-9rem)]">
      <CardSignup className="mt-36 xs:px-6 xs:w-[400px] sm:w-[500px] md:w-[670px] lg:w-[800px] md:px-8">
        <HeaderPartner title="Influencer" activeStep={4} />
        <form
          className="mt-8 space-y-4 mb-12 w-full"
          onSubmit={handleSubmit(handleStepService)}
        >
          <div className="flex justify-between items-center">
            <p className="font-bold">Price Rate per Service (Jumlah: 0)</p>
          </div>
          {fields.map((item, index) => {
            return (
              <Grid key={index} container spacing={2} className="relative">
                <Grid item xs={12} md={4} className="text-left !mb-4">
                  <ListBox
                    label="Service"
                    placeholder="Pilih Service"
                    listOption={serviceTemp || []}
                    name={`influencer_services.${index}.service_id`}
                    selectedName="name"
                    selectedValue="id"
                    setValue={setValue}
                    // required
                    control={control}
                    defaultValues={
                      partner?.influencer?.stepService?.list
                        ?.influencer_services?.[index]?.service_id
                    }
                  />
                </Grid>
                <Grid item xs={12} md={2} className="text-left !mb-4">
                  <TextField
                    placeholder="Masukkan Jumlah"
                    label="Jumlah Post"
                    name={`influencer_services.${index}.total_post`}
                    register={register}
                  />
                </Grid>
                <Grid item xs={12} md={2} className="text-left !mb-4">
                  <TextField
                    placeholder="Masukkan Durasi"
                    label="Durasi Post (Hari)"
                    name={`influencer_services.${index}.post_duration`}
                    register={register}
                  />
                </Grid>
                <Grid item xs={11} md={3} className="text-left !mb-4">
                  <TextField
                    placeholder="Masukkan Price Rate"
                    label="Price Rate Service"
                    name={`influencer_services.${index}.price_rate`}
                    register={register}
                  />
                </Grid>
                {index > 0 && (
                  <Grid item xs={1} className="text-left !mb-4 self-end">
                    <button type="button" onClick={() => handleRemove(index)}>
                      <TrashIcon className="w-7 text-red-500 cursor-pointer" />
                    </button>
                  </Grid>
                )}
              </Grid>
            );
          })}
          <div>
            <Button
              onClick={() => append(addValue)}
              type="button"
              className="!rounded-xl"
              color="primary-bg"
              text="Tambah Service"
            />
          </div>
          <div className="flex justify-end">
            <button
              type="submit"
              className="text-primary-orange font-semibold text-right active:opacity-70"
            >
              Selanjutnya
            </button>
          </div>
        </form>
      </CardSignup>
    </section>
  );
};

export default InfluencerRate;
