
import { useState } from "react";
import TextField from "components/TextField";
import CardSignup from "../CardSignup";
import { useRouter } from "next/router";
import Grid from "@material-ui/core/Grid";
import HeaderPartner from "../HeaderPartner";
import ListBox from "pages/signup/ListBox";

import InputDatePicker from "../InputDate";

import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { schemaValidationTwo } from "./utils";
import { useSelector, useDispatch } from "react-redux";
import { stepTwoInfluencer} from "@/redux/signup/action";
import { fetcher } from "src/utils/fetch";
import useSWR from "swr";

const genre = [{ name: "Laki-laki" }, { name: "Perempuan" }];

const InfluencerRate = () => {
  const { signup: partner } = useSelector((state) => state);
  const { data: cities } = useSWR("/reference/cities", fetcher);
  const { data: religion } = useSWR("/reference/religion", fetcher);
  const { data: marital } = useSWR("/reference/marital", fetcher);
  const dispatch = useDispatch();
  const router = useRouter();

  const handleLink = (href) => {
    router.push(href);
  };

  const defaultValues = {
    influencer: {
      date_of_birth: partner?.influencer?.stepTwo?.influencer?.date_of_birth || "",
      religion_id: partner?.influencer?.stepTwo?.influencer?.religion_id || "",
      marital_id: partner?.influencer?.stepTwo?.influencer?.marital_id || "",
    },
    user: {
      address: partner?.influencer?.stepTwo?.user?.address || "",
    },
    partner: {
      city_id: partner?.influencer?.stepTwo?.partner?.city_id || "",
      description: partner?.influencer?.stepTwo?.partner?.description || "",
      gender: partner?.influencer?.stepTwo?.partner?.gender || "",
    },
    list: { ...partner?.influencer?.stepTwo?.list },
  };

  const {
    register,
    handleSubmit,
    setValue,
    control,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schemaValidationTwo),
    mode: "onBlur",
    defaultValues,
  });

  const handleStepRegister = (data) => {
    dispatch(stepTwoInfluencer(data));
    handleLink("/signup/influencer?step=influencer-photo");
  };

  console.log(errors)

  return (
    <section className="flex items-center justify-center font-pop text-center  min-h-[calc(100vh-9rem)]">
      <CardSignup className="mt-36 xs:px-6 xs:w-[400px] sm:w-[500px] md:w-[670px] lg:w-[800px] md:px-8">
        <HeaderPartner title="Influencer" activeStep={1} />
        <form
          className="mt-8 space-y-4 mb-12 w-full"
          onSubmit={handleSubmit(handleStepRegister)}
        >
          <Grid container spacing={2}>
            <Grid item xs={12} md={4} className="text-left !mb-4">
              <InputDatePicker
                placeholder="Pilih Tanggal Lahir"
                label="Tanggal Lahir"
                name="influencer.date_of_birth"
                required
                control={control}
                error={errors}
                statusError={errors?.influencer?.date_of_birth}
                defaultValues={defaultValues?.influencer?.date_of_birth}
              />
            </Grid>
            <Grid item xs={12} md={4} className="text-left !mb-4">
              <ListBox
                label="Agama"
                placeholder="Pilih Agama"
                listOption={religion?.data || []}
                name="influencer.religion_id"
                selectedName="name"
                selectedValue="id"
                setValue={setValue}
                required
                control={control}
                error={errors}
                statusError={errors?.influencer?.religion_id}
                defaultValues={defaultValues?.list?.influencer?.religion_id}
              />
            </Grid>
            <Grid item xs={12} md={4} className="text-left !mb-4">
              <ListBox
                label="Jenis Kelamin"
                placeholder="Pilih jenis kelamin"
                listOption={genre || []}
                name="partner.gender"
                selectedName="name"
                selectedValue="name"
                setValue={setValue}
                required
                control={control}
                error={errors}
                statusError={errors?.partner?.gender}
                defaultValues={defaultValues?.list?.partner?.gender}
              />
            </Grid>
            <Grid item xs={12} md={8} className="text-left !mb-4">
              <TextField
                placeholder="Masukan Alamat Sesuai KTP"
                label="Alamat Sesuai KTP"
                name="user.address"
                register={register}
                required
                error={errors}
                statusError={errors?.user?.address}
              />
            </Grid>
            <Grid item xs={12} md={4} className="text-left !mb-4">
              <ListBox
                label="Domisili"
                placeholder="Pilih Domisili"
                listOption={cities?.data || []}
                name="partner.city_id"
                selectedName="name"
                selectedValue="id"
                setValue={setValue}
                required
                control={control}
                error={errors}
                statusError={errors?.partner?.city_id}
                defaultValues={defaultValues?.list?.partner?.city_id}
              />
            </Grid>
            <Grid item xs={12} md={8} className="text-left !mb-4">
              <TextField
                placeholder="Masukan Deskripsi"
                label="Deskripsi"
                name="partner.description"
                register={register}
                required
                error={errors}
                statusError={errors?.partner?.description}
              />
            </Grid>
            <Grid item xs={12} md={4} className="text-left !mb-4">
              <ListBox
                label="Status"
                placeholder="Pilih Status"
                listOption={marital?.data || []}
                name="influencer.marital_id"
                selectedName="name"
                selectedValue="id"
                setValue={setValue}
                required
                control={control}
                error={errors}
                statusError={errors?.influencer?.marital_id}
                defaultValues={defaultValues?.list?.influencer?.marital_id}
              />
            </Grid>
          </Grid>
          <div className="flex justify-end">
            <button
              type="submit"
              className="text-primary-orange font-semibold text-right active:opacity-70"
            >
              Selanjutnya
            </button>
          </div>
        </form>
      </CardSignup>
    </section>
  );
};

export default InfluencerRate;
