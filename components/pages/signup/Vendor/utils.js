import * as yup from "yup";

export const schemaValidationOne = yup.object().shape({
  user: yup.object({
    name: yup.string().required("harus diisi"),
    username: yup.string().required("harus diisi"),
    email: yup.string().email("Email tidak valid").required("harus diisi"),
    password: yup.string().required("harus diisi"),
    confirm_password: yup
      .string()
      .required("harus diisi")
      .oneOf([yup.ref("password"), null], "konfirmasi sandi tidak sesuai"),
    phone_number: yup
      .string()
      .matches(/^-?\d*\.?\d*$/, "harus diisi angka")
      .max(13, "maksimal 13 digit")
      .required("harus diisi")
      .min(9, "minimal 9 digit"),
  }),
  partner: yup.object({
    category_id: yup.string().required("harus diisi"),
  }),
});
export const schemaValidationTwo = yup.object().shape({
  user: yup.object({
    address: yup.string().required("harus diisi"),
  }),
  partner: yup.object({
    description: yup.string().required("harus diisi"),
    city_id: yup.string().required("harus diisi"),
  }),
});
export const schemaValidationDetail = yup.object().shape({
  user: yup.object({
    address: yup.string().required("harus diisi"),
  }),
  partner: yup.object({
    gender: yup.string().required("harus diisi"),
  }),
  vendor: yup.object({
    pic_name: yup.string().required("harus diisi"),
    pic_phone_number: yup
      .string()
      .matches(/^-?\d*\.?\d*$/, "harus diisi angka")
      .max(13, "maksimal 13 digit")
      .required("harus diisi")
      .min(9, "minimal 9 digit"),
  }),
});
export const schemaValidationPhoto = yup.object().shape({
  influencer_services: yup.array().of(
    yup.object({
      area_size: yup.string().required("harus diisi"),
      capacity: yup.string().required("harus diisi"),
      // detail_items: yup.string().required("harus diisi"),
      // minimum_order: yup.string().required("harus diisi"),
      name: yup.string().required("harus diisi"),
      price: yup.string().required("harus diisi"),
      quota: yup.string().required("harus diisi"),
      // vendor_vendor_item_images: [],
    })
  ),
});

export const timeArr = [
  { name: "01:00" },
  { name: "02:00" },
  { name: "03.00" },
  { name: "04.00" },
  { name: "05.00" },
  { name: "06.00" },
  { name: "07.00" },
  { name: "08.00" },
  { name: "09.00" },
  { name: "10.00" },
  { name: "11.00" },
  { name: "12.00" },
  { name: "13.00" },
  { name: "14.00" },
  { name: "15.00" },
  { name: "16.00" },
  { name: "17.00" },
  { name: "18.00" },
  { name: "19.00" },
  { name: "20.00" },
  { name: "21.00" },
  { name: "22.00" },
  { name: "23.00" },
  { name: "24.00" },
];
