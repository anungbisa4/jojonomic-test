import * as yup from "yup";
export const schemaValidation = yup.object().shape({
  name: yup.string().required("harus diisi"),
  username: yup.string().required("harus diisi"),
  email: yup
    .string()
    .email("Email tidak valid")
    .required("harus diisi"),
  phone_number: yup
    .string()
    .matches(/^-?\d*\.?\d*$/, "harus diisi angka")
    .max(13, "maksimal 13 digit")
    .min(9, "minimal 9 digit")
    .required("harus diisi"),
  password: yup.string().required("harus diisi"),
  confirm_password: yup
    .string()
    .required("harus diisi")
    .oneOf([yup.ref("password"), null], "Kata sandi tidak sesuai"),
});