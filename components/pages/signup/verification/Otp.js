import { useState, useEffect } from "react";
import { useRouter } from "next/router";
import CardSignup from "../CardSignup";
import Button from "components/Button";

import OtpInput from "react-otp-input";
import Countdown from "react-countdown";
import { ArrowNarrowLeftIcon } from "@heroicons/react/outline";
import { useSelector, useDispatch } from "react-redux";
import { fetcher } from "src/utils/fetch";
import ErrorMessage from "@/components/ErrorMessage";
import { CircleSpinIcon } from "@/components/Icons";
import {
  loadingStatus,
  errorStatus,
  successStatus,
  completedStatus,
} from "src/redux/loader/action";

const CONSTANT_TYPE = "otp";

function Otp() {
  const router = useRouter();
  const [otp, setOtp] = useState("");
  const [resendCode, setResendCode] = useState(false);
  const [status, setStatus] = useState(true);
  const type = router?.query?.type || "/signup";
  const { user, loader } = useSelector((state) => state);
  const dispatch = useDispatch();

  const handleLink = (href) => {
    router.push(href);
  };

  const handleBack = () => {
    router.back();
  };

  const handleOtp = () => {
    const requestBody = {
      category: type,
      code: otp,
      user_id: user?.dataUser?.user?.id,
    };
    dispatch(loadingStatus(CONSTANT_TYPE));
    fetcher("/auth/verify-otp", { data: requestBody, method: "POST" })
      .then((res) => {
        dispatch(successStatus(CONSTANT_TYPE));
        handleLink(`/signup/term?type=${type}`)
      })
      .catch((err) =>
        dispatch(errorStatus(CONSTANT_TYPE, err?.response?.data?.message))
      );
      handleLink(`/signup/term?type=${type}`)
    setStatus(false);
  };

  const handleResendCode = () => setResendCode(true);

  useEffect(() => {
    if (!status) {
      setStatus(true);
      setResendCode(false);
    }
  }, [otp]);

  useEffect(() => {
    return () => {
      dispatch(completedStatus(CONSTANT_TYPE))
    } 
   },[])

  // Renderer callback with condition
  const renderer = ({ hours, minutes, seconds, completed, ...props }) => {
    if (completed) {
      // Render a completed state
      return (
        <span>
          {props.formatted.minutes}:{props.formatted.seconds}
        </span>
      );
    } else {
      // Render a countdown
      return (
        <span>
          {props.formatted.minutes}:{props.formatted.seconds}
        </span>
      );
    }
  };

  const CountTimer = () => {
    return (
      <div className="outline-remove active:opacity-70 text-sm font-bold text-center py-6 transition-all duration-300">
        <Countdown
          date={Date.now() + 60000}
          renderer={renderer}
          zeroPadTime={2}
          zeroPadDays={2}
        />
      </div>
    );
  };

  return (
    <>
      <section className="flex items-center justify-center font-pop text-center  min-h-[calc(100vh-9rem)]">
        <CardSignup className="mt-36 md:w-[525px] xs:px-6 xs:w-[400px] md:px-8 min-h-[300px]">
          <div className="">
            <div className="grid grid-cols-5  items-center justify-center mb-8">
              <a
                onClick={handleBack}
                className="text-[10px] xs:text-xs text-left text-primary-orange font-medium cursor-pointer"
              >
                <ArrowNarrowLeftIcon className="w-8" />
              </a>
              <h1 className="text-base xs:text-xl sm:text-2xl font-bold col-span-3">
                Verification
              </h1>
            </div>
            <p className="text-left text-xs">
              Kami akan mengirimkan kode OTP ke email berikut. Pastikan email
              dibawah sudah benar
            </p>
          </div>
          <div className="py-6 mx-auto ">
            <OtpInput
              value={otp}
              onChange={setOtp}
              numInputs={4}
              containerStyle="otp-container space-x-4 flex justify-center"
              inputStyle="text-xl sm:text-2x bg-blue-pale rounded-md outline-remove text-blue-primary h-12 !w-12  border-2 border-gray-900 border-opacity-50"
              isInputNum
            />
            <div className="flex justify-between">
              <CountTimer />
              <button className="text-xs text-gray-300">Kirim Ulang</button>
            </div>
            <ErrorMessage message={loader?.[CONSTANT_TYPE]?.message} />
          </div>
          <div className="flex justify-end">
            <div className="w-44">
              <Button disabled={loader?.[CONSTANT_TYPE]?.loading}
              loadingIcon={<CircleSpinIcon className="w-5" />} onClick={handleOtp} color="input" text="Verify" />
            </div>
          </div>
        </CardSignup>
      </section>
    </>
  );
}

export default Otp;
