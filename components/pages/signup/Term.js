import { useState } from "react";
import CardSignup from "./CardSignup";
import { useRouter } from "next/router";
import Grid from "@material-ui/core/Grid";
import HeaderPartner from "./HeaderPartner";
import Checkbox from "@material-ui/core/Checkbox";

const TalentInfo = () => {
  const router = useRouter();
  const [checked, setChecked] = useState(false);

  const handleLink = (href) => {
    router.push(href);
  };

  const type = router.query?.type || "Talent";

  const handleChange = (event) => {
    setChecked(event.target.checked);
  };

  const handleRegister = () => {
    if(checked) {
      handleLink(`/signup/success?status=${type}`)
    }
  }

  return (
    <section className="flex items-center justify-center font-pop text-center  min-h-[calc(100vh-9rem)]">
      <CardSignup className="mt-36 xs:px-6 xs:w-[400px] sm:w-[500px] md:w-[670px] lg:w-[800px] md:px-8">
        <HeaderPartner title={type} activeStep={5} />
        <form className="mt-8 space-y-4 mb-12 w-full">
          <Grid container spacing={2}>
            <Grid item xs={12} className="text-left">
              <div className="space-y-1 text-xs relative">
                <label>Syarat dan Ketentuan</label>
                <div className="p-2 border-1 border-gray-500 rounded-lg">
                  <p className="max-h-[150px] overflow-y-scroll">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Praesent et nisi nisl. Maecenas vitae dignissim massa.
                    Aliquam est nisl, auctor sed tempus nec, mattis at diam.
                    Vestibulum lectus urna, commodo a eros a, porttitor faucibus
                    tellus. Integer a congue velit, ut tristique nunc. In hac
                    habitasse platea dictumst. Phasellus tincidunt dignissim
                    dui, at ultricies enim mollis a. Nulla dui erat, iaculis vel
                    ligula non, dictum tristique eros. Pellentesque libero urna,
                    lacinia eget consequat ut, accumsan a nulla. Vestibulum at
                    metus justo. Vivamus vel nibh felis. Duis pharetra erat vel
                    nunc fringilla euismod. Quisque dignissim ut nibh a
                    lobortis. Phasellus fringilla sed nibh et accumsan. Donec
                    tortor odio, bibendum quis congue ut, tempor sit amet enim.
                    Nunc cursus lorem sed ultricies vehicula. Lorem ipsum dolor
                    sit amet, consectetur adipiscing elit. Praesent et nisi
                    nisl. Maecenas vitae dignissim massa. Aliquam est nisl,
                    auctor sed tempus nec, mattis at diam. Vestibulum lectus
                    urna, commodo a eros a, porttitor faucibus tellus. Integer a
                    congue velit, ut tristique nunc. In hac habitasse platea
                    dictumst. Phasellus tincidunt dignissim dui, at ultricies
                    enim mollis a. Nulla dui erat, iaculis vel ligula non,
                    dictum tristique eros. Pellentesque libero urna, lacinia
                    eget consequat ut, accumsan a nulla. Vestibulum at metus
                    justo. Vivamus vel nibh felis. Duis pharetra erat vel nunc
                    fringilla euismod. Quisque dignissim ut nibh a lobortis.
                    Phasellus fringilla sed nibh et accumsan. Donec tortor odio,
                    bibendum quis congue ut, tempor sit amet enim. Nunc cursus
                    lorem sed ultricies vehicula.
                  </p>
                </div>
              </div>
            </Grid>
            <Grid item xs={12} className="text-left !p-0 items-center">
              <Checkbox
                checked={checked}
                onChange={handleChange}
                inputProps={{ "aria-label": "primary checkbox" }}
              />
              <span className="text-xs">
                Saya telah membaca, memahami, dan menyetujui atas persyaratan
                dan kondisi yang berlaku
              </span>
            </Grid>
          </Grid>
          <div className="flex justify-end">
            <button
              type="button"
              className="text-primary-orange font-semibold text-right active:opacity-70"
              onClick={handleRegister}
            >
              Daftar
            </button>
          </div>
        </form>
      </CardSignup>
    </section>
  );
};

export default TalentInfo;
