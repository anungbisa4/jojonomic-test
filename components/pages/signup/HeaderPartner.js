import { useRouter } from "next/router";
import { ArrowNarrowLeftIcon } from "@heroicons/react/outline";
import Stepper from "@/components/Stepper";

const list = [1,2,3,4,5];

const HeaderPartner = ({title, activeStep=1}) => {
  const router = useRouter();
  const handleBack = () => {
    router.back();
  };

  return (
    <div className="space-y-3">
      <div className="grid grid-cols-5  items-center justify-center">
        <a
          onClick={handleBack}
          className="text-[10px] xs:text-xs text-left text-primary-orange font-medium cursor-pointer"
        >
          <ArrowNarrowLeftIcon className="w-8" />
        </a>
        <h1 className="text-base xs:text-xl sm:text-2xl font-bold col-span-3">
          Daftar Sebagai
        </h1>
      </div>
      <p className="font-medium text-xs sm:text-sm capitalize">Partner - {title}</p>
      <div className="w-1/2 mx-auto !my-6">
        <Stepper activeStep={activeStep} listStep={list} />
      </div>
    </div>
  );
};

export default HeaderPartner;
