import React from "react";

export default function SectionBenefit({ DataBenefit }) {
  return (
    <section className="w-screen bg-primary-black px-0 xl:px-32 py-32 ">
      <div
        className="text-center flex flex-col items-center"
        style={{ color: "white", fontFamily: "Poppins" }}
      >
        <h1 className="text-4xl font-bold">Garansi Kami</h1>
        <h2
          className="text-2xl w-[350px] sm:w-[750px] pt-20pt pb-20"
          style={{ fontFamily: "Open Sans" }}
        >
          Pemesanan vendor dan jasa yang pasti{" "}
          <span className="text-primary-orange font-bold">
            terpercaya dan mudah
          </span>{" "}
          dengan{" "}
          <span className="text-primary-orange font-bold">
            jaminan uang kembali
          </span>{" "}
          oleh Eventori.
        </h2>
      </div>
      <div className="flex justify-center">
        <div className="grid lg:grid-cols-3 gap-8 sm:gap-4 xl:gap-8">
          {DataBenefit.benefit.map((item, key) => {
            return (
              <div
                key={key}
                className="w-80 xl:h-[300px] xl:w-[360px] py-10 xl:py-20 border-3 border-primary-orange rounded-lg text-center"
              >
                <h3 className="text-2xl font-bold text-primary-orange pb-2">
                  {item.title}
                </h3>
                <p
                  className="text-lg text-white px-4"
                  style={{ fontFamily: "Open Sans" }}
                >
                  {item.content}
                </p>
              </div>
            );
          })}
        </div>
      </div>
    </section>
  );
}
