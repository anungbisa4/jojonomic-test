import React from "react";
import SwiperProgram from "./SwiperProgram";

export default function SectionProgram({ dataCategory }) {
  return (
    <>
      <section className="container-content">
        <SwiperProgram data={dataCategory} />
      </section>
    </>
  );
}
