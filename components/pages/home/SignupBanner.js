import Image from "components/Image";
import Button from "@/components/Button";

const SignupBanner = () => {
  return (
    <div className="max-h-[750px] mt-11 relative mb-8">
      <span className="bg-[#7B00FF] opacity-50 inset-0 absolute z-[3]" />
      <div className="h-[200px] xs:h-[300px] sm:h-[450px]">
        <Image
          src="/img/dummy_img_banner.jpg"
          alt="signup banner"
          layout="fill"
          objectFit="cover"
        />
      </div>
      <div className="hidden md:inline-block  w-[40%] h-full absolute top-0 z-[4]">
        <span className="inline-block bg-primary-orange w-full h-full opacity-60 rounded-tr-[200px] rounded-br-[200px]" />
        <div className="w-[211px] h-[533px] absolute bottom-0 right-10 z-[5] mr-[79px]">
          <Image
            src="/img/mic-dummy.webp"
            alt="signup banner"
            layout="fill"
            objectFit="cover"
          />
        </div>
      </div>
      <div className="container-fluid absolute inset-0 z-10 flex justify-end items-center text-white">
        <div className="md:w-[60%] text-base space-y-4 flex flex-col justify-end text-right">
          <h1 className="text-[1.5em] font-semibold">
            Kami ada untuk kamu yang sedang mewujudkan impian.
          </h1>
          <p className="text-[0.75em]">
            Eventori adalah pilihan utamamu untuk ecommerce dunia hiburan,
            daftar sekarang juga dan wijidkan impianmu bersama kami
          </p>
          <div className="flex justify-end pt-8">
            <div className="w-[200px]">
              <Button Button color="primary-bg">
                Daftar Sekarang
              </Button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default SignupBanner;
