import React from "react";
import Image from "next/image";

export default function AdsType2({ads}) {
  return (
    <section className="w-screen flex justify-center">
      <Image
        width={1440}
        height={900}
        objectFit="cover"
        src={ads.img}
        alt={ads.title}
      />
    </section>
  );
}
