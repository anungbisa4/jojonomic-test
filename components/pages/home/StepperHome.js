import React from "react";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import clsx from "clsx";
import Image from "next/image";
import {
  Stepper,
  Step,
  StepButton,
  StepLabel,
  StepConnector,
  Button,
} from "@material-ui/core/";
import StepperContent from "./StepperHomeContent";
import arrowIcon from "../../../public/arrow.png";
import DataStepper from "../../../data/DummyContentHome/DataStepper.json";

const CustomConnector = withStyles((theme) => ({
  line: {
    borderColor: "#F6921E",
    [theme.breakpoints.down("md")]: {
      borderTopWidth: 3,
      borderRadius: 1,
    },
    [theme.breakpoints.up("md")]: {
      borderTopWidth: 3,
      borderRadius: 1,
    },
  },
}))(StepConnector);

const useStyles = makeStyles((theme) => ({
  fixStepper: {
    "& .MuiStepLabel-iconContainer": {
      padding: 0,
    },
  },
}));

function getStepContent(step) {
  return <StepperContent contentStepper={DataStepper.stepper[step]}/>;
}

export default function StepperHome() {
  const classes = useStyles();
  const [activeStep, setActiveStep] = React.useState(0);
  const [completed] = React.useState({});
  const steps = [1, 2, 3, 4];

  const totalSteps = () => {
    return steps.length;
  };

  const completedSteps = () => {
    return Object.keys(completed).length;
  };

  const isFirstStep = () => {
    return activeStep === 0;
  };

  const isLastStep = () => {
    return activeStep === totalSteps() - 1;
  };

  const allStepsCompleted = () => {
    return completedSteps() === totalSteps();
  };

  const handleNext = () => {
    const newActiveStep =
      isLastStep() && !allStepsCompleted()
        ? // It's the last step, but not all steps have been completed,
          // find the first step that has been completed
          steps.findIndex((step, i) => !(i in completed))
        : activeStep + 1;
    setActiveStep(newActiveStep);
  };

  const handleBack = () => {
    const newActiveStep = isFirstStep() ? totalSteps() - 1 : activeStep - 1;
    setActiveStep((prevActiveStep) => newActiveStep);
  };

  const handleStep = (step) => () => {
    setActiveStep(step);
  };

  const CustomStepIcon = (props) => {
    const { active } = props;
    return (
      <div
        className={clsx(
          "w-14 h-14 rounded-full border-3 ring-8 ring-inset ring-primary-white border-primary-orange",
          {
            ["bg-primary-orange"]: active,
          }
        )}
      ></div>
    );
  };

  return (
    <div className="w-full">
      <Stepper
        nonLinear
        activeStep={activeStep}
        className="!bg-primary-white"
        connector={<CustomConnector />}
      >
        {steps.map((label, index) => (
          <Step key={label}>
            <StepButton
              onClick={handleStep(index)}
              completed={completed[index]}
              className="!px-0"
              disableRipple
            >
              <StepLabel
                StepIconComponent={CustomStepIcon}
                className={`${classes.fixStepper} !relative`}
              >
                <div
                  className="absolute w-[60px] top-5 left-0 text-white font-bold"
                  style={{ fontFamily: "Open Sans" }}
                >
                  {label}
                </div>
              </StepLabel>
            </StepButton>
          </Step>
        ))}
      </Stepper>
      <div>
        <div className="flex justify-center pt-32 bg-primary-white">
          <Button
            //   disabled={activeStep === 0}
            onClick={handleBack}
            className="!h-[60px] !rounded-full !relative !top-8 !right-10 !hidden sm:!block"
            style={{ backgroundColor: "transparent" }}
          >
            <Image src={arrowIcon}
             objectFit="cover"
             />
          </Button>
          {getStepContent(activeStep)}
          <Button
            //   disabled={activeStep === steps.length-1}
            onClick={handleNext}
            className="!h-[60px] !rounded-full !relative !top-8 !left-10 !hidden sm:!block"
            style={{ backgroundColor: "transparent" }}  
          >
            <Image src={arrowIcon}
             objectFit="cover"
             className="scale-x-[-1]"
             />
          </Button>
        </div>
      </div>
    </div>
  );
}
