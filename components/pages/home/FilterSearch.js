import React from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import { AppBar, Tabs, Tab, Box } from "@material-ui/core";
import { createMuiTheme, ThemeProvider } from "@material-ui/core/styles";
import { StarBorder, Apartment, Build, People } from "@material-ui/icons";
import TabTalentContent from "./TabsContent/TabTalentContent";
import TabVenueContent from "./TabsContent/TabVenueContent";
import TabVendorContent from "./TabsContent/TabVendorContent";
import TabInfluencerContent from "./TabsContent/TabInfluencerContent";

const theme = createMuiTheme({
  overrides: {
    MuiTab: {
      root: {
        "&.Mui-selected": {
          background: "white",
          color: "#000000",
        },
      },
    },
  },
});

const useStyles = makeStyles(() => ({
  root: {
    borderRadius: "20px 20px 0 0",
  },
  flexContainer: {
    background: "#F6921E",
  },
  indicator: {
    background: "#F6921E",
  },
  wrapper: {
    color: "#ffffff",
  },
  wrapper: {
    flexDirection: "row",
    fontFamily: "Open Sans",
    fontWeight: 700,
  },
  labelIcon: {
    minHeight: 50,
  },
  textColorInherit: {
    opacity: 1,
  },
}));

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
      style={{
        backgroundColor: "white",
        position: "relative",
        width: "80vw",
        borderRadius: "0 20px 20px 20px",
        filter: "drop-shadow(0px 4px 4px rgba(0, 0, 0, 0.25))",
      }}
    >
      {value === index && <Box p={6}>{children}</Box>}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
}

export default function FilterSearch() {
  const [value, setValue] = React.useState(0);
  const classes = useStyles();
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  return (
    <div className="">
      <div style={{ width: "53.3%" }}>
        <ThemeProvider theme={theme}>
          <AppBar position="static">
            <Tabs
              value={value}
              onChange={handleChange}
              aria-label="simple tabs example"
              classes={{
                flexContainer: classes.flexContainer,
                root: classes.root,
                indicator: classes.indicator,
              }}
            >
              <Tab
                icon={
                  <StarBorder
                    fontSize="small"
                    style={{ marginRight: 10, marginBottom: 2 }}
                  />
                }
                classes={{
                  wrapper: classes.wrapper,
                  labelIcon: classes.labelIcon,
                  textColorInherit: classes.textColorInherit,
                }}
                label="Talent"
                {...a11yProps(0)}
              />
              <Tab
                icon={
                  <Apartment
                    fontSize="small"
                    style={{ marginRight: 10, marginBottom: 2 }}
                  />
                }
                classes={{
                  wrapper: classes.wrapper,
                  labelIcon: classes.labelIcon,
                  textColorInherit: classes.textColorInherit,
                }}
                label="Venue"
                {...a11yProps(1)}
              />
              <Tab
                icon={
                  <Build
                    fontSize="small"
                    style={{ marginRight: 10, marginBottom: 2 }}
                  />
                }
                classes={{
                  wrapper: classes.wrapper,
                  labelIcon: classes.labelIcon,
                  textColorInherit: classes.textColorInherit,
                }}
                label="Vendor"
                {...a11yProps(2)}
              />
              <Tab
                icon={
                  <People
                    fontSize="small"
                    style={{ marginRight: 10, marginBottom: 2 }}
                  />
                }
                classes={{
                  wrapper: classes.wrapper,
                  labelIcon: classes.labelIcon,
                  textColorInherit: classes.textColorInherit,
                }}
                label="Influencer"
                {...a11yProps(3)}
              />
            </Tabs>
          </AppBar>
        </ThemeProvider>
      </div>
      <TabPanel value={value} index={0}>
        <TabTalentContent />
      </TabPanel>
      <TabPanel value={value} index={1}>
        <TabVenueContent />
      </TabPanel>
      <TabPanel value={value} index={2}>
        <TabVendorContent />
      </TabPanel>
      <TabPanel value={value} index={3}>
        <TabInfluencerContent />
      </TabPanel>
    </div>
  );
}
