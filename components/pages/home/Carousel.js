import { useRef } from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import Image from "components/Image";

// import DataCarousel from "../../../data/DataCarousel.json";

// import Swiper core and required modules
import SwiperCore, { Autoplay, Pagination, Navigation } from "swiper";

// install Swiper modules
SwiperCore.use([Autoplay, Pagination, Navigation]);

const base_url = process.env.base_url;

export default function CarouselReuse({ data }) {
  const list = data;

  const prevRef = useRef(null);
  const nextRef = useRef(null);

  const pagination = {
    modifierClass: `container-fluid w-full bottom-6 md:bottom-[116px] left-1/2 flex justify-center md:justify-end custom-bullet space-x-2 transition-all `,
    bulletActiveClass: `!bg-primary-orange`,
    clickable: true,
    renderBullet: function (index, className) {
      return (
        `<span class="${className} border-2 border-primary-orange !opacity-100 xs:p-1 sm:p-1.5 md:p-2">` +
        "</span>"
      );
    },
  };
  // const navigation = {
  //   disabledClass: "test-disable",
  //   prevEl: navigationPrevRef.current,
  //   nextEl: navigationNextRef.current,
  // };

  return (
    <div className="main-carousel rounded-b-[2.5rem] xs:rounded-b-[3.5rem] sm:rounded-b-[5rem] md:rounded-b-[163px] overflow-hidden">
      <Swiper
        slidesPerView={1}
        className="cursor-pointer max-h-[730px] m-auto"
        preloadImages
        autoplay={{
          delay: 2500,
          disableOnInteraction: false,
        }}
        onSwiper={(swiper) => swiper}
        pagination={pagination}
        // navigation
      >
        {list.data.reverse().map((item, key) => {
          return (
            <SwiperSlide
              key={key}
              className="relative text-white max-h-[730px]"
            >
              <Image src={item.images} alt="Snow" />
              <div className="container-fluid absolute inset-0 flex items-center  m-auto">
                <div className="justify-center font-pop text-white space-y-2 md:space-y-4">
                  <h1 className="text-lg sm:text-2xl md:text-5xl font-bold pt-7 line-clamp-3 w-[17rem] sm:w-2/3 md:w-1/2">
                    {item.title}
                  </h1>
                  <h3 className="w-[17rem] sm:w-2/3 text-xs md:text-base line-clamp-3">
                    {item.sub_title}
                  </h3>
                </div>
              </div>
            </SwiperSlide>
          );
        })}
      </Swiper>
    </div>
  );
}
