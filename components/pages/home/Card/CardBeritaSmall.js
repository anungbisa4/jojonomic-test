import React from "react";
import Image from "next/image";

export default function CardBeritaSmall({ alt, src, title }) {
  return (
    <div className="flex flex-col items-center">
      {/* <div className="w-[163px] h-[135px] md:w-[263px] md:h-[213px] xl:w-[370px] xl:h-[299px] bg-primary-orange rounded-2xl"> */}
      <div className="w-full h-full bg-primary-orange rounded-2xl relative cursor-pointer">
        <Image
          // src={data.profilePicture || data.programBanner || src}
          src={src}
          width={370}
          height={219}
          // alt={`${data.name} Product Picture`}
          alt={alt}
          layout="responsive"
          objectFit="cover"
          className="rounded-2xl"
        />
        {/* <div className="h-[42px] md:h-[60px] xl:h-[80px] -mt-1 flex items-center px-[14px] xl:px-[17px]"> */}
        <div className="text-[8px] sm:text-xs xl:text-base">
          <div className="h-[24%] flex items-center px-[1.2em] pt-[.33em] md:pt-[.8em]">
            <h4 className="line-clamp-2 font-semibold text-white font-pop">
              {/* Lirik Lagu “Widodari” yang Dinyanyikan oleh Denny Caknan dan Guyon
              Waton */}
              {title}
            </h4>
          </div>
        </div>
      </div>
    </div>
  );
}
