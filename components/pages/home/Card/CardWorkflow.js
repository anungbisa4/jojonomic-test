// add home component card
import React from "react";
import Image from "next/image";

export default function CardWorkflow({data}) {
  return (
    <div className="lg:text-lg xl:text-2xl">
      <div className="overflow-visible flex items-center px-[1.67em] py-[1.5em] bg-primary-blue rounded-2xl filter drop-shadow-lg">
        <div>
          <div className=" float-left w-[7.5em] h-[7.5em]">
            <Image
              // src="/iconWorkflow.png"
              src={data.icon}
              height={180}
              width={180}
              layout="responsive"
            />
          </div>
        </div>
        <div className="font-pop">
          <h2 className="font-bold uppercase">
            {data.title}
            {/* Telusuri Pilihan Anda */}
          </h2>
          <p
            className="text-[.75em] leading-normal line-clamp-3"
            // style={{ fontFamily: "Open Sans" }}
          >
            {data.content}
            {/* Jelajahi pilihan vendor-vendor di daerahmu. Lihat foto, rekaman dan */}
            {/* ulas tentang vendor tersebut. */}
          </p>
        </div>
      </div>
    </div>
  );
}
