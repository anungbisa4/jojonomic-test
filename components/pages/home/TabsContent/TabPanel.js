import React, { useState } from "react";
import { Grid, Button } from "@material-ui/core";
import TextField from "components/TextField";
import DatePickerField from "@/components/DatePicker";
import { SearchIcon, ArrowRightIcon } from "@heroicons/react/outline";
import { fetcher } from "src/utils/fetch";
import useSWR from "swr";
import ListBox from "pages/signup/ListBox";
import router, { useRouter } from "next/router";
import { useForm } from "react-hook-form";

export default function TabTalentContent({ label, textBtn }) {
  const { data: categoriesPartner } = useSWR(
    "/reference/partner_categories",
    fetcher
  );
  const { data: cities } = useSWR("/reference/cities", fetcher);

  const query = useRouter();

  // console.log(data);

  const { register, control, setValue, handleSubmit, watch } = useForm();
  console.log(watch());
  const onSubmit = (body) => {
    // console.log(body);
    const modif = {
      ...body,
      category_id: body.partner.category_id,
      city_id: body.partner.city_id,
    };

    router.push(
      {
        pathname: "/list-partner",
        query: modif,
      },
      undefined,
      { shallow: true }
    );
  };

  return (
    <form
      onSubmit={handleSubmit(onSubmit)}
      autoComplete="off"
      style={{ width: "100%" }}
    >
      <Grid container spacing={2} direction="row">
        <Grid item xs={12} sm={6} md={6} className="text-left !mb-2">
          <TextField
            placeholder={`Masukkan ${label}`}
            label={label}
            name="name"
            register={register}
            type="text"
          />
        </Grid>
        <Grid item xs={12} sm={6} md={6} className="text-left !mb-2">
          <DatePickerField
            placeholder="Masukkan Tanggal (contoh: 12/12/2021)"
            label="Tanggal Acara"
            register={register}
            name="schedule_date"
            control={control}
          />
        </Grid>
        <Grid item xs={12} sm={6} md={6} className="text-left !mb-2">
          {/* <TextField
            placeholder="Masukkan Categori (contoh: Musik)"
            label="Kategori"
            name="kategori"
            register={register}
          /> */}
          <ListBox
            label="Kategori"
            placeholder="Pilih Kategori"
            listOption={categoriesPartner?.data || []}
            name="partner.category_id"
            selectedName="name_categories"
            selectedValue="id"
            setValue={setValue}
            control={control}
            // error={errors}
            // statusError={errors?.partner?.category_id}
            // defaultValues={defaultValues?.list?.partner?.category_id}
            // required
          />
        </Grid>
        <Grid item xs={12} sm={6} md={6} className="text-left !mb-2">
          {/* <TextField
            placeholder="Masukkan Lokasi (contoh: Bali)"
            label="Lokasi"
            name="lokasi"
            register={register}
          /> */}
          <ListBox
            label="Domisili"
            placeholder="Pilih Domisili"
            listOption={cities?.data || []}
            name="partner.city_id"
            selectedName="name"
            selectedValue="id"
            setValue={setValue}
            control={control}
            // control={control}
            // error={errors}
            // statusError={errors?.partner?.city_id}
            // defaultValues={defaultValues?.list?.partner?.city_id}
          />
        </Grid>
      </Grid>
      <Grid container direction="row" className="justify-end mt-4">
        <Grid
          item
          xs={12}
          sm={5}
          md={3}
          className="text-left !mb-2 justify-end"
        >
          <Button
            fullWidth
            className="!normal-case !bg-primary-orange !font-semibold !text-white !rounded-lg"
            type="submit"
          >
            <div className="flex justify-between items-center w-full px-4">
              <div className="flex space-x-2">
                <SearchIcon className="text-white w-4" />
                <span>{textBtn}</span>
              </div>
              <ArrowRightIcon className="text-white w-4" />
            </div>
          </Button>
        </Grid>
      </Grid>
    </form>
  );
}
