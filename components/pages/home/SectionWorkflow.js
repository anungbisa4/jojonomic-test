// add home component
import React from "react";
import StepperHome from "./StepperHome";
import SwiperWorkflow from "./SwiperWorkflow/SwiperWorkflow";

export default function SectionWorkflow() {
  return (
    <section className="container-content bg-primary-white">
      <div
        className="flex flex-col items-center"
      >
        <div className="text-center max-w-content flex flex-col items-center font-pop">
          <h1 className="text-2xl md:text-4xl xl:text-5xl font-bold pb-[.625em]">
            Bagaimana Cara Kerja Eventori?
          </h1>
          <div className="w-[100%] md:w-[65%] xl:w-[75%]">
            <p className="text-sm md:text-base xl:text-lg pb-[3.5em]" 
            // style={{ fontFamily: "Open Sans" }}
            >
            Dapatkan pengalaman yang mudah dan berbeda dalam mencari talent, venue, vendor dan influencer
            </p>
          </div>
        </div>
        {/* <StepperHome /> */}
        <SwiperWorkflow />
      </div>
    </section>
  );
}
