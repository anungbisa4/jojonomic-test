import React from "react";
import { ErrorMessage } from "@hookform/error-message";

const TextField = ({
  label,
  type = "text",
  placeholder,
  labelClass,
  inputClass = "",
  containerClass,
  required,
  prefixPhone,
  prefixIcon,
  value,
  error,
  statusError,
  name,
  validationError,
  disabled,
  register,
  ...props
}) => {
  return (
    <div className={`space-y-1 text-xs relative ${containerClass}`}>
      {label && (
        <label htmlFor={name} className={`${labelClass} line-clamp-1`}>
          {label}
          {required && <span className="text-red-500 font-bold">*</span>}
        </label>
      )}
      <div
        className={`relative flex items-center border-1 rounded-lg ${
          error?.[name] || statusError ? "border-red-500" : "border-gray-900"
        }  w-full focus:outline-none focus:border-primary-orange overflow-hidden focus:ring-2 focus:ring-primary-orange ${inputClass}`}
      >
        {prefixPhone && (
          <div className="bg-gray-300 font-medium absolute top-0 bottom-0 px-2 flex items-center justify-center">
            +62
          </div>
        )}
        {prefixIcon && (
          <div className=" absolute top-0 bottom-0 px-2 flex items-center justify-center">
            {prefixIcon}
          </div>
        )}
        <div className={`w-full ${(prefixPhone || prefixIcon) ? "pl-11 py-2" : "p-2"}`}>
          {type === "textarea" ? (
            register ? (
              <textarea
                className={`focus:outline-none bg-transparent w-full ] ${inputClass}`}
                placeholder={placeholder}
                {...register(name, validationError)}
                {...props}
              />
            ) : (
              <textarea
                className={`focus:outline-none bg-transparent w-full ] ${inputClass}`}
                placeholder={placeholder}
                {...props}
              />
            )
          ) : (
            <>
              {register ? (
                <input
                  className={`focus:outline-none bg-transparent w-full ${inputClass}`}
                  type={type}
                  placeholder={placeholder}
                  value={value}
                  {...register(name, validationError)}
                  disabled={disabled}
                />
              ) : (
                <input
                  className={`focus:outline-none bg-transparent w-full ${inputClass}`}
                  type={type}
                  placeholder={placeholder}
                  value={value}
                  disabled={disabled}
                />
              )}
            </>
          )}
        </div>
      </div>
      {error && (
        <div className="absolute text-[11px] text-red-500">
          <ErrorMessage
            errors={error}
            name={name}
            render={({ message }) => <p>{message}</p>}
          />
        </div>
      )}
    </div>
  );
};
export default TextField;
