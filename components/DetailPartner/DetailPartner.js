import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Link from "next/link";
import PropTypes from "prop-types";

// import component material ui
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Avatar from "@material-ui/core/Avatar";
import Rating from "@material-ui/lab/Rating";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";

// import component Modal Booking
import ModalBooking from "../ModalBooking/ModalBooking";

const useStyles = makeStyles((theme) => ({
  tabBarStyle: {
    backgroundColor: "#23282D",
  },
  tabsTitle: {
    fontFamily: "Montserrat",
    fontWeight: 700,
    fontSize: 24,
    color: "#ffffff",
    // color: "#F6921E",
    textTransform: "capitalize",
    "&:hover": {
      color: "#F6921E",
    },
  },
  contentRider: {
    fontFamily: "Montserrat",
    fontWeight: 400,
    color: "#000000",
    fontSize: 16,
  },
  titleUlasan: {
    fontFamily: "Montserrat",
    fontWeight: 700,
    color: "#000000",
    fontSize: 20,
  },
  contentUlasan: {
    fontFamily: "Montserrat",
    fontWeight: 400,
    color: "#000000",
    fontSize: 16,
    textAlign: "justify",
    marginBottom: 30,
  },
  contentLink: {
    fontFamily: "Montserrat",
    fontWeight: 400,
    color: "#F6921E",
    fontSize: 16,
    textAlign: "justify",
    marginBottom: 30,
  },

  textColorPrimary: {
    color: "#ffffff",
  },
  textColor: {
    color: "#ffffff",
  },
  productCard: {
    borderRadius: 10,
    maxWidth: 325,
    minHeight: 330,
  },
  productMedia: {
    height: 140,
  },
  productTitle: {
    fontFamily: "Montserrat",
    fontWeight: 700,
    color: "#23282D",
    fontSize: 14,
    textAlign: "center",
  },
  productContent: {
    fontFamily: "Montserrat",
    fontWeight: 400,
    color: "#23282D",
    fontSize: 14,
    textAlign: "center",
  },
  avatar: {
    width: theme.spacing(8),
    height: theme.spacing(8),
  },
  styleBtn: {
    background: "#F6921E",
    color: "#ffffff",
    fontWeight: 700,
    borderRadius: 10,
    marginTop: 70,
    marginBottom: 70,
    fontFamily: "Montserrat",
    width: 200,
    height: 40,
    textTransform: "capitalize",
    "&:hover": {
      backgroundColor: "#F6921E",
      borderColor: "#F6921E",
      boxShadow: "none",
      color: "#ffffff",
    },
  },
}));

// Support Function
function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
}

export default function DetailPartner(props) {
  // Styling
  const classes = useStyles();

  // State for Tabpanel
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  // end State for Tabpanel

  // state modal
  const [openModal, setOpen] = React.useState(false);
  const handleClickOpenModal = () => {
    setOpen(true);
  };
  const handleClickCloseModal = () => {
    setOpen(false);
  };
  // end state modal

  var pilihan = (pilihan, item) => {
    if (pilihan == "talent") {
      return renderDetailTalent(item);
    } else if (pilihan == "influencer") {
      return renderDetailInfluencer(item);
    } else if (pilihan == "production") {
      return renderDetailProduction(item);
    } else if (pilihan == "venue") {
      return renderDetailVenue(item);
    }
  };

  var renderDetailTalent = (item) => {
    return (
      <div>
        {item.data.map((categories) => {
          if (categories.title.toLowerCase() == props.type) {
            return categories.items.map((member) => {
              if (member.id == props.id) {
                return (
                  <div>
                    <Grid container justify="center" alignitem="center">
                      <Grid item lg={12} xs={10} sm={12}>
                        <Card className={classes.rootCard} variant="outlined">
                          <AppBar
                            position="static"
                            className={classes.tabBarStyle}
                          >
                            <Tabs
                              value={value}
                              onChange={handleChange}
                              aria-label="simple tabs example"
                              TabIndicatorProps={{
                                style: { background: "#F6921E" },
                              }}
                            >
                              <Tab
                                className={classes.tabsTitle}
                                // label="Detail"
                                label={
                                  <span className={classes.tabsSpan}>
                                    Detail
                                  </span>
                                }
                                {...a11yProps(0)}
                              />
                              <Tab
                                className={classes.tabsTitle}
                                label="Ulasan"
                                {...a11yProps(1)}
                              />
                              <Tab
                                className={classes.tabsTitle}
                                label="Galeri"
                                {...a11yProps(2)}
                              />
                            </Tabs>
                          </AppBar>
                          {/* Panel Detail */}
                          <TabPanel value={value} index={0}>
                            <CardContent>
                              <Typography
                                className={classes.titleUlasan}
                                gutterBottom
                              >
                                Deskripsi
                              </Typography>
                              <Typography
                                className={classes.contentUlasan}
                                gutterBottom
                              >
                                {member.description}
                              </Typography>
                              <Typography
                                className={classes.titleUlasan}
                                gutterBottom
                              >
                                Link Youtube
                              </Typography>

                              <Link href={member.linkYoutube} passHref={true}>
                                <Typography
                                  className={classes.contentLink}
                                  gutterBottom
                                  style={{ cursor: "pointer" }}
                                >
                                  Klik Disini
                                </Typography>
                              </Link>
                              <Typography
                                className={classes.titleUlasan}
                                gutterBottom
                              >
                                Penunjang Teknis / Rider
                              </Typography>
                              <ul>
                                {member.riderList.map((item) => {
                                  return (
                                    <li>
                                      <Typography
                                        className={classes.contentRider}
                                      >
                                        <span style={{ fontWeight: "700" }}>
                                          {item.title}
                                        </span>{" "}
                                        - {item.content}
                                      </Typography>
                                    </li>
                                  );
                                })}
                              </ul>
                            </CardContent>
                          </TabPanel>
                          {/* Panel Ulasan */}
                          <TabPanel value={value} index={1}>
                            {/* Loop item ulasan */}
                            {member.dataUlasan.map((ulasan, index) => {
                              return (
                                <CardContent
                                  style={{ borderBottom: "1px solid #C4C4C4" }}
                                  key={index}
                                >
                                  <Grid
                                    container
                                    justify="flex-start"
                                    alignitem="center"
                                    direction="row"
                                  >
                                    <Grid item lg={5}>
                                      <Grid container direction="row">
                                        <Grid item lg={3}>
                                          <Avatar
                                            src={ulasan.avatar}
                                            className={classes.large}
                                          />
                                        </Grid>
                                        <Grid item>
                                          <Grid container direction="column">
                                            <Grid item>
                                              <Typography
                                                className={classes.titleUlasan}
                                              >
                                                {ulasan.name}
                                              </Typography>
                                            </Grid>
                                            <Grid item>
                                              <Typography
                                                className={
                                                  classes.contentUlasan
                                                }
                                              >
                                                {ulasan.timePost}
                                              </Typography>
                                            </Grid>
                                          </Grid>
                                        </Grid>
                                      </Grid>
                                    </Grid>
                                    <Grid item lg={7}>
                                      <Rating
                                        name="read-only"
                                        value={ulasan.rating}
                                        readOnly
                                      />
                                      <Typography
                                        className={classes.contentUlasan}
                                      >
                                        {ulasan.content}
                                      </Typography>
                                    </Grid>
                                  </Grid>
                                </CardContent>
                              );
                            })}
                          </TabPanel>
                          {/* Panel Galeri */}
                          <TabPanel value={value} index={2}>
                            <CardContent>Galeri Content Goes Here</CardContent>
                          </TabPanel>
                        </Card>
                      </Grid>
                      <Grid item>
                        <Button
                          variant="contained"
                          className={classes.styleBtn}
                          onClick={handleClickOpenModal}
                        >
                          Pesan Sekarang
                        </Button>
                        <ModalBooking
                          openModal={openModal}
                          handleClickCloseModal={handleClickCloseModal}
                          type={props.type}
                        />
                      </Grid>
                    </Grid>

                    {console.log(member.name)}
                  </div>
                );
              }
            });
          }
        })}
      </div>
    );
  };

  var renderDetailInfluencer = (item) => {
    return (
      <div>
        {item.data.map((categories) => {
          if (categories.title.toLowerCase() == props.type) {
            return categories.items.map((member) => {
              if (member.id == props.id) {
                return (
                  <Grid container justify="center" alignitem="center">
                    <Grid item lg={12} xs={10} sm={12}>
                      <Card className={classes.rootCard} variant="outlined">
                        <AppBar
                          position="static"
                          className={classes.tabBarStyle}
                        >
                          <Tabs
                            value={value}
                            onChange={handleChange}
                            aria-label="simple tabs example"
                            TabIndicatorProps={{
                              style: { background: "#F6921E" },
                            }}
                            // textColor="secondary"
                          >
                            <Tab
                              className={classes.tabsTitle}
                              // label="Detail"
                              label={
                                <span className={classes.tabsSpan}>Detail</span>
                              }
                              {...a11yProps(0)}
                            />
                            <Tab
                              className={classes.tabsTitle}
                              label="Ulasan"
                              {...a11yProps(1)}
                            />
                            <Tab
                              className={classes.tabsTitle}
                              label="Galeri"
                              {...a11yProps(2)}
                            />
                          </Tabs>
                        </AppBar>
                        {/* Panel Detail */}
                        <TabPanel value={value} index={0}>
                          <CardContent>
                            <Typography
                              className={classes.titleUlasan}
                              gutterBottom
                            >
                              Deskripsi
                            </Typography>
                            <Typography
                              className={classes.contentUlasan}
                              gutterBottom
                            >
                              {member.description}
                            </Typography>
                            <Typography
                              className={classes.titleUlasan}
                              gutterBottom
                            >
                              Link Youtube
                            </Typography>

                            <Link href={member.linkYoutube} passHref={true}>
                              <Typography
                                className={classes.contentLink}
                                gutterBottom
                                style={{ cursor: "pointer" }}
                              >
                                Klik Disini
                              </Typography>
                            </Link>
                          </CardContent>
                        </TabPanel>
                        {/* Panel Ulasan */}
                        <TabPanel value={value} index={1}>
                          {/* Loop item ulasan */}
                          {member.dataUlasan.map((ulasan, index) => {
                            return (
                              <CardContent
                                style={{ borderBottom: "1px solid #C4C4C4" }}
                                key={index}
                              >
                                <Grid
                                  container
                                  justify="flex-start"
                                  alignitem="center"
                                  direction="row"
                                >
                                  <Grid item lg={5}>
                                    <Grid container direction="row">
                                      <Grid item lg={3}>
                                        <Avatar
                                          src={ulasan.avatar}
                                          className={classes.large}
                                        />
                                      </Grid>
                                      <Grid item>
                                        <Grid container direction="column">
                                          <Grid item>
                                            <Typography
                                              className={classes.titleUlasan}
                                            >
                                              {ulasan.name}
                                            </Typography>
                                          </Grid>
                                          <Grid item>
                                            <Typography
                                              className={classes.contentUlasan}
                                            >
                                              {ulasan.timePost}
                                            </Typography>
                                          </Grid>
                                        </Grid>
                                      </Grid>
                                    </Grid>
                                  </Grid>
                                  <Grid item lg={7}>
                                    <Rating
                                      name="read-only"
                                      value={ulasan.rating}
                                      readOnly
                                    />
                                    <Typography
                                      className={classes.contentUlasan}
                                    >
                                      {ulasan.content}
                                    </Typography>
                                  </Grid>
                                </Grid>
                              </CardContent>
                            );
                          })}
                        </TabPanel>
                        {/* Panel Galeri */}
                        <TabPanel value={value} index={2}>
                          <CardContent>Galeri Content Goes Here</CardContent>
                        </TabPanel>
                      </Card>
                    </Grid>
                    <Grid item>
                      <Button
                        variant="contained"
                        className={classes.styleBtn}
                        onClick={handleClickOpenModal}
                      >
                        Pesan Sekarang
                      </Button>
                      <ModalBooking
                        openModal={openModal}
                        handleClickCloseModal={handleClickCloseModal}
                        type={props.type}
                      />
                    </Grid>
                  </Grid>
                );
              }
            });
          }
        })}
      </div>
    );
  };

  var renderDetailProduction = (item) => {
    return (
      <div>
        {item.data.map((categories) => {
          if (categories.title.toLowerCase() == props.type) {
            return categories.items.map((member) => {
              if (member.id == props.id) {
                return (
                  <Grid container justify="center" alignitem="center">
                    <Grid item lg={12} xs={10} sm={12}>
                      <Card className={classes.contentCard} variant="outlined">
                        <AppBar
                          position="static"
                          className={classes.tabBarStyle}
                        >
                          <Tabs
                            value={value}
                            onChange={handleChange}
                            aria-label="simple tabs example"
                            TabIndicatorProps={{
                              style: { background: "#F6921E" },
                            }}
                          >
                            <Tab
                              className={classes.tabsTitle}
                              label="Detail"
                              {...a11yProps(0)}
                            />
                            <Tab
                              className={classes.tabsTitle}
                              label="Daftar Harga"
                              {...a11yProps(1)}
                            />
                            <Tab
                              className={classes.tabsTitle}
                              label="Ulasan"
                              {...a11yProps(2)}
                            />
                          </Tabs>
                        </AppBar>

                        {/* Panel Detail */}
                        <TabPanel value={value} index={0}>
                          <CardContent>
                            <Typography
                              className={classes.titleUlasan}
                              gutterBottom
                            >
                              Deskripsi
                            </Typography>
                            <Typography
                              className={classes.contentUlasan}
                              gutterBottom
                            >
                              {member.description}
                            </Typography>
                            <Typography
                              className={classes.titleUlasan}
                              gutterBottom
                            >
                              Waktu Operasional
                            </Typography>
                            <Typography
                              className={classes.contentUlasan}
                              gutterBottom
                            >
                              {member.operationalTime}
                            </Typography>
                          </CardContent>
                        </TabPanel>

                        {/* Panel Daftar Harga */}
                        <TabPanel value={value} index={1}>
                          <Box width="90%" margin="auto">
                            <Grid
                              container
                              direction="row"
                              spacing={3}
                              justify="flex-start"
                            >
                              {member.dataProduk.map((product, index) => {
                                return (
                                  <Grid item>
                                    <Card
                                      className={classes.productCard}
                                      key={index}
                                    >
                                      <CardActionArea>
                                        <CardMedia
                                          className={classes.productMedia}
                                          image={product.media}
                                          title={product.name}
                                        />
                                        <CardContent>
                                          <Typography
                                            className={classes.productTitle}
                                          >
                                            {product.name}
                                          </Typography>
                                          <Typography
                                            gutterBottom
                                            className={classes.productContent}
                                          >
                                            {product.description}
                                          </Typography>
                                          <Typography
                                            className={classes.productTitle}
                                          >
                                            Harga
                                          </Typography>
                                          <Typography
                                            gutterBottom
                                            className={classes.productContent}
                                          >
                                            {product.price} / {product.unit}
                                          </Typography>
                                        </CardContent>
                                      </CardActionArea>
                                    </Card>
                                  </Grid>
                                );
                              })}
                            </Grid>
                          </Box>
                        </TabPanel>

                        {/* Panel Ulasan */}
                        <TabPanel value={value} index={2}>
                          {/* Loop item ulasan */}
                          {member.dataUlasan.map((ulasan, index) => {
                            return (
                              <CardContent
                                style={{ borderBottom: "1px solid #C4C4C4" }}
                                key={index}
                              >
                                <Grid
                                  container
                                  justify="flex-start"
                                  alignitem="center"
                                  direction="row"
                                >
                                  <Grid item lg={5}>
                                    <Grid container direction="row">
                                      <Grid item lg={3}>
                                        <Avatar
                                          src={ulasan.avatar}
                                          className={classes.avatar}
                                        />
                                      </Grid>
                                      <Grid item>
                                        <Grid container direction="column">
                                          <Grid item>
                                            <Typography
                                              className={classes.titleUlasan}
                                            >
                                              {ulasan.name}
                                            </Typography>
                                          </Grid>
                                          <Grid item>
                                            <Typography
                                              className={classes.contentUlasan}
                                            >
                                              {ulasan.timePost}
                                            </Typography>
                                          </Grid>
                                        </Grid>
                                      </Grid>
                                    </Grid>
                                  </Grid>
                                  <Grid item lg={7}>
                                    <Rating
                                      name="read-only"
                                      value={ulasan.rating}
                                      readOnly
                                    />
                                    <Typography
                                      className={classes.contentUlasan}
                                    >
                                      {ulasan.content}
                                    </Typography>
                                  </Grid>
                                </Grid>
                              </CardContent>
                            );
                          })}
                        </TabPanel>
                      </Card>
                    </Grid>
                    <Grid item>
                      <Button
                        variant="contained"
                        className={classes.styleBtn}
                        onClick={handleClickOpenModal}
                      >
                        Pesan Sekarang
                      </Button>
                      <ModalBooking
                        openModal={openModal}
                        handleClickCloseModal={handleClickCloseModal}
                        type={props.type}
                      />
                    </Grid>
                  </Grid>
                );
              }
            });
          }
        })}
      </div>
    );
  };

  var renderDetailVenue = (item) => {
    return (
      <div>
        {item.data.map((categories) => {
          if (categories.title.toLowerCase() == props.type) {
            return categories.items.map((member) => {
              if (member.id == props.id) {
                return (
                  <Grid container justify="center" alignitem="center">
                    <Grid item lg={12} xs={10} sm={12}>
                      <Card className={classes.contentCard} variant="outlined">
                        <AppBar
                          position="static"
                          className={classes.tabBarStyle}
                        >
                          <Tabs
                            value={value}
                            onChange={handleChange}
                            aria-label="simple tabs example"
                            TabIndicatorProps={{
                              style: { background: "#F6921E" },
                            }}
                          >
                            <Tab
                              className={classes.tabsTitle}
                              label="Detail"
                              {...a11yProps(0)}
                            />
                            <Tab
                              className={classes.tabsTitle}
                              label="Ruangan"
                              {...a11yProps(1)}
                            />
                            <Tab
                              className={classes.tabsTitle}
                              label="Ulasan"
                              {...a11yProps(2)}
                            />
                          </Tabs>
                        </AppBar>

                        {/* Panel Detail */}
                        <TabPanel value={value} index={0}>
                          <CardContent>
                            <Typography
                              className={classes.titleUlasan}
                              gutterBottom
                            >
                              Deskripsi
                            </Typography>
                            <Typography
                              className={classes.contentUlasan}
                              gutterBottom
                            >
                              {member.description}
                            </Typography>
                            <Typography
                              className={classes.titleUlasan}
                              gutterBottom
                            >
                              Kapasitas
                            </Typography>
                            <Typography
                              className={classes.contentUlasan}
                              gutterBottom
                            >
                              {member.capacity}
                            </Typography>
                            <Typography
                              className={classes.titleUlasan}
                              gutterBottom
                            >
                              Waktu Operasional
                            </Typography>
                            <Typography
                              className={classes.contentUlasan}
                              gutterBottom
                            >
                              {member.operationalTime}
                            </Typography>
                          </CardContent>
                        </TabPanel>

                        {/* Panel Daftar Harga */}
                        <TabPanel value={value} index={1}>
                          <Box width="90%" margin="auto">
                            <Grid
                              container
                              direction="row"
                              spacing={3}
                              justify="flex-start"
                            >
                              {member.dataProduk.map((product, index) => {
                                return (
                                  <Grid item>
                                    <Card
                                      className={classes.productCard}
                                      key={index}
                                    >
                                      <CardActionArea>
                                        <CardMedia
                                          className={classes.productMedia}
                                          image={product.media}
                                          title={product.name}
                                        />
                                        <CardContent>
                                          <Typography
                                            className={classes.productTitle}
                                          >
                                            {product.name}
                                          </Typography>
                                          <Typography
                                            gutterBottom
                                            className={classes.productContent}
                                          >
                                            {product.description}
                                          </Typography>
                                          <Typography
                                            className={classes.productTitle}
                                          >
                                            Harga
                                          </Typography>
                                          <Typography
                                            gutterBottom
                                            className={classes.productContent}
                                          >
                                            {product.price} / {product.unit}
                                          </Typography>
                                        </CardContent>
                                      </CardActionArea>
                                    </Card>
                                  </Grid>
                                );
                              })}
                            </Grid>
                          </Box>
                        </TabPanel>

                        {/* Panel Ulasan */}
                        <TabPanel value={value} index={2}>
                          {/* Loop item ulasan */}
                          {member.dataUlasan.map((ulasan, index) => {
                            return (
                              <CardContent
                                style={{ borderBottom: "1px solid #C4C4C4" }}
                                key={index}
                              >
                                <Grid
                                  container
                                  justify="flex-start"
                                  alignitem="center"
                                  direction="row"
                                >
                                  <Grid item lg={5}>
                                    <Grid container direction="row">
                                      <Grid item lg={3}>
                                        <Avatar
                                          src={ulasan.avatar}
                                          className={classes.avatar}
                                        />
                                      </Grid>
                                      <Grid item>
                                        <Grid container direction="column">
                                          <Grid item>
                                            <Typography
                                              className={classes.titleUlasan}
                                            >
                                              {ulasan.name}
                                            </Typography>
                                          </Grid>
                                          <Grid item>
                                            <Typography
                                              className={classes.contentUlasan}
                                            >
                                              {ulasan.timePost}
                                            </Typography>
                                          </Grid>
                                        </Grid>
                                      </Grid>
                                    </Grid>
                                  </Grid>
                                  <Grid item lg={7}>
                                    <Rating
                                      name="read-only"
                                      value={ulasan.rating}
                                      readOnly
                                    />
                                    <Typography
                                      className={classes.contentUlasan}
                                    >
                                      {ulasan.content}
                                    </Typography>
                                  </Grid>
                                </Grid>
                              </CardContent>
                            );
                          })}
                        </TabPanel>
                      </Card>
                    </Grid>
                    <Grid item>
                      <Button
                        variant="contained"
                        className={classes.styleBtn}
                        onClick={handleClickOpenModal}
                      >
                        Pesan Sekarang
                      </Button>
                      <ModalBooking
                        openModal={openModal}
                        handleClickCloseModal={handleClickCloseModal}
                        type={props.type}
                      />
                    </Grid>
                  </Grid>
                );
              }
            });
          }
        })}
      </div>
    );
  };

  return (
    <Box
      width="80%"
      pt={4}
      minWidth={310}
      maxWidth={800}
      style={{ margin: "auto" }}
    >
      {pilihan(props.type, props.item)}
    </Box>
  );
}
