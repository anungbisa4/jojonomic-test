import { useState } from "react";
import Image from "next/image";
// import PropTypes from "prop-types";

export default function ImageComponent({
  blur,
  className,
  src,
  bgTransparent,
  ...props
}) {
  const [errImage, setErrImage] = useState(false);
  const onImage = (e) => {
    setTimeout(() => setErrImage(true), 1000);
  };
  const onLoad = (e) => console.log(e);

  const typeError = errImage
    ? { objectFit: "contain", objectPosition: "50% 50%" }
    : {};
  return (
    <div
      className={`main-banner ${className} ${
        bgTransparent ? "" : "bg-gray-200"
      }`}
    >
      <Image
        src={errImage ? "/img/no-image.svg" : src || "/img/no-image.svg"}
        {...props}
        onError={onImage}
        objectFit="cover"
        placeholder={blur === "empty" ? "empty" : "blur"}
        blurDataURL={src || "/img/no-image.svg"}
        {...typeError}
      />
    </div>
  );
}

ImageComponent.defaultProps = {
  src: "/img/no-image.svg",
  alt: "alt-img",
  width: 700,
  height: 475,
  className: "",
  layout: "responsive",
};
