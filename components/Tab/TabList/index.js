import React from "react";
import { Tab } from "@headlessui/react";
import Button from "@/components/Button";
// import ArrowLeft from "@/components/Icons/ArrowLeftIcon";

const TabList = ({ label, type, back }) => {
  return (
    <div>
      {type === "feed" ? (
        <Tab className="font-bold text-lg lg:text-2xl">
          {({ selected }) => (
            <div
              className={
                selected
                  ? "border-t-[3px] border-primary-orange h-full text-center p-4"
                  : "border-t-[3px] border-transparent p-4"
              }
            >
              {label}
            </div>
          )}
        </Tab>
      ) : (
        <div className="grid xs:grid-cols-2 sm:grid-cols-3 lg:grid-cols-3 md:mx-1 lg:mx-5 justify-between">
          <div className="text-left">
            <p className="flex xs:p-5 sm:p-4 lg:p-4 sm:text-lg lg:text-2xl text-primary-orange">
              <span className="pr-2">
                {/* <ArrowLeft className="w-[16px] h-[25px] lg:m-1" /> */}
              </span>
              {back}
            </p>
          </div>
          <div className="text-center">
            <Tab className="font-bold sm:text-sm lg:text-xl xl:text-2xl">
              {({ selected }) => (
                <div
                  className={
                    selected
                      ? "border-t-[3px] border-primary-orange h-full text-center xs:p-4 sm:p-4 lg:p-4"
                      : "border-t-[3px] border-transparent p-4"
                  }
                >
                  {label}
                </div>
              )}
            </Tab>
          </div>

          <div className="text-right xs:hidden sm:hidden md:block lg:block">
            <div className="flex justify-end">
              <Button
                color="primary-bg"
                textClass=""
                text="Pesan Sekarang"
                className="m-[10px] lg:w-[224px]"
              />
              <div className="border-[3px] rounded-lg border-primary-orange p-4 m-3"></div>
            </div>
          </div>
          <div className="col-span-2 mx-2 xs:block sm:hidden lg:hidden">
            <div className="flex">
              <Button
                color="primary-bg"
                textClass=""
                text="Pesan Sekarang"
                className="m-[10px] lg:w-[224px]"
              />
              <div className="border-[3px] rounded-lg border-primary-orange p-4 m-3"></div>
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

export default TabList;
