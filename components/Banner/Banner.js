import React from "react";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";

const useStyles = makeStyles({
  image: {
    backgroundImage:
      'url("https://eventori.id/assets/images/img_galeri/slide_1584528871.png")',
  },
});

export default function CardBanner(props) {
  const classes = useStyles();

  return (
    <Box pt={5} height={390} width="100%" className={classes.image}>
      <Grid
        container
        justifyContent="center"
        alignItems="center"
        className="h-full"
      >
        <div className="xs:text-lg lg:-text-4xl font-bold text-white">
          {props.title}
        </div>
      </Grid>
    </Box>
  );
}
