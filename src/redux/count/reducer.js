import types from './types'

const initialState = {
  count: 1
}

export default function reducer(state= initialState, action) {
  switch (action.type) {
    case types["ADD"]:
      return { ...state, count: action.payload };
    case types["REMOVE"]:
      return { ...state, count: action.payload };
    case types["CLEAR"]:
      return { ...state, count: 1 };
    default:
      return state;
  }
}