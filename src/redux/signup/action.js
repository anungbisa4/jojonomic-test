import types from "./types";

export const stepOneTalent = (payload) => {
  return (dispatch) => {
    dispatch({ type: types.STEP_1_TALENT, payload });
  };
};
export const stepTwoTalent = (payload) => {
  return (dispatch) => {
    dispatch({ type: types.STEP_2_TALENT, payload });
  };
};
export const stepPhotoTalent = (payload, index) => {
  return (dispatch) => {
    dispatch({ type: types.STEP_PHOTO_TALENT, payload, index });
  };
};
export const removePhotoTalent = (payload) => {
  return (dispatch) => {
    dispatch({ type: types.REMOVE_PHOTO_TALENT, payload });
  };
};
export const stepRiderTalent = (payload) => {
  return (dispatch) => {
    dispatch({ type: types.STEP_RIDER_TALENT, payload });
  };
};

export const stepOneInfluencer = (payload) => {
  return (dispatch) => {
    dispatch({ type: types.STEP_1_INFLUENCER, payload });
  };
};
export const stepTwoInfluencer = (payload) => {
  return (dispatch) => {
    dispatch({ type: types.STEP_2_INFLUENCER, payload });
  };
};
export const stepPhotoInfluencer = (payload, index) => {
  return (dispatch) => {
    dispatch({ type: types.STEP_PHOTO_INFLUENCER, payload, index });
  };
};
export const removePhotoInfluencer = (payload) => {
  return (dispatch) => {
    dispatch({ type: types.REMOVE_PHOTO_INFLUENCER, payload });
  };
};
export const stepServiceInfluencer = (payload) => {
  return (dispatch) => {
    dispatch({ type: types.STEP_SERVICE_INFLUENCER, payload });
  };
};


export const stepOneVenue = (payload) => {
  return (dispatch) => {
    dispatch({ type: types.STEP_1_VENUE, payload });
  };
};
export const stepTwoVenue = (payload) => {
  return (dispatch) => {
    dispatch({ type: types.STEP_2_VENUE, payload });
  };
};
export const stepDetailVenue = (payload) => {
  return (dispatch) => {
    dispatch({ type: types.STEP_DETAIL_VENUE, payload });
  };
};
export const stepPhotoVenue = (payload, index) => {
  return (dispatch) => {
    dispatch({ type: types.STEP_PHOTO_VENUE, payload, index });
  };
};
export const stepFourVenue = (payload) => {
  return (dispatch) => {
    dispatch({ type: types.STEP_4_VENUE, payload });
  };
};

export const stepOneVendor = (payload) => {
  return (dispatch) => {
    dispatch({ type: types.STEP_1_VENDOR, payload });
  };
};
export const stepTwoVendor = (payload) => {
  return (dispatch) => {
    dispatch({ type: types.STEP_2_VENDOR, payload });
  };
};
export const stepDetailVendor = (payload) => {
  return (dispatch) => {
    dispatch({ type: types.STEP_DETAIL_VENDOR, payload });
  };
};
export const stepPhotoVendor = (payload, index) => {
  return (dispatch) => {
    dispatch({ type: types.STEP_PHOTO_VENDOR, payload, index });
  };
};
export const stepFourVendor = (payload) => {
  return (dispatch) => {
    dispatch({ type: types.STEP_4_VENDOR, payload });
  };
};


// email verification and social media
export const replaceEmail = (payload, typeSignup) => {
  return (dispatch) => {
    dispatch({ type: types.REPLACE_EMAIL, payload, typeSignup });
  };
};

export const stepSocialMedia = (payload, typeSignup) => {
  return (dispatch) => {
    dispatch({ type: types.STEP_SOCIAL_MEDIA, payload, typeSignup });
  };
};

