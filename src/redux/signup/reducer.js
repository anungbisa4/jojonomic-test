import types from "./types";
import isEmpty from "lodash/isEmpty";

const initialState = {
  talent: {
    stepPhoto: [],
  },
  vendor: {
    stepPhoto: [],
  },
  influencer: {
    stepPhoto: [],
  },
  venue: {
    stepPhoto: [],
  },
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case types.STEP_1_TALENT:
      return {
        ...state,
        talent: { ...state.talent, stepOne: action.payload },
        status: "talent",
      };
    case types.STEP_2_TALENT:
      return {
        ...state,
        talent: { ...state.talent, stepTwo: action.payload },
      };
    case types.STEP_RIDER_TALENT:
      return {
        ...state,
        talent: { ...state.talent, stepRider: action.payload },
      };
    case types.STEP_PHOTO_TALENT:
      const photoTalent = state?.talent?.stepPhoto || [];
      return {
        ...state,
        talent: {
          ...state.talent,
          stepPhoto: [...photoTalent, ...action.payload],
        },
      };
    case types.REMOVE_PHOTO_TALENT:
      let removePhotoTalent = state.talent.stepPhoto.filter((_, index) => {
        return index !== action.payload;
      });
      return {
        ...state,
        talent: {
          ...state.talent,
          stepPhoto: [...removePhotoTalent],
        },
      };

    // influencer
    case types.STEP_1_INFLUENCER:
      return {
        ...state,
        influencer: { ...state.influencer, stepOne: action.payload },
        status: "influencer",
      };
    case types.STEP_2_INFLUENCER:
      return {
        ...state,
        influencer: { ...state.influencer, stepTwo: action.payload },
        status: "influencer",
      };
    case types.STEP_SERVICE_INFLUENCER:
      return {
        ...state,
        influencer: { ...state.influencer, stepService: action.payload },
      };
    case types.STEP_PHOTO_INFLUENCER:
      const photoInfluencer = state?.influencer?.stepPhoto || [];
      return {
        ...state,
        influencer: {
          ...state.influencer,
          stepPhoto: [...photoInfluencer, ...action.payload],
        },
      };
    case types.REMOVE_PHOTO_INFLUENCER:
      let removePhotoInfluencer = state.influencer.stepPhoto.filter(
        (_, index) => {
          return index !== action.payload;
        }
      );
      return {
        ...state,
        influencer: {
          ...state.influencer,
          stepPhoto: [...removePhotoInfluencer],
        },
      };

    case types.STEP_SOCIAL_MEDIA:
      return {
        ...state,
        [action.typeSignup]: {
          ...state[action.typeSignup],
          [`${action.typeSignup}Socmed`]: action.payload,
        },
      };
    case types.REPLACE_EMAIL:
      return {
        ...state,
        [action.typeSignup]: {
          ...state[action.typeSignup],
          [`${action.typeSignup}ReplaceEmail`]: !isEmpty(action.payload)
            ? action.payload
            : null,
        },
      };
    // venue
    case types.STEP_1_VENUE:
      return {
        ...state,
        venue: { ...state.venue, stepOne: action.payload },
        status: "venue",
      };
    case types.STEP_2_VENUE:
      return {
        ...state,
        venue: { ...state.venue, stepTwo: action.payload },
        status: "venue",
      };
    case types.STEP_4_VENUE:
      return {
        ...state,
        venue: { ...state.venue, stepFour: action.payload },
        status: "venue",
      };
    case types.STEP_DETAIL_VENUE:
      return {
        ...state,
        venue: { ...state.venue, stepDetail: action.payload },
        status: "venue",
      };
    case types.STEP_PHOTO_VENUE:
      let venuePhoto = state?.venue?.stepPhoto || [];
      let venuePhotoIndex = (venuePhoto && venuePhoto?.[action.index]) || [];
      venuePhotoIndex.push(action.payload);
      const arrPhoto = { ...venuePhoto, [action.index]: venuePhotoIndex };
      return {
        ...state,
        venue: { ...state.venue, stepPhoto: arrPhoto },
        status: "venue",
      };

    case types.STEP_1_VENDOR:
      return {
        ...state,
        vendor: { ...state.vendor, stepOne: action.payload },
        status: "vendor",
      };
    case types.STEP_2_VENDOR:
      return {
        ...state,
        vendor: { ...state.vendor, stepTwo: action.payload },
        status: "vendor",
      };
    case types.STEP_4_VENDOR:
      return {
        ...state,
        vendor: { ...state.vendor, stepFour: action.payload },
        status: "vendor",
      };
    case types.STEP_DETAIL_VENDOR:
      return {
        ...state,
        vendor: { ...state.vendor, stepDetail: action.payload },
        status: "vendor",
      };
    case types.STEP_PHOTO_VENDOR:
      let vendorPhoto = state?.vendor?.stepPhoto || [];
      let vendorPhotoIndex = (vendorPhoto && vendorPhoto?.[action.index]) || [];
      vendorPhotoIndex.push(action.payload);
      const arrPhotoVendor = { ...vendorPhoto, [action.index]: vendorPhotoIndex };
      return {
        ...state,
        vendor: { ...state.vendor, stepPhoto: arrPhotoVendor },
        status: "vendor",
      };
    default:
      return state;
  }
}
