import types from "./types";
import isEmpty from "lodash/isEmpty";
import { removeData } from "dom7";

const initialState = {
  dataUser: {},
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case types.USER_REGISTER:
      return {
        ...state,
        dataUser: action.payload,
      };
    default:
      return state;
  }
}
