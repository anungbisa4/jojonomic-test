import types from "./types";

const initialState = {};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case types["LOADING"]:
      return {
        ...state,
        [action.key]: {
          loading: true,
          // message: "loading",
        },
      };
    case types["ERROR"]:
      console.log("testt", action.key);
      return {
        ...state,
        [action.key]: {
          error: true,
          message: action.message,
        },
      };
    case types["SUCCESS"]:
      return {
        ...state,
        [action.key]: {
          success: true,
          // message: "success",
        },
      };
    case types["COMPLETED"]:
      return {
        ...state,
        [action.key]: {
          loading: false,
          error: false,
        },
      };
    default:
      return state;
  }
}
