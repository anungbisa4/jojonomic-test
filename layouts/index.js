import AppBar from "components/AppBar/AppBar";
import Footer from "components/Footer";

const Index = ({ children, disableHeader, disableFooter, footerCopyRight }) => {
  return (
    <>
      <div className="flex flex-col h-screen font-pop">
        {!disableHeader && (
          <header>
            <AppBar />
          </header>
        )}
        <main className="flex-1">{children}</main>
        {!disableFooter && (
          <Footer />
        )}
        {footerCopyRight && (
          <h5 className="text-center font-pop" style={{padding: "20px 0"}}>
            © Eventori 2021
          </h5>
        )}
      </div>
    </>
  );
};

export default Index