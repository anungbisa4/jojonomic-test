## NOTE UNTUK Front End Developer
- supaya menggunakan yarn, **Jangan** pakai npm
- selali diskusi ketika install library baru **Jangan asal install 'yarn add nama-package'**
- struktur sesuai dengan contoh yg sudah ada penamaan components, folder components, pages, (penamaan component == penamaan folder components)
- css global disimpan di styles, tapi **better** ketika menggunakan untuk merapihkan saja misal seperti margin kurang kekanan, merapihkan padding, image fit, dsj, inline saja di componets.
- folder api untuk mapping rest api / mock server

## NOTE Dokumentasi

- Penamaan component harus proper, misal jika component itu tidak memiliki data/props yang di kirim **ATAU** memiliki hanya teks section saja maka prefix yang digunakan yaitu ComponentNamapagesNamanya. dan juga jika 
misal ComponentIndexBanner, ComponentIndexJoinUs, ComponentContactUsTitle dst. 
- Jika Component yang dibuat adalah reusable maka harus dengan nama yang umum/bhs inggris, misal Stepper jadi Stepper, Card jadi Card aja. dst.
  



## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
```

[API routes](https://nextjs.org/docs/api-routes/introduction) can be accessed on [http://localhost:3000/api/hello](http://localhost:3000/api/hello). This endpoint can be edited in `pages/api/hello.js`.

The `pages/api` directory is mapped to `/api/*`. Files in this directory are treated as [API routes](https://nextjs.org/docs/api-routes/introduction) instead of React pages.